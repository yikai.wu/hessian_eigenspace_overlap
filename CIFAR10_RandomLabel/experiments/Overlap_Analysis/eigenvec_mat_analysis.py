import os
import torch
import numpy as np
import matplotlib as mpl
from matplotlib import cm
from matplotlib import gridspec
import matplotlib.pyplot as plt
import pynvml
mpl.use('Agg')
vis_dir = "Eigespace_Overlap/CIFAR10/"
# plt.style.use('seaborn')

from subspace_overlap import overlap, file_fetch
from networks import LeNet
from subspace_overlap.overlap import Subspaces

epoch = -1
eval_dim = 1000
pca_dim = 1000
pca_rank = 1000
layer = 'fc1.weight'
dircs = ['../LeNet5_fixlr0.01_RL']

name_alias = '_X_'.join([x.split('/')[-1] for x in dircs])
desc = 'CIFAR10RandomLabel_{}_layer{}_EigenvecMatNormalized'.format(name_alias, layer)
eval_paths = file_fetch.fetch_dirc(dircs, eval_dim, epoch, arg="_LW_ET")[:2]


subspace = Subspaces(eval_paths)

plt.figure(figsize=(12,24))
k = 20
m_cnt = 2
gs = gridspec.GridSpec(k, m_cnt)
sd = subspace.metas[0]['model_sd']

for m in range(m_cnt):
    model_ind = m
    eigenvecs_layerwise = subspace.metas[model_ind]['eigenvecs_layer']
    eigenvals_layerwise = subspace.metas[model_ind]['eigenvals_layer']
    assert layer in eigenvecs_layerwise
    layer_sd = sd[layer]
    layer_shape = sd[layer].size()
    eigenvecs_layer = eigenvecs_layerwise[layer]
    eigenvals_layer = eigenvals_layerwise[layer]

    for i, eigenvec in enumerate(eigenvecs_layer):
        mat = eigenvec.view(layer_shape).numpy()
        plt.subplot(gs[i, m])
        plt.title(layer + " model{} λ{}={:.4g}".format(m, i + 1, eigenvals_layer[i]))
        plt.imshow(np.abs(mat), cmap="Reds")
        if i == k - 1:
            break
plt.tight_layout()
img_name = "Dim2OverlapLayer_{}.jpg".format(desc)
img_path = os.path.join(vis_dir, img_name)
print("" + img_path.split("Visualizations")[1])
plt.savefig(img_path, dpi=150)

