import os
import torch
import numpy as np
import matplotlib as mpl
from matplotlib import cm
from matplotlib import gridspec
import matplotlib.pyplot as plt
import pynvml
mpl.use('Agg')
vis_dir = "Eigespace_Overlap/CIFAR10/"
# plt.style.use('seaborn')

from subspace_overlap import overlap, file_fetch
from networks import LeNet
from subspace_overlap.overlap import Subspaces

epoch = -1
eval_dim = 1000
pca_dim = 1000
pca_rank = 1000
layer = 'fc3.weight'
dircs = ['../LeNet5_fixlr0.01_RL']

name_alias = '_X_'.join([x.split('/')[-1] for x in dircs])
eval_paths = file_fetch.fetch_dirc(dircs, eval_dim, epoch, arg="_LW_ET")[:1]


subspace = Subspaces(eval_paths)

plt.figure(figsize=(12,12))
k = 5
gs = gridspec.GridSpec(k + 2, 3)
sd = subspace.metas[0]['model_sd']
start = 0

desc = 'CIFAR10RandomLabel_{}_layer{}_EigenvecMatNormalize_start{}'.format(name_alias, layer, start)
model_ind = 0
eigenvecs_layerwise = subspace.metas[model_ind]['eigenvecs_layer']
eigenvals_layerwise = subspace.metas[model_ind]['eigenvals_layer']
assert layer in eigenvecs_layerwise
layer_sd = sd[layer]
layer_shape = sd[layer].size()
eigenvecs_layer = eigenvecs_layerwise[layer][start:]
eigenvals_layer = eigenvals_layerwise[layer]

for i, eigenvec in enumerate(eigenvecs_layer):
    mat = np.abs(eigenvec.view(layer_shape).numpy())
    mat_col = np.divide(mat, mat.sum(0))
    mat_row = np.divide(mat.transpose(), mat.sum(1)).transpose()
    plt.subplot(gs[i, 0])
    plt.title(layer + " λ{}={:.4g}".format(start + i + 1, eigenvals_layer[i + start]))
    plt.imshow(np.abs(mat), cmap="Reds")
    plt.subplot(gs[i, 1])
    plt.title("col normalize")
    plt.imshow(mat_col, cmap="Blues")
    plt.subplot(gs[i, 2])
    plt.title("row normalize")
    plt.imshow(mat_row, cmap="Purples")

    if i == k - 1:
        break
plt.subplot(gs[k:, 0:])
plt.title("eigenvals {}".format(layer))
plt.scatter(np.arange(len(eigenvals_layer)), eigenvals_layer, s=7)
plt.xlim([-2, 2*layer_shape[0]])
plt.tight_layout()
img_name = "Dim2OverlapLayerNormalized_{}.jpg".format(desc)
img_path = os.path.join(vis_dir, img_name)
print("" + img_path.split("Visualizations")[1])
plt.savefig(img_path, dpi=150)

