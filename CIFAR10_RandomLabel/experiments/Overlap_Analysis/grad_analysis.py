import os, sys
sys.path.append(os.getcwd())

import torch
import glob
import numpy as np
from tqdm import tqdm
import argparse

import matplotlib as mpl
from matplotlib import cm
import matplotlib.pyplot as plt
mpl.use('Agg')

vis_dirc = "train_log/cifar10_landscape_exp/LeNetHEP1/Overlap_Analysis_Data/vis/colnorm/"

global_device = 'cpu'
if torch.cuda.is_available():
    global_device = 'cuda'

from networks.LeNet import LeNet5
net = LeNet5()

def get_layer_info():
    s = 0
    e = 0
    sd = net.state_dict()
    ret = []
    for k in (sd):
        e += sd[k].view(-1).size()[0]
        print(s, e, k)
        ret.append([s, e, k])
        s = e
    return ret

def eigenvecs2projmat(eigenvecs, num: int):
    assert len(eigenvecs) >= num
    assert num >= 1
    vecs = [eigenvecs[i] for i in range(num)]
    paramspace_dim = len(vecs[0])
    M = torch.cat(vecs).view(torch.Size([num, paramspace_dim])) # pylint: disable=no-member
    Mt = torch.transpose(M, 0, 1) # pylint: disable=no-member
    return M, Mt

def get_files(base, epochs):
    fl_ls = []
    for i in range(epochs + 1):
        potential_eval = glob.glob(os.path.join(base, "epoch{}.pth*eval".format(i)))
        if len(potential_eval) != 0:
            fl_ls.append(potential_eval[0])
    return fl_ls

def col_norm_M(eigenvecs, n):
    M, _ = eigenvecs2projmat(eigenvecs, n)
    return torch.norm(M, dim=0).to('cpu').numpy() # pylint: disable=no-member

def col_norm_MMt(eigenvecs, n):
    M, Mt = eigenvecs2projmat(eigenvecs, n)
    Mv = torch.sum(M, dim=1) # pylint: disable=no-member
    return torch.mv(Mt, Mv).to('cpu').numpy() # pylint: disable=no-member

def load_eval(eval_path, device=global_device):
    metas = torch.load(eval_path, map_location=device)
    metas['eigenvecs'] = [torch.from_numpy(v).to(global_device) for v in metas["eigenvecs"]] # pylint: disable=no-member
    return metas

def colnorm_analysis():

    limit = [0, 1000]
    limit = [0, 62006]
    limit = [0, 5000]
    task_name = 'base_grad_epoch1000_{}to{}'.format(limit[0], limit[1])

    bases = {"base1": "../LeNet_base_repeating/experiment_log/run_lr0.01_1/models/epoch1000.pth_ET300.eval",
             "base2": "../LeNet_base_repeating/experiment_log/run_lr0.01_2/models/epoch1000.pth_ET300.eval",
             "base3": "../LeNet_base_repeating/experiment_log/run_lr0.01_3/models/epoch1000.pth_ET300.eval",
             "base4": "../LeNet_base_repeating/experiment_log/run_lr0.01_4/models/epoch1000.pth_ET300.eval"
             }

    exps = {"exp1": "../LeNet_explr0.1_100_repeating/experiment_log/run_exp_rep1/models/epoch1000.pth_ET300.eval",
            "exp2": "../LeNet_explr0.1_100_repeating/experiment_log/run_exp_rep2/models/epoch1000.pth_ET300.eval",
            "exp3": "../LeNet_explr0.1_100_repeating/experiment_log/run_exp_rep3/models/epoch1000.pth_ET300.eval",
            "exp4": "../LeNet_explr0.1_100_repeating/experiment_log/run_exp_rep4/models/epoch1000.pth_ET300.eval",
             }
    
    evals = exps
    evals = {}
    evals.update(bases)
    
    plt.figure(figsize=(12,8))
    for k in list(evals.keys()):
        print(k)
        metas = load_eval(evals[k])
        y_data = metas['train_grad_vect']
        plt.scatter(np.arange(len(y_data))[limit[0]:limit[1]], y_data[limit[0]:limit[1]], s=0.5, label=k)
    plt.title(task_name)
    
    cmap = cm.hsv # pylint: disable=no-member

    layer_info = get_layer_info()
    for i, (s, e, k) in enumerate(layer_info):
        if s >= limit[0] and s <= limit[1]:
            plt.axvspan(s, min(e, limit[1]), alpha=0.1, color=cmap(i / len(layer_info)), label=k)
    
    plt.legend(loc=1, ncol=2)

    img_name = os.path.join(vis_dirc, "{}.jpg".format(task_name))
    print(img_name)
    print("" + img_name.split("pe_exp")[1])
    plt.savefig(img_name, dpi=200)



if __name__ == "__main__":
    colnorm_analysis()