import os, sys
sys.path.append(os.getcwd())
import torch
import torchvision
import numpy as np
import datetime
from torch.utils.data import DataLoader
from algos.layerwise_feature import IntermediateFeatureExtractor
from utils import prepare_net
from config import Config
import matplotlib as mpl
mpl.use('Agg')
from matplotlib import pyplot as plt

conf = Config()
model = conf.net()
model, device, _ = prepare_net(model, use_gpu=True)
dataset = conf.dataset(train=True, transform=conf.test_transform)
data_loader = DataLoader(dataset, batch_size=conf.testing_batchsize, shuffle=True, num_workers=conf.test_provider_count)

def kronecker(A, B):
    return torch.einsum("ab,cd->acbd", A, B).view(A.size(0)*B.size(0),  A.size(1)*B.size(1))

def kron_batch(A, B):
    res = kronecker(A[0], B[0])
    for i in range(1, A.shape[0]):
        res = res.add_(kronecker(A[i], B[i]))
    return res.div_(A.shape[0])

def matrix_diag(diagonal):
    N = diagonal.shape[-1]
    shape = diagonal.shape[:-1] + (N, N)
    device, dtype = diagonal.device, diagonal.dtype
    result = torch.zeros(shape, dtype=dtype, device=device) # pylint: disable=no-member
    indices = torch.arange(result.numel(), device=device).reshape(shape) # pylint: disable=no-member
    indices = indices.diagonal(dim1=-2, dim2=-1)
    result.view(-1)[indices] = diagonal
    return result

def sample_midlayer(ife, data_loader, sample_size=1):
    inputs, _ = iter(data_loader).next()
    inputs = inputs[:sample_size].to(device)
    mid_out, final_out = ife(inputs)
    return mid_out, final_out

def overlap_calc(subspace1, subspace2, device):
    M = subspace1.to(device)
    V = subspace2.to(device)
    Mt = torch.transpose(M, 0, 1) # pylint: disable=no-member
    n = M.size()[0]
    k = 0
    for i in range(n):
        vi = V[i]
        li = Mt.mv(M.mv(vi))
        ki = torch.dot(li, li) # pylint: disable=no-member
        k += ki
    del Mt, M, V
    torch.cuda.empty_cache()
    return (k / n).to('cpu').numpy()

def E_xxT_comp(net: torch.nn.Module, fc_seq):
    ife = IntermediateFeatureExtractor(net, fc_seq)#, 'fc1':'fc1'})
    batch_sums = {layer:[] for layer in fc_seq}
    for i, (inputs, _) in enumerate(data_loader):
        inputs = inputs.to(device)
        mid_out, final_out = ife(inputs)
        for layer in mid_out.keys():
            feat_in, _ = mid_out[layer][0], mid_out[layer][1]
            feat_in_vectorized = feat_in.view(feat_in.size()[0], -1)
            feat_in_ext = feat_in_vectorized.unsqueeze(1)
            batch_E_xxT = torch.matmul(feat_in_ext.transpose(1, 2), feat_in_ext).sum(axis=[0]).unsqueeze(0) # pylint: disable=no-member
            batch_sums[layer].append(batch_E_xxT)
    ret = {}
    for layer in fc_seq:
        E_xxT = torch.cat(batch_sums[layer]).sum(axis=0) / len(dataset) # pylint: disable=no-member
        ret[layer] = E_xxT
    return ret

def E_x_comp(net: torch.nn.Module, fc_seq):
    ife = IntermediateFeatureExtractor(net, fc_seq)#, 'fc1':'fc1'})
    batch_sums = {layer:[] for layer in fc_seq}
    for i, (inputs, _) in enumerate(data_loader):
        inputs = inputs.to(device)
        mid_out, final_out = ife(inputs)
        for layer in mid_out.keys():
            feat_in, _ = mid_out[layer][0], mid_out[layer][1]
            feat_in_vectorized = feat_in.view(feat_in.size()[0], -1)
            batch_E_x = feat_in_vectorized.sum(axis=0).unsqueeze(0) # pylint: disable=no-member
            batch_sums[layer].append(batch_E_x)
    ret = {}
    for layer in fc_seq:
        E_x = torch.cat(batch_sums[layer]).sum(axis=0) / len(dataset) # pylint: disable=no-member
        ret[layer] = E_x
    return ret

def UTAU_comp(ife, Ws, fc_seq, inputs):

    softmax = torch.nn.Softmax(dim=1).to(device)
    inputs = inputs.to(device)
    mid_out, final_out = ife(inputs)
    p = softmax(final_out)

    diag_p = matrix_diag(p)
    p_mat = p.unsqueeze(1)
    ppTs = torch.matmul(p_mat.transpose(1, 2), p_mat) # pylint: disable=no-member
    A = diag_p - ppTs

    Cs = []
    for layer in fc_seq:
        feat_in, feat_out = mid_out[layer][0], mid_out[layer][1]
        Cs.append(matrix_diag((feat_out >= 0).float()))
    
    Us = []
    batch_identity = torch.eye(diag_p.size()[-1]).unsqueeze(0).repeat(diag_p.size()[0], 1, 1).to(device) # pylint: disable=no-member
    Us.append(batch_identity)
    for i in range(len(fc_seq) - 1):
        U_prev = Us[i]
        U_next = U_prev.matmul(Ws[i]).matmul(Cs[i + 1])
        Us.append(U_next)
    
    UTAUs = [Us[i].transpose(1, 2).matmul(A).matmul(Us[i]) for i in range(len(fc_seq))]
    return UTAUs, Us, Cs

def E_UTAU_comp(net, fc_seq):

    softmax = torch.nn.Softmax(dim=1).to(device)
    fc_seq.reverse()
    ife = IntermediateFeatureExtractor(net, fc_seq)
    _, final_out = sample_midlayer(ife, data_loader)
    class_count = final_out.size()[1]
    
    # Sanity Check
    sd = net.state_dict()
    Ws = [sd[layer + '.weight'] for layer in fc_seq]
    assert class_count == Ws[0].size()[0], (class_count, Ws[0].size(), fc_seq)
    for i in range(len(Ws) - 1):
        assert Ws[i].size()[1] == Ws[i + 1].size()[0], "{}-{} weight size mismatch".format(fc_seq[i], fc_seq[i + 1])
    
    # UT * diag(p) - pp^T * U
    sum_UTAUs = {layer: [] for layer in fc_seq}
    sum_Us = {layer: [] for layer in fc_seq}
    for i, (inputs, _) in enumerate(data_loader):
        UTAUs, Us, Cs = UTAU_comp(ife, Ws, fc_seq, inputs)
        for i in range(len(fc_seq)):
            sum_UTAUs[fc_seq[i]].append(UTAUs[i].sum(axis=0).unsqueeze(0))
            sum_Us[fc_seq[i]].append(Us[i].sum(axis=0).unsqueeze(0))
    
    E_UTAU = {}
    E_U = {}
    for layer in fc_seq:
        E_UTAU[layer] = torch.cat(sum_UTAUs[layer], axis=0).sum(axis=0) / len(dataset) # pylint: disable=no-member
        E_U[layer] = torch.cat(sum_Us[layer], axis=0).sum(axis=0) / len(dataset) # pylint: disable=no-member

    fc_seq.reverse()
    return E_UTAU, E_U

def single_Hessian_comp(ife, Ws, fc_seq, inputs):

    softmax = torch.nn.Softmax(dim=1).to(device)
    inputs = inputs.to(device)
    mid_out, final_out = ife(inputs)
    p = softmax(final_out)

    diag_p = matrix_diag(p)
    p_mat = p.unsqueeze(1)
    ppTs = torch.matmul(p_mat.transpose(1, 2), p_mat) # pylint: disable=no-member
    A = diag_p - ppTs

    Cs = []
    xs = []
    for layer in fc_seq:
        feat_in, feat_out = mid_out[layer][0], mid_out[layer][1]
        xs.append(feat_in.view(feat_in.size()[0], -1).unsqueeze(1))
        Cs.append(matrix_diag((feat_out >= 0).float()))
    
    Us = []
    batch_identity = torch.eye(diag_p.size()[-1]).unsqueeze(0).repeat(diag_p.size()[0], 1, 1).to(device) # pylint: disable=no-member
    Us.append(batch_identity)
    for i in range(len(fc_seq) - 1):
        U_prev = Us[i]
        U_next = U_prev.matmul(Ws[i]).matmul(Cs[i + 1])
        Us.append(U_next)
    
    UTAUs = [Us[i].transpose(1, 2).matmul(A).matmul(Us[i]) for i in range(len(fc_seq))]
    xxTs = [xs[i].transpose(1,2).matmul(xs[i]) for i in range(len(fc_seq))]
    Hs = [kron_batch(UTAUs[i], xxTs[i]) for i in range(len(fc_seq))]
    return Hs, UTAUs, xxTs

def Full_Hessian_comp(net, fc_seq):

    softmax = torch.nn.Softmax(dim=1).to(device)
    fc_seq.reverse()
    ife = IntermediateFeatureExtractor(net, fc_seq)
    _, final_out = sample_midlayer(ife, data_loader)
    class_count = final_out.size()[1]
    
    # Sanity Check
    sd = net.state_dict()
    Ws = [sd[layer + '.weight'] for layer in fc_seq]
    assert class_count == Ws[0].size()[0], (class_count, Ws[0].size(), fc_seq)
    for i in range(len(Ws) - 1):
        assert Ws[i].size()[1] == Ws[i + 1].size()[0], "{}-{} weight size mismatch".format(fc_seq[i], fc_seq[i + 1])
    
    # UT * diag(p) - pp^T * U
    sum_Hs = dict()
    for i, (inputs, _) in enumerate(data_loader):
        Hs, _, _ = single_Hessian_comp(ife, Ws, fc_seq, inputs)
        for i in range(len(fc_seq)):
            if fc_seq[i] in sum_Hs:
                sum_Hs[fc_seq[i]].add_(Hs[i])
            else:
                sum_Hs[fc_seq[i]] = Hs[i]
    
    for layer in fc_seq:
        sum_Hs[layer].div_(len(data_loader)) # pylint: disable=no-member
    fc_seq.reverse()
    return sum_Hs

model_path = "./experiment_log/run_2/models/epoch10000.pth"
eval_meta = "./experiment_log/run_2/models/final.pth_LW_ET1000.eval"
sd = torch.load(model_path, map_location=device)
meta = torch.load(eval_meta)
fc_seq = ['fc1','fc2','fc3']
fc_size = {'fc1': (120, 400), 'fc2': (84, 120), 'fc3': (10, 84)}
im_name = '{}_{}'.format(os.getcwd().split('/')[-1], model_path.split('/')[-1])
model.load_state_dict(sd)

eval_eigenvals = meta['eigenvals_layer']
eval_eigenvecs = meta['eigenvecs_layer']

E_xxT = E_xxT_comp(model, fc_seq)
E_x = E_x_comp(model, fc_seq)

for layer in fc_seq:
    print(layer)
    xxT = E_xxT[layer]
    x = E_x[layer]
    x = x.div(x.norm()).to('cpu')
    U, S, V = torch.svd_lowrank(xxT, q=1)
    eigenvec = V.transpose(0,1)[0].to('cpu')
    print(eigenvec)
    print(x)
    print(x.dot(eigenvec))
    plt.figure(figsize=(20, 15))
    plt.plot(range(eigenvec.shape[0]), eigenvec, '.')
    for i in range(0, eigenvec.shape[0], 25):
        plt.axvline(x=i)
    plt.savefig('./figs/'+layer+'.png')
    
    n = eigenvec.shape[0]
    eigspace = eval_eigenvecs[layer+'.weight'][0] 
    eigspace = torch.from_numpy(eigspace).to('cuda').reshape(fc_size[layer])
    U, S, V = torch.svd_lowrank(eigspace, q=1)
    eigenvec2 = V.transpose(0,1)[0].to('cpu')
    print(eigenvec2)
    print(eigenvec.dot(eigenvec2))
    plt.figure(figsize=(20, 15))
    plt.plot(range(eigenvec2.shape[0]), eigenvec, '.')
    for i in range(0, eigenvec2.shape[0], 25):
        plt.axvline(x=i)
    plt.savefig('./figs/'+layer+'_2.png')

