import os
import sys
from torch import optim, nn
import torchvision.transforms as transforms
from torchvision.datasets import CIFAR10

from networks.resnet import *

class Config():

    def __init__(self, data_load=False):

        # Basic configuration
        self.dataset_path = '/Datasets/CIFAR10'

        # PLEASE CHANGE TO YOUR OWN DIRECTORY
        self.base_stoage_path = ''
        # PLEASE CHANGE TO YOUR OWN DIRECTORY

        self.experiment_path = os.path.join(self.base_stoage_path, "train_log/preliminary_exp/cifar10_resnets/resnet34vanillaSGD")
        self.vis_dir = os.path.join(self.experiment_path, 'vis')
        self.model_path = os.path.join(self.experiment_path, 'models')
        self.log_path = os.path.join(self.experiment_path, 'log')
        self.classes = ('plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck')

        self.use_gpu = True

        self.net = ResNet34
        
        # Training
        self.transform = transforms.Compose(
            [transforms.ToTensor(),
            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]
        )
        self.trainset_info = 'CIFAR10Train'
        if data_load:
            self.trainset = CIFAR10(root=self.dataset_path, train=True, download=True, transform=self.transform)
        self.train_provider_count = 4
        self.training_batchsize = 128
        self.epoch_num = 30
        self.snapshot_freq = 2


        self.criterion = nn.CrossEntropyLoss()
        self.stop_by_criterion = 1e-3

        # Testing
        self.testset_info = 'CIFAR10Test'
        if data_load:
            self.testset = CIFAR10(root=self.dataset_path, train=False, download=True, transform=self.transform)
        self.test_provider_count = 4
        self.testing_batchsize = 128
        
        self.local_explog_softlink = "./experiment_log"
        
        if data_load:
            os.system("rm -f {}".format(self.local_explog_softlink))
            os.system("ln -s {} {}".format(self.experiment_path, self.local_explog_softlink))
    
    def optimizer_conf(self, net):
        optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)
        return optimizer
    
    def lr_scheduler_conf(self, optimizer):
        lr_scheduler = optim.lr_scheduler.StepLR(optimizer, 10, gamma=0.1)
        return lr_scheduler