import os, sys
sys.path.append(os.getcwd())

import torch
import glob
import numpy as np
from tqdm import tqdm

import utils
import subspace_proj as proj
import argparse
from config import Config # pylint:disable=no-name-in-module

conf = Config()

def get_files(epochs):
    
    fl_ls = []
    for i in range(epochs + 1):
        potential_eval = glob.glob(os.path.join(conf.model_path, "epoch{}.pth*eval".format(i)))
        if len(potential_eval) != 0:
            fl_ls.append(potential_eval[0])
    return fl_ls

def overlap_calc(eigenvecs1, eigenvecs2, n):
    M, Mt = proj.eigenvecs2projmat(eigenvecs1, n)
    k = 0
    for i in range(n):
        vi = eigenvecs2[i]
        ki = torch.norm(M.mv(vi)) ** 2
        k += ki
    return (k / n).to('cpu').numpy()

def proj_grad_ratio(grad_vect, eigenvecs, n):

    M, Mt = proj.eigenvecs2projmat(eigenvecs, n)
    p_grad_vect = Mt.mv(M.mv(grad_vect))
    ratio = torch.norm(p_grad_vect) / torch.norm(grad_vect)
    return ratio.to('cpu').numpy()

def proj_grad_eigenspace_coord(grad_vect, eigenvecs, n):

    M, Mt = proj.eigenvecs2projmat(eigenvecs, n)
    eigenspace_coord = M.mv(grad_vect)
    return eigenspace_coord.to('cpu').numpy()

def eigen_abs(eigenvecs, eigenvals):
    assert len(eigenvals) == len(eigenvecs)
    indicies = list(range(len(eigenvecs)))
    indicies.sort(key = lambda x: abs(eigenvals[x]), reverse=True)
    ret_vecs = [eigenvecs[i] for i in indicies]
    ret_vals = [eigenvals[i] for i in indicies]
    return ret_vecs, ret_vals

def plot_base(metas_filename, metas, scale=[0,300]):

    import matplotlib.pyplot as plt
    import matplotlib as mpl
    mpl.use('Agg')
    
    for k in metas:
        metas[k] = metas[k][scale[0]: scale[1] + 1]
    
    print("plotting")
    plt.figure(figsize=(10,14))
    plt.subplot(611)
    plt.title("Gradient vs Projected Gradient ({})".format(metas_filename))
    plt.ylabel('Projected Ratio')
    plt.plot(metas['epoch'], metas["proj_ratio"], label='Frac Norm')
    plt.legend()
    plt.subplot(612)
    plt.ylabel('Test Loss')
    plt.plot(metas['epoch'], metas["test_loss"], label='Test Loss')
    plt.plot(metas['epoch'], [x['loss'] for x in metas["gen_gap"]], label='Loss Generalization Gap')
    plt.legend()
    plt.subplot(613)
    plt.yscale('log')
    plt.ylabel('Train Loss')
    plt.plot(metas['epoch'], metas["train_loss"], label='Train Loss')
    plt.legend()
    plt.subplot(614)
    plt.ylabel('Accuracy')
    plt.plot(metas['epoch'], metas["test_acc"], label='Test Acc')
    plt.plot(metas['epoch'], [x['acc'] for x in metas["gen_gap"]], label='Accuracy Generalization Gap')
    plt.xlabel('Epochs')
    plt.legend()
    plt.subplot(615)
    plt.plot(metas['epoch'][2:], metas["eigenspace_overlap"][2:], label='Eigenspace Overlap')
    plt.ylabel('Eigenspace Overlap')
    plt.xlabel('Epochs')
    plt.legend()
    plt.subplot(616)
    plt.plot(metas['epoch'], metas["train_grad_norm"], label='Gradient Norm')
    plt.ylabel('Norm of Gradient')
    plt.yscale('log')
    plt.xlabel('Epochs')
    plt.legend()
    plt.savefig(os.path.join(conf.vis_dir, metas_filename) + '.jpg', dpi=200)
    print(os.path.join(conf.vis_dir, metas_filename) + '.jpg')

def eigenproj_default():

    parser = argparse.ArgumentParser()
    parser.add_argument('-run', type=str, help='The indicator of run (default=1)', required=True)
    parser.add_argument('-n', '--eigenspacedim', type=int, help='Number of eigenvalues to calculate', default=10)
    parser.add_argument('-e', type=int, help='Calculate up to epoch ?', default=1000)
    parser.add_argument('-abs', '--abs', action="store_true", help="Use magnitude to define top eigenspace")
    args = parser.parse_args()

    eigen_dim = args.eigenspacedim
    eigen_mag = args.abs

    device = 'cpu'
    if torch.cuda.is_available():
        device = 'cuda'

    global conf
    conf = Config(run_number=args.run)
    fl_ls = get_files(1000)
    # print(fl_ls)

    ct_keys = ["epoch", "train_grad_norm", "train_loss", "train_acc", "test_grad_norm", "test_loss", "test_acc", "gen_gap"]
    new_keys = ["proj_ratio", "proj_ratio_test", "proj_ratio_prev", "proj_ratio_fixed", "proj_coord", "eigenspace_overlap", "eigenvals"]

    metas = {k : [] for k in ct_keys + new_keys}

    subspace_fl = fl_ls[10]
    # subspace_fl = "train_log/cifar10_landscape_exp/LeNetHEP1/LeNet_explr0.1/run_eigentrend50_1/models/epoch10.pth_ET50.eval"
    special_ind = os.getcwd().split('/')[-1]

    fixed_meta = torch.load(subspace_fl, map_location=device)
    fixed_eigenvecs = [torch.from_numpy(v) for v in fixed_meta["eigenvecs"]] # pylint:disable=no-member

    prev_eigenvecs = None
    for fl in tqdm(fl_ls):
        # print(fl)
        meta = torch.load(fl, map_location=device)
        assert meta["eigenvals"] is not None
        for k in ct_keys:
            assert k in meta
            metas[k].append(meta[k])
        # print(meta['epoch'])
        grad_vect = meta["train_grad_vect"]
        eigenvecs = [torch.from_numpy(v) for v in meta["eigenvecs"]] # pylint: disable=no-member
        eigenvals = meta["eigenvals"]

        if eigen_mag:
            eigenvecs, eigenvals = eigen_abs(eigenvecs, eigenvals)
        
        proj_ratio = proj_grad_ratio(grad_vect, eigenvecs, eigen_dim)
        proj_ratio_test = proj_grad_ratio(meta['test_grad_vect'], eigenvecs, eigen_dim)
        proj_ratio_fixsubspace = proj_grad_ratio(grad_vect, fixed_eigenvecs, eigen_dim)
        proj_coord = proj_grad_eigenspace_coord(grad_vect, eigenvecs, len(eigenvals))
        if prev_eigenvecs is not None:
            proj_ratio_prev = proj_grad_ratio(grad_vect, prev_eigenvecs, eigen_dim)
            eigenspace_overlap = overlap_calc(prev_eigenvecs, eigenvecs, eigen_dim)
        else:
            proj_ratio_prev = None
            eigenspace_overlap = None

        prev_eigenvecs = eigenvecs
        metas["proj_ratio"].append(proj_ratio)
        metas["proj_ratio_test"].append(proj_ratio_test)
        metas["proj_ratio_fixed"].append(proj_ratio_fixsubspace)
        metas["proj_ratio_prev"].append(proj_ratio_prev)
        metas["proj_coord"].append(proj_coord)
        metas["eigenvals"].append(eigenvals)
        metas["eigenspace_overlap"].append(eigenspace_overlap)

    metas_filename = "proj_dim{}_{}.evalmeta".format(eigen_dim, special_ind + "_abs" if eigen_mag else special_ind)
    torch.save(metas, os.path.join(conf.model_path, metas_filename))
    print(os.path.join(conf.model_path, metas_filename))

    plot_base(metas_filename, metas, scale=[0, len(metas['epoch'])])

if __name__ == "__main__":
    eigenproj_default()
