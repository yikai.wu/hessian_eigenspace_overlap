import torch
import torch.nn as nn
import torch.nn.functional as F

class FC3(nn.Module):
    def __init__(self):
        super(FC3, self).__init__()
        self.fc1   = nn.Linear(28*28, 200)
        self.fc2   = nn.Linear(200, 200)
        self.fc3   = nn.Linear(200, 100)
        self.fc4   = nn.Linear(100, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = F.relu(self.fc3(out))
        out = self.fc4(out)
        return out

class FC3_150_100_50(nn.Module):
    def __init__(self):
        super(FC3_150_100_50, self).__init__()
        self.fc1   = nn.Linear(28*28, 150)
        self.fc2   = nn.Linear(150, 100)
        self.fc3   = nn.Linear(100, 50)
        self.fc4   = nn.Linear(50, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = F.relu(self.fc3(out))
        out = self.fc4(out)
        return out

class FC3_1500(nn.Module):
    def __init__(self):
        super(FC3_1500, self).__init__()
        self.fc1   = nn.Linear(28*28, 1500)
        self.fc2   = nn.Linear(1500, 1500)
        self.fc3   = nn.Linear(1500, 1500)
        self.fc4   = nn.Linear(1500, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = F.relu(self.fc3(out))
        out = self.fc4(out)
        return out

class FC2(nn.Module):
    def __init__(self):
        super(FC2, self).__init__()
        self.fc1   = nn.Linear(28*28, 200)
        self.fc2   = nn.Linear(200, 200)
        self.fc3   = nn.Linear(200, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = self.fc3(out)
        return out

class FC2nb(nn.Module):
    def __init__(self):
        super(FC2nb, self).__init__()
        self.fc1   = nn.Linear(28*28, 200, bias=False)
        self.fc2   = nn.Linear(200, 200, bias=False)
        self.fc3   = nn.Linear(200, 10, bias=False)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = self.fc3(out)
        return out

class FC4nb(nn.Module):
    def __init__(self):
        super(FC4nb, self).__init__()
        self.fc1   = nn.Linear(28*28, 200, bias=False)
        self.fc2   = nn.Linear(200, 200, bias=False)
        self.fc3   = nn.Linear(200, 200, bias=False)
        self.fc4   = nn.Linear(200, 200, bias=False)
        self.fc5   = nn.Linear(200, 10, bias=False)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = F.relu(self.fc3(out))
        out = F.relu(self.fc4(out))
        out = self.fc5(out)
        return out

class FC4_600(nn.Module):
    def __init__(self):
        super(FC4_600, self).__init__()
        self.fc1   = nn.Linear(28*28, 600)
        self.fc2   = nn.Linear(600, 600)
        self.fc3   = nn.Linear(600, 600)
        self.fc4   = nn.Linear(600, 600)
        self.fc5   = nn.Linear(600, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = F.relu(self.fc3(out))
        out = F.relu(self.fc4(out))
        out = self.fc5(out)
        return out

class FC4_600_nb(nn.Module):
    def __init__(self):
        super(FC4_600_nb, self).__init__()
        self.fc1   = nn.Linear(28*28, 600, bias=False)
        self.fc2   = nn.Linear(600, 600, bias=False)
        self.fc3   = nn.Linear(600, 600, bias=False)
        self.fc4   = nn.Linear(600, 600, bias=False)
        self.fc5   = nn.Linear(600, 10, bias=False)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = F.relu(self.fc3(out))
        out = F.relu(self.fc4(out))
        out = self.fc5(out)
        return out

class FC8_600(nn.Module):
    def __init__(self):
        super(FC8_600, self).__init__()
        self.fc1   = nn.Linear(28*28, 600)
        self.fc2   = nn.Linear(600, 600)
        self.fc3   = nn.Linear(600, 600)
        self.fc4   = nn.Linear(600, 600)
        self.fc5   = nn.Linear(600, 600)
        self.fc6   = nn.Linear(600, 600)
        self.fc7   = nn.Linear(600, 600)
        self.fc8   = nn.Linear(600, 600)
        self.fc9   = nn.Linear(600, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = F.relu(self.fc3(out))
        out = F.relu(self.fc4(out))
        out = F.relu(self.fc5(out))
        out = F.relu(self.fc6(out))
        out = F.relu(self.fc7(out))
        out = F.relu(self.fc8(out))
        out = self.fc9(out)
        return out

class FC8_600_nb(nn.Module):
    def __init__(self):
        super(FC8_600_nb, self).__init__()
        self.fc1   = nn.Linear(28*28, 600, bias=False)
        self.fc2   = nn.Linear(600, 600, bias=False)
        self.fc3   = nn.Linear(600, 600, bias=False)
        self.fc4   = nn.Linear(600, 600, bias=False)
        self.fc5   = nn.Linear(600, 600, bias=False)
        self.fc6   = nn.Linear(600, 600, bias=False)
        self.fc7   = nn.Linear(600, 600, bias=False)
        self.fc8   = nn.Linear(600, 600, bias=False)
        self.fc9   = nn.Linear(600, 10, bias=False)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = F.relu(self.fc3(out))
        out = F.relu(self.fc4(out))
        out = F.relu(self.fc5(out))
        out = F.relu(self.fc6(out))
        out = F.relu(self.fc7(out))
        out = F.relu(self.fc8(out))
        out = self.fc9(out)
        return out

class FC2_narrow(nn.Module):
    def __init__(self):
        super(FC2_narrow, self).__init__()
        self.fc1   = nn.Linear(28*28, 100)
        self.fc2   = nn.Linear(100, 100)
        self.fc3   = nn.Linear(100, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = self.fc3(out)
        return out

class FC2_narrow_30(nn.Module):
    def __init__(self):
        super(FC2_narrow_30, self).__init__()
        self.fc1   = nn.Linear(28*28, 30)
        self.fc2   = nn.Linear(30, 30)
        self.fc3   = nn.Linear(30, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = self.fc3(out)
        return out

class FC2_narrow_20(nn.Module):
    def __init__(self):
        super(FC2_narrow_20, self).__init__()
        self.fc1   = nn.Linear(28*28, 20)
        self.fc2   = nn.Linear(20, 20)
        self.fc3   = nn.Linear(20, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = self.fc3(out)
        return out


class FC1(nn.Module):
    def __init__(self):
        super(FC1, self).__init__()
        self.fc1   = nn.Linear(28*28, 200)
        self.fc2   = nn.Linear(200, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = self.fc2(out)
        return out

class FC3_small(nn.Module):
    def __init__(self):
        super(FC3_small, self).__init__()
        self.fc1 = nn.Linear(7*7, 40)
        self.fc2 = nn.Linear(40, 20)
        self.fc3 = nn.Linear(20, 10)

    def forward(self, x):
        out = F.max_pool2d(x, 4)
        out = out.view(out.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = self.fc3(out)
        return out

class FC2_sigmoid(nn.Module):
    def __init__(self):
        super(FC2_sigmoid, self).__init__()
        self.fc1   = nn.Linear(28*28, 200)
        self.fc2   = nn.Linear(200, 200)
        self.fc3   = nn.Linear(200, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.sigmoid(self.fc1(out))
        out = F.sigmoid(self.fc2(out))
        out = self.fc3(out)
        return out

class FC2_tanh(nn.Module):
    def __init__(self):
        super(FC2_tanh, self).__init__()
        self.fc1   = nn.Linear(28*28, 200)
        self.fc2   = nn.Linear(200, 200)
        self.fc3   = nn.Linear(200, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.tanh(self.fc1(out))
        out = F.tanh(self.fc2(out))
        out = self.fc3(out)
        return out

class FC4(nn.Module):
    def __init__(self):
        super(FC4, self).__init__()
        self.fc1   = nn.Linear(28*28, 200)
        self.fc2   = nn.Linear(200, 200)
        self.fc3   = nn.Linear(200, 200)
        self.fc4   = nn.Linear(200, 200)
        self.fc5   = nn.Linear(200, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = F.relu(self.fc3(out))
        out = F.relu(self.fc4(out))
        out = self.fc5(out)
        return out