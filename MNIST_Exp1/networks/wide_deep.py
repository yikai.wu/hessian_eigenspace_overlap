import torch
import torch.nn as nn
import torch.nn.functional as F

class FC2_500_10(nn.Module):
    def __init__(self):
        super(FC2_500_10, self).__init__()
        self.fc1   = nn.Linear(28*28, 500)
        self.fc2   = nn.Linear(500, 500)
        self.fc3   = nn.Linear(500, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = self.fc3(out)
        return out

class FC4_200_10(nn.Module):
    def __init__(self):
        super(FC4_200_10, self).__init__()
        self.fc1   = nn.Linear(28*28, 200)
        self.fc2   = nn.Linear(200, 200)
        self.fc3   = nn.Linear(200, 200)
        self.fc4   = nn.Linear(200, 200)
        self.fc5   = nn.Linear(200, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = F.relu(self.fc3(out))
        out = F.relu(self.fc4(out))
        out = self.fc5(out)
        return out

class FC4_64_10(nn.Module):
    def __init__(self):
        super(FC4_64_10, self).__init__()
        self.fc1   = nn.Linear(28*28, 64)
        self.fc2   = nn.Linear(64, 64)
        self.fc3   = nn.Linear(64, 64)
        self.fc4   = nn.Linear(64, 64)
        self.fc5   = nn.Linear(64, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = F.relu(self.fc3(out))
        out = F.relu(self.fc4(out))
        out = self.fc5(out)
        return out

class FC8_200_10(nn.Module):
    def __init__(self):
        super(FC8_200_10, self).__init__()
        self.fc1   = nn.Linear(28*28, 200)
        self.fc2   = nn.Linear(200, 200)
        self.fc3   = nn.Linear(200, 200)
        self.fc4   = nn.Linear(200, 200)
        self.fc5   = nn.Linear(200, 200)
        self.fc6   = nn.Linear(200, 200)
        self.fc7   = nn.Linear(200, 200)
        self.fc8   = nn.Linear(200, 200)
        self.fc9   = nn.Linear(200, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = F.relu(self.fc3(out))
        out = F.relu(self.fc4(out))
        out = F.relu(self.fc5(out))
        out = F.relu(self.fc6(out))
        out = F.relu(self.fc7(out))
        out = F.relu(self.fc8(out))
        out = self.fc9(out)
        return out

class FC8_100_10(nn.Module):
    def __init__(self):
        super(FC8_100_10, self).__init__()
        self.fc1   = nn.Linear(28*28, 100)
        self.fc2   = nn.Linear(100, 100)
        self.fc3   = nn.Linear(100, 100)
        self.fc4   = nn.Linear(100, 100)
        self.fc5   = nn.Linear(100, 100)
        self.fc6   = nn.Linear(100, 100)
        self.fc7   = nn.Linear(100, 100)
        self.fc8   = nn.Linear(100, 100)
        self.fc9   = nn.Linear(100, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = F.relu(self.fc3(out))
        out = F.relu(self.fc4(out))
        out = F.relu(self.fc5(out))
        out = F.relu(self.fc6(out))
        out = F.relu(self.fc7(out))
        out = F.relu(self.fc8(out))
        out = self.fc9(out)
        return out

class FC8_32_10(nn.Module):
    def __init__(self):
        super(FC8_32_10, self).__init__()
        self.fc1   = nn.Linear(28*28, 32)
        self.fc2   = nn.Linear(32, 32)
        self.fc3   = nn.Linear(32, 32)
        self.fc4   = nn.Linear(32, 32)
        self.fc5   = nn.Linear(32, 32)
        self.fc6   = nn.Linear(32, 32)
        self.fc7   = nn.Linear(32, 32)
        self.fc8   = nn.Linear(32, 32)
        self.fc9   = nn.Linear(32, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = F.relu(self.fc3(out))
        out = F.relu(self.fc4(out))
        out = F.relu(self.fc5(out))
        out = F.relu(self.fc6(out))
        out = F.relu(self.fc7(out))
        out = F.relu(self.fc8(out))
        out = self.fc9(out)
        return out