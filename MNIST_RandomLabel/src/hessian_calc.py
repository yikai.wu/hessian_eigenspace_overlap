import os, sys, json
sys.path.append(os.getcwd())

import torch
from torch.utils.data import DataLoader, SubsetRandomSampler
import torch.backends.cudnn as cudnn
import pynvml

import numpy as np
from copy import deepcopy
from collections import OrderedDict

from config import Config # pylint: disable=no-name-in-module
import utils
from utils import *
import algos.pyhessian as pyhessian
import algos.hessian_eigenthings as hessian_eigenthings
import datetime

import argparse
conf = Config()

def lanczos_eigenthings(net, dataloader, topn):
    net.eval()
    start_time = datetime.datetime.now()
    print("Computing the top {} eigenvectors using Lanczos Method.".format(topn))
    eigenvals, eigenvecs = hessian_eigenthings.compute_hessian_eigenthings(net, dataloader, conf.criterion, topn, mode="lanczos")
    eigenvec_sds = []
    for vec in eigenvecs:
        eigenvec_sds.append(vec2statedict_eigenthings(net, vec))
    print("Took {}".format(datetime.datetime.now() - start_time))
    eigenvals = eigenvals[::-1]
    eigenvec_sds.reverse()
    return eigenvals, eigenvec_sds

def pyhessian_eigenthings(net, dataloader, topn):
    start_time = datetime.datetime.now()
    print("Computing the top {} eigenvectors using Power Iteration Method.".format(topn))
    hessian_comp = pyhessian.hessian(net, conf.criterion, dataloader=dataloader, cuda=True)
    eigenvals, eigenvecs = hessian_comp.eigenvalues(top_n=topn)
    eigenvec_sds = []
    for vec in eigenvecs:
        eigenvec_sds.append(vec2statedict_pyhessian(net, vec))
    print("Took {}".format(datetime.datetime.now() - start_time))
    return eigenvals, eigenvec_sds

def pyhessian_trace(net, dataloader):
    start_time = datetime.datetime.now()
    print("Computing the trace of hessian")
    hessian_comp = pyhessian.hessian(net, conf.criterion, dataloader=dataloader, cuda=True)
    print("Took {}".format(datetime.datetime.now() - start_time))
    return hessian_comp.trace()

def pyhessian_spec(net, dataloader):
    print("Computing the eigenspec of hessian using lanczos")
    start_time = datetime.datetime.now()
    hessian_comp = pyhessian.hessian(net, conf.criterion, dataloader=dataloader, cuda=True)
    print("Took {}".format(datetime.datetime.now() - start_time))
    return hessian_comp.density()

def main():
    print("start at {}".format(datetime.datetime.now()))
    global conf

    parser = argparse.ArgumentParser()
    parser.add_argument('-run', type=str, help='The indicator of run', default='1')
    parser.add_argument('-m', '--modelpath', type=str, default='final.pth', help='the .pth state dict model for testing')
    parser.add_argument('-ET', '--eigenthings', action='store_true', help="calculate the eigenvalue and eigenvectors")
    parser.add_argument('-ES', '--eigenspec', action='store_true', help="calculate the spectrum of eigenvalues")
    parser.add_argument('-trace', action='store_true', help="calculate the trace of the hessian")

    parser.add_argument('-method', type=str, choices=['power', 'lanczos'], default='power')
    parser.add_argument('-topn', type=int, default=2)
    parser.add_argument('-bt', type=int, default=conf.testing_batchsize)
    parser.add_argument('-P','--portion', default=1, type=float, help="the portion of training set to be used when calculating the hessian")
    parser.add_argument('-R','--randomportion', action="store_false", help="use a random partition of the dataset to compute the hessian")
    args = parser.parse_args()

    conf = Config(run_number=args.run)
    print(conf.model_path)

    net, device, _ = utils.prepare_net(conf.net(), conf.use_gpu)
    
    model_path = args.modelpath
    model_path = os.path.join(conf.model_path, model_path)
    # model_path = '/train_log/preliminary_exp/cifar10_resnets/resnet34vanilla_SGD_bt128_lrmodified_tfmodified_noaug/models/final.pth'

    assert os.path.isfile(model_path), 'invalid model path'
    net.load_state_dict(torch.load(model_path))

    
    print('Network state loaded.')
    assert (args.portion > 0 and args.portion <= 1)
    print(args)
    # dataset = conf.dataset(train=True, transform=conf.test_transform)
    dataset = conf.dataset(train=True, transform=conf.transforms.rgb_normalized)
    total_size = len(dataset)
    if args.portion != 1:
        if args.randomportion:
            indicies = np.random.choice(total_size, int(total_size * args.portion), replace=False)
        else:
            indicies = np.arange(int(total_size * args.portion))
        testloader = DataLoader(dataset, batch_size=args.bt, shuffle=True, sampler=SubsetRandomSampler(indicies))
    else:
        testloader = DataLoader(dataset, batch_size=args.bt, shuffle=True)
    
    if args.eigenthings:
        if args.method == 'power':
            eigenvals, eigenvec_sds = pyhessian_eigenthings(net, testloader, args.topn)
        else:
            try:
                eigenvals, eigenvec_sds = lanczos_eigenthings(net, testloader, args.topn)
            except:
                print('Lanczos failed to converge, use power iter instead')
                args.method = "power"
                eigenvals, eigenvec_sds = pyhessian_eigenthings(net, testloader, args.topn)
        log_file = model_path + ".P{}_{}_topn{}.eigenthings".format(args.portion, args.method, args.topn)
        # log_file = model_path + ".P{}_{}_topn{}_{}_bt{}.eigenthings".format(args.portion, args.method, args.topn, datetime.datetime.now(), args.bt)
        print(eigenvals)
        torch.save([eigenvals, eigenvec_sds], log_file)
                
    if args.eigenspec:
        log_file = model_path + ".P{}.eigenspec".format(args.portion)
        eigenspec = pyhessian_spec(net, testloader)
        # log_file = model_path + ".P{}_{}.eigenspec".format(args.portion, datetime.datetime.now())
        torch.save(eigenspec, log_file)

    if args.trace:
        log_file = model_path + ".P{}.eigenspec".format(args.portion)
        trace = pyhessian_trace(net, testloader)
        # log_file = model_path + ".P{}_{}.hessiantrace".format(args.portion, datetime.datetime.now())
        torch.save(trace, log_file)

if __name__ == "__main__":
    main()