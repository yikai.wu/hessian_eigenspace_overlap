import os
import torch
import numpy as np
import matplotlib as mpl
from matplotlib import cm
from matplotlib import gridspec
import matplotlib.pyplot as plt
import pynvml
mpl.use('Agg')
vis_dir = "Eigespace_Overlap/CIFAR10/"
# plt.style.use('seaborn')

from subspace_overlap import overlap, file_fetch
from networks import LeNet
from subspace_overlap.overlap import Subspaces

epoch = -1
eval_dim = 250
pca_dim = 1000
pca_rank = 1000
layer = 'fc1.weight'
dircs = ['../LeNet5_exp0.01_500']

name_alias = '_X_'.join([x.split('/')[-1] for x in dircs])
eval_paths = file_fetch.fetch_dirc(dircs, eval_dim, epoch, arg="_LW_ET")[:1]


subspace = Subspaces(eval_paths)

plt.figure(figsize=(16,24))
k = 12
gs = gridspec.GridSpec(k + 2, 4, width_ratios=[2,2,2,1])
sd = subspace.metas[0]['model_sd']
start = 115

desc = 'CIFAR10_{}_layer{}_EigenvecMatNormalize_start{}'.format(name_alias, layer, start)
model_ind = 0
eigenvecs_layerwise = subspace.metas[model_ind]['eigenvecs_layer']
eigenvals_layerwise = subspace.metas[model_ind]['eigenvals_layer']
assert layer in eigenvecs_layerwise
layer_sd = sd[layer]
layer_shape = sd[layer].size()
layer_grad = subspace.metas[model_ind]['train_grad_sd'][layer]
eigenvecs_layer = eigenvecs_layerwise[layer][start:]
eigenvals_layer = eigenvals_layerwise[layer]

mat_np_abs = np.abs(layer_sd.numpy())
U, S, V = torch.svd(layer_sd)
mat_col = np.divide(mat_np_abs, mat_np_abs.sum(0))
mat_row = np.divide(mat_np_abs.transpose(), mat_np_abs.sum(1)).transpose()
plt.subplot(gs[0, 0])
plt.title(layer + " parameter")
plt.imshow(mat_np_abs, cmap="Reds")
plt.subplot(gs[0, 1])
plt.title("col normalize")
plt.imshow(mat_col, cmap="Blues")
plt.subplot(gs[0, 2])
plt.title("row normalize")
plt.imshow(mat_row, cmap="Purples")
plt.subplot(gs[0, 3])
S = S.numpy()
plt.scatter(np.arange(len(S)), S, s=10)
plt.title("singular value dist")

mat_np_abs = np.abs(layer_grad.numpy())
U, S, V = torch.svd(layer_grad)
mat_col = np.divide(mat_np_abs, mat_np_abs.sum(0))
mat_row = np.divide(mat_np_abs.transpose(), mat_np_abs.sum(1)).transpose()
plt.subplot(gs[1, 0])
plt.title(layer + " full batch grad")
plt.imshow(mat_np_abs, cmap="Reds")
plt.subplot(gs[1, 1])
plt.title("col normalize")
plt.imshow(mat_col, cmap="Blues")
plt.subplot(gs[1, 2])
plt.title("row normalize")
plt.imshow(mat_row, cmap="Purples")
plt.subplot(gs[1, 3])
S = S.numpy()
plt.scatter(np.arange(len(S)), S, s=10)
plt.title("singular value dist")

for i, eigenvec in enumerate(eigenvecs_layer):
    eigenmat = eigenvec.view(layer_shape)
    U, S, V = torch.svd(eigenmat)
    mat_np_abs = np.abs(eigenmat.numpy())
    mat_col = np.divide(mat_np_abs, mat_np_abs.sum(0))
    mat_row = np.divide(mat_np_abs.transpose(), mat_np_abs.sum(1)).transpose()
    plt.subplot(gs[i + 2, 0])
    plt.title(layer + " λ{}={:.4g}".format(start + i + 1, eigenvals_layer[i + start]))
    plt.imshow(mat_np_abs, cmap="Reds")
    plt.subplot(gs[i + 2, 1])
    plt.title("col normalize")
    plt.imshow(mat_col, cmap="Blues")
    plt.subplot(gs[i + 2, 2])
    plt.title("row normalize")
    plt.imshow(mat_row, cmap="Purples")
    plt.subplot(gs[i + 2, 3])
    S = S.numpy()
    plt.scatter(np.arange(len(S)), S, s=10)
    plt.title("singular value dist")
    if i == k - 1:
        break
plt.tight_layout()
img_name = "Dim2OverlapLayerNormalized_{}.jpg".format(desc)
img_path = os.path.join(vis_dir, img_name)
print("" + img_path.split("Visualizations")[1])
plt.savefig(img_path, dpi=150)

