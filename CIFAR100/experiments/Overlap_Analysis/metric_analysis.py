import os
import torch
import numpy as np
import matplotlib as mpl
from matplotlib import cm
from matplotlib import gridspec
import datetime
import matplotlib.pyplot as plt
mpl.use('Agg')
vis_dir = "Eigespace_Overlap/CIFAR10/"
plt.style.use('seaborn')
from subspace_overlap import file_fetch

epoch = -1
eval_dim = 1000
compare_targets = [
    ['../LeNet5_fixlr0.01'],
    ['../LeNet5_fixlr0.01_btsz8'],
    ['../LeNet5_fixlr0.01_momentum0.9'],
    ['../LeNet5_AdamDefault']
]
# compare_targets = [
#     ['../LeNet5_fixlr0.01']
# ]

inds = ['gen_loss', 'gen_acc', 'train_loss', 'train_acc']
gridw, gridh, hratio = 2, 2, [2, 1]

gs = gridspec.GridSpec(gridw, gridh, height_ratios=hratio)

name_aliases = ['_X_'.join([x.split('/')[-1] for x in dircs]) for dircs in compare_targets]

def get_info(eval_paths, inds):
    st = datetime.datetime.now()
    raw = {k: [] for k in inds}
    # for f in eval_paths:
    for f in eval_paths[:2]:
        meta = torch.load(f, map_location='cpu')
        meta['gen_acc'] = -meta['gen_gap']['acc']
        meta['gen_loss'] = meta['gen_gap']['loss']
        for k in inds:
            raw[k].append(meta[k])
        del meta
    ret = {}
    for k in inds:
        ret[k] = (np.mean(raw[k]), np.std(raw[k]))
    print('took {}'.format(datetime.datetime.now() - st))
    return ret

def comp_models(target_dircs, inds):

    dists = {k: [[], []] for k in inds}
    
    for dircs in target_dircs:
        eval_paths = file_fetch.fetch_dirc(dircs, eval_dim, epoch)
        print("loading {}, total model count: {}".format(str(dircs), len(eval_paths)))
        res = get_info(eval_paths, inds)

        for k in inds:
            dists[k][0].append(res[k][0])
            dists[k][1].append(res[k][1])
    
    return dists

if __name__ == "__main__":
    
    dists = comp_models(compare_targets, inds)
    plt.figure(figsize=(10,8))

    for i, k in enumerate(inds):
        print(i, k)
        plt.subplot(gs[i // gridw, i % gridw])
        plt.title(k, fontsize=14)
        for j, case in enumerate(name_aliases):
            plt.bar(j, dists[k][0][j], yerr=dists[k][1][j], label=case)
        plt.legend()
    img_name = "Metric_{}.jpg".format('X'.join(name_aliases))
    img_path = os.path.join(vis_dir, img_name)
    plt.savefig(img_path, dpi=200)
    print("" + img_path.split("Visualizations")[1])


    
