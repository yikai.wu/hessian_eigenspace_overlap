import os, sys
import torch
import numpy as np
import matplotlib as mpl
from matplotlib import cm
from matplotlib import gridspec
import matplotlib.pyplot as plt
import pynvml
import datetime
from tqdm import tqdm

mpl.use('Agg')
from matplotlib import rc
plt.style.use('seaborn-deep')
rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)
params= {'text.latex.preamble' : [r'\usepackage{amsmath}',r'\usepackage{amssymb}', r'\usepackage{bm}']}
plt.rcParams.update(params)

plt.style.use('seaborn-deep')
plt.rcParams['xtick.labelsize']= 12
plt.rcParams['ytick.labelsize']= 12
plt.rcParams['axes.labelsize'] = 14
plt.rcParams['axes.titlesize'] = 14

from subspace_overlap import overlap, file_fetch
from networks import LeNet
from subspace_overlap.overlap import Subspaces, overlap_explicit

dev = 'cuda' if torch.cuda.is_available() else 'cpu'

def get_image_path(name, store_format='jpg'):
    vis_dir = ""
    img_path = os.path.join(vis_dir, "{}.{}".format(name, store_format))
    print("" + img_path.split("Shared")[1] + '\n')
    return img_path

epoch = -1
# dims = np.concatenate([np.arange(1, 50, 2), np.arange(51, 300, 10)])
dims = np.concatenate([np.arange(1, 121, 1)])
dims = np.concatenate([np.arange(1, 151, 1)])
n_runs = 5
# dims = np.concatenate([np.arange(1, 100, 2), np.arange(101, 250, 10)])

dirc = '../Resnet18W64ShiftX_nobn_fixlr0.01'
dirc = '../Resnet18W48_nobn_fixlr0.01'
dirc = '../VGG11W80New_nobn_fixlr0.01'
dirc = '../Resnet18W80_nobn_fixlr0.01'
dirc = '../Resnet18W64New_nobn_fixlr0.01'
dirc = '../VGG11W48New_nobn_fixlr0.01'

assert os.path.isdir(dirc)
sys.path.append(dirc)
from config import Config # pylint: disable=no-name-in-module
conf = Config()

net = conf.net()
layer_info = {}
for layer in net.state_dict().keys():
    layer_info[layer] = len(net.state_dict()[layer].view(-1))

layers = []
for layer in layer_info:
    if layer.endswith('.weight'):
        layers.append(layer)
log_format = 'final.pth_LW_part_t120_layer{}'
log_format = 'final.pth_LW_part_t150_layer{}'

def get_logs(n, layer):
    log_name = log_format.format(layer)
    ret = []
    for i in range(1, n + 1):
        log_dirc = os.path.join(dirc, 'experiment_log', 'run_{}'.format(i), 'models', log_name)
        if os.path.isfile(log_dirc):
            meta = torch.load(log_dirc)
            ret.append(torch.from_numpy(meta[1]).to(dev))
        else:
            continue
    return ret

exp_name = 'DimOverlap_CIFAR100_{}'.format(dirc.split('/')[-1])

store_file = dirc + '/overlap_layerwise_{}_{}_{}.ovlp'.format(min(dims), max(dims), len(dims))

load_log = True
if os.path.isfile(store_file) and load_log:
    overlap_means, overlap_stds, overlap_dims, layer_width, layers = torch.load(store_file, map_location='cpu')
    print('loaded previously computed result')

else:
    
    overlap_means = []
    overlap_stds = []
    overlap_dims = []

    for layer in layers:
        
        cnt = layer_info[layer]
        pos = np.sum(dims < cnt)
        dims_layer = dims[:pos]

        overlaps_raw = []
        eigenvecs = get_logs(n_runs, layer)
        print('Layer {} Total Models: {} Dim: {}'.format(layer, len(eigenvecs), cnt))
        if len(eigenvecs) == 0:
            continue

        for dim in tqdm(dims_layer):
            overlaps_raw.append(overlap_explicit(eigenvecs, dim, device=dev))
        
        overlap_mean = np.zeros(len(dims_layer))
        overlap_std = np.zeros(len(dims_layer))
        for i in range(len(dims_layer)):
            overlap_mean[i] = np.mean(overlaps_raw[i])
            overlap_std[i] = np.std(overlaps_raw[i])
        overlap_means.append(overlap_mean)
        overlap_stds.append(overlap_std)

        overlap_dims.append(dims_layer)

    layer_width = {}
    for layer in layers:
        if layer != 'full':
            layer_width[layer] = net.state_dict()[layer].shape[0]
    print(layer_width)

    torch.save([overlap_means, overlap_stds, overlap_dims, layer_width, layers], store_file)
    print('save computed result to {}'.format(store_file))

cmap = cm.Set2 # pylint: disable=no-member
i = 0
translate_dict = {}
scconv_i, conv_i, fc_i = 1, 1, 1
print(layers)
for layer in layers:
    if 'conv' in layer or 'feature' in layer:
        translate_dict[layer] = 'conv{}'.format(conv_i)
        conv_i += 1
    elif 'shortcut' in layer:
        translate_dict[layer] = 'sc-conv{}'.format(scconv_i)
        scconv_i += 1
    else:
        translate_dict[layer] = 'fc{}'.format(fc_i)
        fc_i += 1

# single plot
xmax = 2000
if True:
    for i, layer in enumerate(layers):
        try:
            if len(overlap_dims[i]) == 0:
                continue
            plt.figure(figsize=(3, 3))
            cap = min(layer_width[layer] * 3, xmax)
            plt.plot(overlap_dims[i][:cap], overlap_means[i][:cap])
            plt.fill_between(overlap_dims[i][:cap], overlap_means[i][:cap] + overlap_stds[i][:cap], overlap_means[i][:cap] - overlap_stds[i][:cap], alpha=0.2)
            bottom, top = plt.ylim() 
            plt.ylim([0, top])
            plt.xlabel('Dimension', fontsize=12)
            plt.xlim([0, min(xmax, max(overlap_dims[i][:cap]))])
            plt.axvline(layer_width[layer], linestyle=":", label=r"Output Dim: {}".format(layer_width[layer]))
            plt.legend(loc=4)
            plt.tight_layout()
            image_path = get_image_path(exp_name + "_{}".format(translate_dict[layer]), 'pdf')
            plt.savefig(image_path, bbox_inches='tight')
        except:
            continue

# stitched plot
if False:
    plt.figure(figsize=(20, 3))
    gs = gridspec.GridSpec(1, len(layers))
    for i, layer in enumerate(layers):
        plt.subplot(gs[0, i])

        cap = layer_width[layer] * 3
        plt.plot(overlap_dims[i][:cap], overlap_means[i][:cap])
        plt.fill_between(overlap_dims[i][:cap], overlap_means[i][:cap] + overlap_stds[i][:cap], overlap_means[i][:cap] - overlap_stds[i][:cap], alpha=0.2)
        bottom, top = plt.ylim() 
        plt.ylim([0, top])
        plt.axvline(layer_width[layer], linestyle=":")
    
        plt.xlabel('Dimension', fontsize=12)
        plt.title(r'{} $m={}$'.format(translate_dict[layer], layer_width[layer]))

        plt.tight_layout(pad=0.2)
    image_path = get_image_path(exp_name + "_appendix_full", 'pdf')
    plt.savefig(image_path, bbox_inches='tight')


if False:
    plt.figure(figsize=(8, 12))
    print(len(layers))
    gs = gridspec.GridSpec(7, 3)
    for i, layer in enumerate(layers):
        try:
            plt.subplot(gs[i//3, i%3])

            cap = layer_width[layer] * 3
            plt.plot(overlap_dims[i][:cap], overlap_means[i][:cap], label=r'{} $m={}$'.format(translate_dict[layer], layer_width[layer]))
            plt.fill_between(overlap_dims[i][:cap], overlap_means[i][:cap] + overlap_stds[i][:cap], overlap_means[i][:cap] - overlap_stds[i][:cap], alpha=0.2)
            bottom, top = plt.ylim() 
            plt.ylim([0, top])
            plt.legend(loc=4)
            plt.axvline(layer_width[layer], linestyle=":")
            if i//3 == 2:
                plt.xlabel('Dimension', fontsize=12)
            if i%3 == 0:
                plt.ylabel('Overlap', fontsize=12)
            # plt.title()
            plt.tight_layout(pad=0.2)
        except:
            print("Failed at layer {}".format(layer[:-7]))
            continue
    image_path = get_image_path(exp_name + "_response_full", 'pdf')
    plt.savefig(image_path, bbox_inches='tight')

if True:

    cols = 3
    plt.figure(figsize=(2.4 * cols, 2.8 * len(layers) // cols))
    print(len(layers))
    gs = gridspec.GridSpec(len(layers) // cols, cols)
    for i, layer in enumerate(layers):
        # try:
        plt.subplot(gs[i // cols, i % cols])

        cap = layer_width[layer] * 3
        plt.plot(overlap_dims[i][:cap], overlap_means[i][:cap], label=r'{} $m={}$'.format(translate_dict[layer], layer_width[layer]))
        plt.fill_between(overlap_dims[i][:cap], overlap_means[i][:cap] + overlap_stds[i][:cap], overlap_means[i][:cap] - overlap_stds[i][:cap], alpha=0.2)
        bottom, top = plt.ylim() 
        plt.ylim([0, top])
        plt.legend(loc=4, fontsize=12)
        plt.axvline(layer_width[layer], linestyle=":")
        if i // cols == (len(layers) - 1) // cols:
            plt.xlabel('Dimension', fontsize=12)
        # plt.title()
        plt.tight_layout(pad=0.2)
        # except:
            # print("Failed at layer {}".format(layer[:-7]))
            # continue

            
    image_path = get_image_path(exp_name + "_appendix_vertical_{}col".format(cols), 'pdf')
    plt.savefig(image_path, bbox_inches='tight')

# Zoomed Stacked In Plots
if False:
    x_max_zoomed = 20
    for i, layer in enumerate(layers):
        try:
            if len(overlap_dims[i]) == 0:
                continue
            plt.figure(figsize=(3, 6))
            
            cap = min(layer_width[layer] * 3, x_max_zoomed)
            plt.plot(overlap_dims[i][:cap], overlap_means[i][:cap])
            plt.fill_between(overlap_dims[i][:cap], overlap_means[i][:cap] + overlap_stds[i][:cap], overlap_means[i][:cap] - overlap_stds[i][:cap], alpha=0.2)
            bottom, top = plt.ylim() 
            plt.ylim([0, top])
            plt.xlabel('Dimension', fontsize=12)
            plt.xlim([0, min(xmax, max(overlap_dims[i][:cap]))])
            plt.axvline(layer_width[layer], linestyle=":", label=r"Output Dim: {}".format(layer_width[layer]))
            plt.legend(loc=4)
            plt.tight_layout()
            image_path = get_image_path(exp_name + "_{}_top20".format(translate_dict[layer]), 'pdf')
            plt.savefig(image_path, bbox_inches='tight')
        except:
            continue

# Stack Zoomed
if True:
    for i, layer in enumerate(layers):
        try:
            if len(overlap_dims[i]) == 0:
                continue
            plt.figure(figsize=(3, 6))
            
            plt.subplot(211)
            cap = min(layer_width[layer] * 3, xmax)
            plt.plot(overlap_dims[i][:cap], overlap_means[i][:cap])
            plt.fill_between(overlap_dims[i][:cap], overlap_means[i][:cap] + overlap_stds[i][:cap], overlap_means[i][:cap] - overlap_stds[i][:cap], alpha=0.2)
            bottom, top = plt.ylim() 
            plt.ylim([0, top])
            plt.xlabel('Dimension', fontsize=12)
            plt.xlim([0, min(xmax, max(overlap_dims[i][:cap]))])
            plt.axvline(layer_width[layer], linestyle=":", label=r"Output Dim: {}".format(layer_width[layer]))
            plt.legend(loc=4)

            plt.subplot(212)
            cap = min(layer_width[layer] * 3, xmax, 30)
            plt.plot(overlap_dims[i][:cap], overlap_means[i][:cap])
            plt.fill_between(overlap_dims[i][:cap], overlap_means[i][:cap] + overlap_stds[i][:cap], overlap_means[i][:cap] - overlap_stds[i][:cap], alpha=0.2)
            bottom, top = plt.ylim() 
            plt.ylim([0, top])
            plt.xlabel('Dimension (Zoom In)', fontsize=12)
            plt.xlim([0, min(xmax, max(overlap_dims[i][:cap]))])
            plt.axvline(layer_width[layer], linestyle=":", label=r"Output Dim: {}".format(layer_width[layer]))
            plt.legend(loc=4)

            plt.tight_layout()
            image_path = get_image_path(exp_name + "_{}_zoom_stacked".format(translate_dict[layer]), 'pdf')
            plt.savefig(image_path, bbox_inches='tight')
        except:
            continue