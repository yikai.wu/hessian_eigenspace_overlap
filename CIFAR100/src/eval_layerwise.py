import torch
from test import test_model
from torch.utils.data import DataLoader
import numpy as np 
from collections import OrderedDict
import sys, os, argparse
from datetime import datetime

from config import Config # pylint: disable=no-name-in-module
import algos.hessian_eigenthings as hessian_eigenthings
import algos.pyhessian as pyhessian
import algos.closeformhessian.dp as dp
import utils
import pynvml

def gpu_memory(handle, info=""):
    try:
        mem_info = pynvml.nvmlDeviceGetMemoryInfo(handle)
        print("| GPU RAM {:.4g}% | Total {} | {} |\t {}".format(100 * mem_info.used / mem_info.total, mem_info.total, pynvml.nvmlDeviceGetName(handle), info))
    except:
        return

def minibatch_loss_eval(model_path, use_train_set=True):

    start_time = datetime.now()

    assert os.path.isfile(model_path)
    net, device, _ = utils.prepare_net(conf.net(), use_gpu=True)
    net_dict = torch.load(model_path, map_location=device)
    net.load_state_dict(net_dict)
    criterion = conf.criterion

    dataset = conf.dataset(train=use_train_set, transform=conf.test_transform)
    dataloader = DataLoader(dataset, batch_size=conf.testing_batchsize, shuffle=False, num_workers=2)
    enumerator = enumerate(dataloader, 0)
    net.zero_grad()

    loss = 0.0
    corrected, total = 0, 0

    for _, data in enumerator:

        inputs, labels = data
        inputs, labels = inputs.to(device), labels.to(device)

        outputs = net(inputs)
        loss += criterion(outputs, labels).item()
        _, predicted = torch.Tensor.max(outputs.data, 1)
        corrected += (predicted == labels).sum().item()
        total += labels.size(0)
    
    loss_ret = loss / len(dataloader)
    accuracy = corrected / total
    del net

    end_time = datetime.now()
    print("\nDONE: Full batch {}ing loss.\t Took {}\t Log {}\n".format('train' if use_train_set else 'test', end_time - start_time, end_time))

    return None, None, None, loss_ret, accuracy


def eigenthings_calc(model_path, topn_eigenthings, eigenval_criteria='LM', eigenthings_mode="lanczos", max_samples_count=1024, tol=1e-6, filterwise_normalization=False):
    
    start_time = datetime.now()
    
    assert eigenthings_mode in ["lanczos", "power_iter"]
    assert os.path.isfile(model_path)
    net, device, _ = utils.prepare_net(conf.net(), use_gpu=True)
    net_dict = torch.load(model_path, map_location=device)
    net.load_state_dict(net_dict)

    net.eval()
    
    dataset = conf.dataset(train=True, transform=conf.test_transform)
    dataloader = dp.OnDeviceDataLoader(dataset, batchsize=conf.testing_batchsize, device=device, shuffle=True)
    if eigenthings_mode == 'lanczos':
        eigenvals, eigenvecs = hessian_eigenthings.compute_hessian_eigenthings(net, dataloader, conf.criterion, topn_eigenthings, mode=eigenthings_mode, max_samples=max_samples_count, which=eigenval_criteria, tol=tol, filterwise_normalization=filterwise_normalization)
    else:
        eigenvals, eigenvecs = hessian_eigenthings.compute_hessian_eigenthings(net, dataloader, conf.criterion, topn_eigenthings, mode=eigenthings_mode, max_samples=max_samples_count, filterwise_normalization=filterwise_normalization)
    if len(eigenvals) > 1:
        if eigenvals[1] > eigenvals[0]:
            eigenvals = eigenvals[::-1]
            eigenvecs = eigenvecs[::-1]
    
    end_time = datetime.now()
    save_dirc = "{}_LW_part_t{}_full".format(model_path, topn_eigenthings)
    torch.save([eigenvals, eigenvecs], save_dirc)
    print(eigenvals)
    print("\nDONE: Top {} Eigenthings.\t Took {}\t Log {}\n".format(topn_eigenthings, end_time - start_time, end_time))
    return eigenvals, eigenvecs

def eigenthings_calc_layer(model_path, topn_eigenthings, eigenval_criteria='LM', eigenthings_mode="lanczos", layer_method='block', max_samples_count=1024, tol=1e-6, top_layers=4):
    
    start_time = datetime.now()
    
    assert eigenthings_mode in ["lanczos", "power_iter"]
    assert os.path.isfile(model_path)
    net, device, handle = utils.prepare_net(conf.net(), use_gpu=True)
    net_dict = torch.load(model_path, map_location=device)
    net.load_state_dict(net_dict)

    net.eval()
    
    dataset = conf.dataset(train=True, transform=conf.test_transform)
    dataloader = dp.OnDeviceDataLoader(dataset, batchsize=conf.testing_batchsize, device=device, shuffle=True)
    # dataloader = DataLoader(dataset, batch_size=conf.testing_batchsize, shuffle=True, num_workers=conf.test_provider_count)
    results = []

    layerinfo_dict = OrderedDict()
    eigenval_dict = OrderedDict()
    eigenvec_dict = OrderedDict()
    layer_num = 0
    for _ in net.named_parameters():
        layer_num += 1
    
    partition = hessian_eigenthings.get_partition(net)

    for i, (var_name, var_param) in enumerate(net.named_parameters()):
        if 'bias' in var_name:
            continue
        if i >= top_layers:
            continue
        start_time_layer = datetime.now()
        start, end = partition[var_name]
        print("\nComputing Hessian Layer Block {} Top {} Eigenthings. {} / {}".format(var_name, topn_eigenthings, i + 1, layer_num), flush=True)
        layerinfo_dict[var_name] = (start, end)
        eigennum = min(end - start, topn_eigenthings)
        if eigenthings_mode == 'lanczos':
            eigenvals, eigenvecs = hessian_eigenthings.compute_hessian_eigenthings_layer(net, dataloader, conf.criterion, eigennum, mode=eigenthings_mode, max_samples=max_samples_count, which=eigenval_criteria, start=start, end=end, method=layer_method, tol=tol)
        else:
            eigenvals, eigenvecs = hessian_eigenthings.compute_hessian_eigenthings_layer(net, dataloader, conf.criterion, eigennum, mode=eigenthings_mode, max_samples=max_samples_count, start=start, end=end, method=layer_method, tol=tol)
        if len(eigenvals) > 1:
            if eigenvals[1] > eigenvals[0]:
                eigenvals = eigenvals[::-1]
                eigenvecs = eigenvecs[::-1]
        print(eigenvals[:10], flush=True)
        print(eigenvecs.shape, flush=True)
        eigenval_dict[var_name] = eigenvals
        eigenvec_dict[var_name] = eigenvecs
        gpu_memory(handle, 'comp done')
        gpu_memory(handle, 'memory clear')
        save_dirc = "{}_LW_part_t{}_layer{}".format(model_path, topn_eigenthings, var_name)
        torch.save([eigenvals, eigenvecs], save_dirc)
        print("DONE: Took {}\t Log {}, saved to {}\n".format(datetime.now() - start_time_layer, datetime.now(), save_dirc), flush=True)
    end_time = datetime.now()
    print("\nDONE: Layer-wise Top {} Eigenthings.\t Took {}\t Log {}\n".format(topn_eigenthings, end_time - start_time, end_time), flush=True)
    return layerinfo_dict, eigenval_dict, eigenvec_dict

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-mdir', '--modeldir', type=str, default=None)
    parser.add_argument('-m', '--modelpath', type=str, help='The indicator of run (default=1)', default=None)
    parser.add_argument('-n', '--topneigenthings', type=int, help='Number of eigenvalues to calculate', default=10)
    parser.add_argument('-t', '--topnlayers', type=int, help='Number of layers to calculate', default=100)
    parser.add_argument('-e', '--epoch', type=int, default=os.getenv('SLURM_ARRAY_TASK_ID'))
    parser.add_argument('-em', type=str, default='lanczos')
    parser.add_argument('-tol', type=float, default=1e-6)
    parser.add_argument('-fn', action="store_true")

    args = parser.parse_args()
    conf = Config()

    if args.modelpath is not None:
        assert os.path.isfile(args.modelpath)
        model_path = args.modelpath
    elif args.modeldir is not None:
        assert os.path.isdir(args.modeldir)
        model_path = os.path.join(args.modeldir, "epoch{}.pth".format(args.epoch))
    else:
        print("invalid arguments")
    print(model_path)
    # model_path = "train_log/cifar10_landscape_exp/LeNetProjection/LeNet_momentum0.9/run_eigentrend/models/epoch300.pth"
    assert os.path.isfile(model_path)
    out_dict = dict()
    out_dict['epoch'] = args.epoch
    print(args)
    net_tmp = conf.net()
    sd = torch.load(model_path, map_location='cpu')
    net_tmp.load_state_dict(sd)
    sd_vec = utils.net2vec(net_tmp)
    out_dict["model_sd"] = sd
    out_dict["model_vect"] = sd_vec

    print('computing top {} layer-wise eigenthings'.format(args.topneigenthings))
    layerinfo_dict, eigenvals_layer, eigenvecs_layer = eigenthings_calc_layer(model_path, args.topneigenthings, eigenthings_mode=args.em, tol=args.tol, top_layers=args.topnlayers)
    out_dict['layerinfo'] = layerinfo_dict
    out_dict['eigenvals_layer'] = eigenvals_layer
    out_dict['eigenvecs_layer'] = eigenvecs_layer

    eigenvals, eigenvecs = eigenthings_calc(model_path, args.topneigenthings, eigenthings_mode=args.em, tol=args.tol, filterwise_normalization=False)
    out_dict['eigenvals'] = eigenvals
    out_dict['eigenvecs'] = eigenvecs

    name_attr = "_LW_part_t{}_l{}".format(args.topnlayers, args.topneigenthings)
    name_attr += '.eval'

    save_dirc = model_path + name_attr
    torch.save(out_dict, save_dirc)
    print("saved to {}".format(save_dirc))