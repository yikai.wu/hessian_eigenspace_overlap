import os, sys
import torch
import numpy as np
from algos.closeformhessian import hessianmodule
from evaluations import component_compare, overlap_analysis
from tools import file_fetch
import visualization as vis

import numpy as np
import matplotlib as mpl
mpl.use('Agg')
from matplotlib import cm
from matplotlib import gridspec
import matplotlib.pyplot as plt
from matplotlib import rc

from IPython import embed

rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)
params= {'text.latex.preamble' : [r'\usepackage{amsmath}',r'\usepackage{amssymb}', r'\usepackage{bm}']}

plt.style.use('seaborn-deep')
plt.rcParams.update(params)


# T600
dirc = '../MNIST_Binary/experiments/FC1_600_sgd0.01m0.9LS_l1d_pic01_labelpn1_bt100'
path_vanilla = '../../eigen_pac_bayes/tmp_log/sigma_post/run_FC1_600.pth'
path_optim = '../../eigen_pac_bayes/tmp_log/sigma_post/hessian_approx_FC1_600_final.pth'
path_iter = '../../eigen_pac_bayes/tmp_log/sigma_post/hessian_iterative_FC1_600_10.pth'

# FC2 10class
dirc = '../MNIST_Exp1/experiments/FC2_fixlr0.01'
path_vanilla = '../../eigen_pac_bayes/tmp_log/sigma_post/run_10class_FC2_final.pth'
path_optim = '../../eigen_pac_bayes/tmp_log/sigma_post/hessian_approx_10class_FC2_final.pth'
path_iter = '../../eigen_pac_bayes/tmp_log/sigma_post/hessian_iterative_10class_FC2_10.pth'
path_iterH = '../../eigen_pac_bayes/tmp_log/sigma_post/hessian_iterative_10class_FC2_10_test.pth'

# FC2-600 10class
# dirc = '../MNIST_Exp1/experiments/FC2_600_fixlr0.01'
# path_vanilla = '../../eigen_pac_bayes/tmp_log/sigma_post/run_10class_FC2_600_final.pth'
# path_optim = '../../eigen_pac_bayes/tmp_log/sigma_post/hessian_approx_10class_FC2_600_final.pth'

assert os.path.isdir(dirc)
sys.path.append(dirc)
from config import Config # pylint: disable=no-name-in-module
conf = Config()
net = conf.net()

comp_layers = ['fc1', 'fc2']

def get_layer_info(sd):
    s = 0
    e = 0
    ret = []
    for k in (sd):
        e += sd[k].view(-1).size()[0]
        print(s, e, k)
        ret.append([s, e, k])
        s = e
    return ret

layer_info = get_layer_info(net.state_dict())
exp_name = "Sigma_post_" + dirc.split('/')[-3] + dirc.split('/')[-1]


def sigma_post_plot_hessian(sigma_file):
    data = torch.load(sigma_file)
    eigenvals = data['eigenvals']
    sigma_post = data['sigma_post']
    res = {}
    for layer in layer_info:
        s, e, k = layer[0], layer[1], layer[2]
        if k[:-7] not in comp_layers:
            continue
        print(k, flush=True)
        name = 'sigma_post_hessian_{}_{}'.format(exp_name, k)
        eig = eigenvals[s:e]
        sig = sigma_post[s:e]
        eig, ind = torch.sort(eig, descending=True) # pylint: disable=no-member
        sig = sig[ind]
        dim_x = torch.range(1, sig.shape[0]) # pylint: disable=no-member
        dim_y = torch.Tensor.exp(sig * 2)

        plt.figure(figsize=(4, 3), dpi=600)
        # plt.xlabel(r"$i$")
        # plt.ylabel(r"$\sigma_i$")
        plt.ylabel(r"Variance $e^{2\sigma}$")
        plt.yscale('log')
        plt.scatter(dim_x, dim_y, s=1, rasterized=True)
        
        image_path = vis.get_image_path("Optim_" + exp_name + "_{}".format(k[:-7]), store_format='pdf')
        plt.tight_layout()
        plt.savefig(image_path, dpi=600, bbox_inches='tight')

def sigma_post_plot_standard(sigma_file):
    sigma_post= torch.load(sigma_file)
    res = {}
    for layer in layer_info:
        s, e, k = layer[0], layer[1], layer[2]
        if k[:-7] not in comp_layers:
            continue
        print(k, flush=True)
        name = 'sigma_post_standard_{}_{}'.format(exp_name, k)
        sig = sigma_post[s:e]
        dim_x = np.arange(1, sig.shape[0] + 1) # pylint: disable=no-member
        # dim_y = sig
        dim_y = torch.Tensor.exp(sig * 2)
        print(dim_x, dim_y)

        plt.figure(figsize=(4, 3), dpi=600)
        # plt.xlabel(r"$i$")
        plt.ylabel(r"Variance $e^{2\sigma}$")
        # plt.ylabel(r"$\sigma_i$")
        plt.scatter(dim_x, dim_y, s=1, rasterized=True)
        plt.yscale('log')
        
        image_path = vis.get_image_path("Vanilla_" + exp_name + "_{}".format(k[:-7]), store_format='pdf')
        plt.tight_layout()
        plt.savefig(image_path, dpi=600, bbox_inches='tight')


def sigma_post_plot_compare(sigma_file_vanilla, sigma_file_optim):
    sigma_post_vanilla = torch.load(sigma_file_vanilla)
    data = torch.load(sigma_file_optim)
    sigma_post_optim = data['sigma_post']
    eigenvals_optim = data['eigenvals']
    res = {}
    for layer in layer_info:
        s, e, k = layer[0], layer[1], layer[2]
        if k[:-7] not in comp_layers:
            continue
        print(k, flush=True)
        name = 'sigma_post_compare_{}_{}'.format(exp_name, k)
        fig, axes = plt.subplots(1, 2, sharey=True, dpi=600, figsize=(8, 3))

        # Vanilla
        sig = sigma_post_vanilla[s:e]
        dim_x = np.arange(1, sig.shape[0] + 1) # pylint: disable=no-member
        dim_y = torch.Tensor.exp(sig * 2)

        axes[0].set_ylabel(r"Variance $e^{2\sigma}$")
        axes[0].scatter(dim_x, dim_y, s=1, rasterized=True)
        axes[0].set_yscale('log')
        axes[0].set_title(r'Optimized without Hessian Information')

        # OPtim
        eig = torch.Tensor.abs(eigenvals_optim[s:e])
        sig = sigma_post_optim[s:e]
        eig, ind = torch.sort(eig, descending=True) # pylint: disable=no-member
        sig = sig[ind]
        dim_x = torch.range(1, sig.shape[0]) # pylint: disable=no-member
        dim_y = torch.Tensor.exp(sig * 2)

        axes[1].scatter(dim_x, dim_y, s=1, rasterized=True)
        axes[1].set_yscale('log')
        axes[1].set_title(r'Optimized with Hessian Information')

        image_path = vis.get_image_path(name, store_format='pdf')
        plt.tight_layout()
        plt.savefig(image_path, dpi=600, bbox_inches='tight')

def sigma_post_plot_compare_iter(sigma_file_vanilla, sigma_file_optim, sigma_file_iter):
    sigma_post_vanilla = torch.load(sigma_file_vanilla)
    data = torch.load(sigma_file_optim)
    data_iter = torch.load(sigma_file_iter)
    sigma_post_optim = data['sigma_post']
    eigenvals_optim = data['eigenvals']
    sigma_post_optim_iter = data_iter['sigma_post']
    eigenvals_optim_iter = data_iter['eigenvals']
    res = {}
    for layer in layer_info:
        s, e, k = layer[0], layer[1], layer[2]
        if k[:-7] not in comp_layers:
            continue
        print(k, flush=True)
        name = 'sigma_post_compare_iter_{}_{}'.format(exp_name, k)
        fig, axes = plt.subplots(1, 3, sharey=True, dpi=600, figsize=(10, 3))

        # Vanilla
        sig = sigma_post_vanilla[s:e]
        dim_x = np.arange(1, sig.shape[0] + 1) # pylint: disable=no-member
        dim_y = torch.Tensor.exp(sig * 2)

        axes[0].set_ylabel(r"Variance $\bm{s}$")
        axes[0].scatter(dim_x, dim_y, s=1, rasterized=True)
        axes[0].set_yscale('log')
        axes[0].set_title(r'Optimized without Hessian')

        # OPtim
        eig = torch.Tensor.abs(eigenvals_optim[s:e])
        sig = sigma_post_optim[s:e]
        eig, ind = torch.sort(eig, descending=True) # pylint: disable=no-member
        sig = sig[ind]
        dim_x = torch.range(1, sig.shape[0]) # pylint: disable=no-member
        dim_y = torch.Tensor.exp(sig * 2)

        axes[1].scatter(dim_x, dim_y, s=1, rasterized=True)
        axes[1].set_yscale('log')
        axes[1].set_title(r'Optimized with Hessian')
        
        eig = torch.Tensor.abs(eigenvals_optim_iter[s:e])
        sig = sigma_post_optim_iter[s:e]
        eig, ind = torch.sort(eig, descending=True) # pylint: disable=no-member
        sig = sig[ind]
        dim_x = torch.range(1, sig.shape[0]) # pylint: disable=no-member
        dim_y = torch.Tensor.exp(sig * 2)

        axes[2].scatter(dim_x, dim_y, s=1, rasterized=True)
        axes[2].set_yscale('log')
        axes[2].set_title(r'Optimized with Iterative Hessian')

        image_path = vis.get_image_path(name, store_format='pdf')
        plt.tight_layout(pad=1.02)
        plt.savefig(image_path, dpi=600, bbox_inches='tight')

def sigma_post_plot_compare_iter_iterH(sigma_file_vanilla, sigma_file_optim, sigma_file_iter, sigma_file_iterH):
    sigma_post_vanilla = torch.load(sigma_file_vanilla)
    data = torch.load(sigma_file_optim)
    data_iter = torch.load(sigma_file_iter)
    data_iterH = torch.load(sigma_file_iterH)
    sigma_post_optim = data['sigma_post']
    eigenvals_optim = data['eigenvals']
    sigma_post_optim_iter = data_iter['sigma_post']
    eigenvals_optim_iter = data_iter['eigenvals']
    sigma_post_optim_iterH = data_iterH['sigma_post']
    eigenvals_optim_iterH = data_iterH['eigenvals']
    res = {}
    for layer in layer_info:
        s, e, k = layer[0], layer[1], layer[2]
        if k[:-7] not in comp_layers:
            continue
        print(k, flush=True)
        name = 'sigma_post_compare_iter_iterH_{}_{}'.format(exp_name, k)
        fig, axes = plt.subplots(1, 4, sharey=True, dpi=600, figsize=(10, 2.5))

        # Vanilla
        sig = sigma_post_vanilla[s:e]
        dim_x = np.arange(1, sig.shape[0] + 1) # pylint: disable=no-member
        dim_y = torch.Tensor.exp(sig * 2)

        axes[0].set_ylabel(r"Variance $\bm{s}$")
        axes[0].scatter(dim_x, dim_y, s=1, rasterized=True)
        axes[0].set_yscale('log')
        axes[0].set_title(r'\textsc{Base}')

        # OPtim
        eig = torch.Tensor.abs(eigenvals_optim[s:e])
        sig = sigma_post_optim[s:e]
        eig, ind = torch.sort(eig, descending=True) # pylint: disable=no-member
        sig = sig[ind]
        dim_x = torch.range(1, sig.shape[0]) # pylint: disable=no-member
        dim_y = torch.Tensor.exp(sig * 2)

        axes[1].scatter(dim_x, dim_y, s=1, rasterized=True)
        axes[1].set_yscale('log')
        axes[1].set_title(r'\textsc{Appr}')
        
        eig = torch.Tensor.abs(eigenvals_optim_iter[s:e])
        sig = sigma_post_optim_iter[s:e]
        eig, ind = torch.sort(eig, descending=True) # pylint: disable=no-member
        sig = sig[ind]
        dim_x = torch.range(1, sig.shape[0]) # pylint: disable=no-member
        dim_y = torch.Tensor.exp(sig * 2)

        axes[2].scatter(dim_x, dim_y, s=1, rasterized=True)
        axes[2].set_yscale('log')
        axes[2].set_title(r'\textsc{Iter}')

        eig = torch.Tensor.abs(eigenvals_optim_iterH[s:e])
        sig = sigma_post_optim_iter[s:e]
        eig, ind = torch.sort(eig, descending=True) # pylint: disable=no-member
        sig = sig[ind]
        dim_x = torch.range(1, sig.shape[0]) # pylint: disable=no-member
        dim_y = torch.Tensor.exp(sig * 2)

        axes[3].scatter(dim_x, dim_y, s=1, rasterized=True)
        axes[3].set_yscale('log')
        axes[3].set_title(r'\textsc{Iter.M}')

        image_path = vis.get_image_path(name, store_format='pdf')
        plt.tight_layout(pad=1.02)
        plt.savefig(image_path, dpi=600, bbox_inches='tight')

# sigma_post_plot_standard(path_vanilla)
# sigma_post_plot_hessian(path_optim)

# sigma_post_plot_compare(path_vanilla, path_optim)
# sigma_post_plot_compare_iter(path_vanilla, path_optim, path_iter)
sigma_post_plot_compare_iter_iterH(path_vanilla, path_optim, path_iter, path_iterH)