import os, sys
import torch
import numpy as np
from algos.cfhag import hessianmodule
from evaluations import component_compare, confidence_compare, eigenspace_est, full_hessian, overlap_analysis, utau_eig_propagation
from tools import file_fetch

from torch.utils.data import DataLoader
from torch.nn import functional, Conv2d

from algos.cfhag.visualization import vis
from algos.cfhag.measures import Measures
from algos.cfhag.layerwisefeature import IntermediateFeatureExtractor, rgetattr
from algos.cfhag.dp import OnDeviceDataLoader
from algos.cfhag.utils import *
import visualization as vis

import numpy as np
import matplotlib as mpl
from matplotlib import rc
from matplotlib import cm
from matplotlib import gridspec
import matplotlib.pyplot as plt
mpl.use('Agg')
plt.style.use('ggplot')
rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)
params= {'text.latex.preamble' : [r'\usepackage{amsmath}',r'\usepackage{amssymb}', r'\usepackage{bm}']}
plt.rcParams.update(params)

dirc = '../CIFAR100/experiments/VGG11W48New_nobn_fixlr0.01'
dirc = '../CIFAR10_Exp1/experiments/VGG11W200_fxlr0.01'
topn = 150
topn = 400
epoch = -1
run = 1

dev = 'cuda' if torch.cuda.is_available() else 'cpu'

storage_path = os.path.join(dirc, 'experiment_log/run_{}'.format(run))

load_evals = True
remain_labels = None
load_prev = True
crop_ratio = 1
layer_seq = ['conv1', 'conv2', 'fc1', 'fc2', 'fc3']
comp_layers = ['conv1', 'conv2']

# comp_layers = ['conv1', 'layer1.0.conv1', 'layer1.0.conv2', 'layer1.1.conv1', 'layer1.1.conv2', 'layer2.0.conv1', 'layer2.0.conv2', 'layer2.0.shortcut.0', 'layer2.1.conv1', 'layer2.1.conv2', 'layer3.0.conv1', 'layer3.0.conv2', 'layer3.1.conv1', 'layer3.1.conv2', 'layer4.0.conv1', 'layer4.0.conv2', 'layer4.1.conv1', 'layer4.1.conv2']#, 'linear']
layer_seq = ['features.0', 'features.3', 'features.6', 'features.8', 'features.11', 'features.13', 'features.16', 'features.18']#, 'classifier']
comp_layers = ['features.0', 'features.3', 'features.6', 'features.8', 'features.11', 'features.13', 'features.16', 'features.18']#, 'classifier']


snapshot_file = file_fetch.fetch_snapshot(dirc, epoch=epoch, run_name=run)
log_format = 'final.pth_LW_part_t{}_layer{}.weight'

eigenvals_real = {}
eigenvecs_real = {}

for layer in layer_seq:
    log_name = log_format.format(topn, layer)
    log_dirc = os.path.join(dirc, 'experiment_log', 'run_{}'.format(run), 'models', log_name)
    if os.path.isfile(log_dirc):
        meta = torch.load(log_dirc, map_location='cpu')
        eigenvecs_real[layer + '.weight'] = torch.from_numpy(meta[1]).to(dev)
        eigenvals_real[layer + '.weight'] = meta[0]
    else:
        print('Failed at {}'.format(layer))
        continue

assert os.path.isdir(dirc)
sys.path.append(dirc)
from config import Config # pylint: disable=no-name-in-module
conf = Config()

dev = 'cuda' if torch.cuda.is_available() else 'cpu'

net = conf.net()
net.load_state_dict(torch.load(snapshot_file, map_location='cuda' if torch.cuda.is_available() else 'cpu'))
dataset = conf.dataset(train=True, transform=conf.test_transform)
HM = hessianmodule.HessianModule(net, dataset, layer_seq, RAM_cap=64, remain_labels=remain_labels)

exp_name = dirc.split('/')[-3] + '_' + dirc.split('/')[-1] + "_E{}".format(epoch)
dataset = conf.dataset(train=True, transform=conf.test_transform)
if remain_labels is not None:
    exp_name += '_y' + ''.join([str(i) for i in remain_labels]) # pylint: disable=not-an-iterable

xxT_snapshot = snapshot_file + "xxT_Conv"
if os.path.isfile(xxT_snapshot) and load_prev:
    xxT_conv = torch.load(xxT_snapshot, map_location=dev)
    print('Loaded Conv xxT from previous computed result')
else:
    xxT_conv = HM.expectation(HM.decomp.xxTC_comp, comp_layers)
    torch.save(xxT_conv, xxT_snapshot)
UTAU_snapshot = snapshot_file + "UTAU_Conv"
if os.path.isfile(UTAU_snapshot) and load_prev:
    UTAU_conv = torch.load(UTAU_snapshot, map_location=dev)
    print('Loaded Conv UTAU from previous computed result')
else:
    UTAU_conv = HM.expectation(HM.decomp.UCTAUC_comp, comp_layers, batchsize=256)
    torch.save(UTAU_conv, UTAU_snapshot)
# component_compare.matvis_E_computed(HM, comp_layers, [UTAU_conv, xxT_conv], ['UTAU', 'xxT'], exp_name + 'ConvMX')

def plot_single(name, mat, title=None):
    
    plt.figure(figsize=(6, 3), dpi=600)
    plt.imshow(mat.cpu(), cmap='Reds', interpolation='nearest')
    plt.grid(False)
    plt.box(on=None)
    # for direction in ["top", "bottom", "left", "right"]:
    #     axes[0].spines[direction].set_visible(False)
    # plt.title(title)
    for spine in plt.gca().spines.values():
        spine.set_visible(False)
        print(spine)
    image_path = vis.get_image_path(name, store_format='pdf')
    plt.tight_layout()
    plt.savefig(image_path, bbox_inches='tight')

def plot_all(name, mats, eigenvals, title=None):
    
    plt.figure(figsize=(36, 18), dpi=600)
    labels = [['xxT real', 'xxT approx'], ['UTAU real', 'UTAU approx']]
    labels_eigenval = ['xxT', 'UTAU']
    gs = gridspec.GridSpec(2, 5)
    for i in range(2):
        for j in range(2):
            plt.subplot(gs[i, j])
            plt.imshow(mats[i][j].cpu(), cmap='Reds', interpolation='nearest')
            plt.grid(False)
            plt.box(on=None)
            # for direction in ["top", "bottom", "left", "right"]:
            #     axes[0].spines[direction].set_visible(False)
            # plt.title(title)
            for spine in plt.gca().spines.values():
                spine.set_visible(False)
            plt.title(labels[i][j])
        for j in range(2):
            plt.subplot(gs[i, j + 2])
            plt.imshow(mats[i][j].cpu()[:20, :20], cmap='Reds', interpolation='nearest')
            plt.grid(False)
            plt.box(on=None)
            # for direction in ["top", "bottom", "left", "right"]:
            #     axes[0].spines[direction].set_visible(False)
            # plt.title(title)
            for spine in plt.gca().spines.values():
                spine.set_visible(False)
            plt.title(labels[i][j])
    for i in range(2):
        plt.subplot(gs[i, 4])
        plt.scatter(np.arange(len(eigenvals[i])), eigenvals[i].cpu())
        plt.title('eigenvals ' + labels_eigenval[i])
    plt.suptitle(name)
    image_path = vis.get_image_path(name, store_format='pdf')
    # plt.tight_layout()
    plt.savefig(image_path, bbox_inches='tight')

def correspondance_expand(layer, topn=50):

    device = HM.device
    pic_name = 'est_real_corr_expand_t{}_{}_{}'.format(topn, exp_name, layer)
    E_UTAUs = UTAU_conv
    E_xxTs = xxT_conv
    eigenthings_est = HM.hessian_eigenthings_estimate_precomputed(comp_layers, E_UTAUs, E_xxTs, num_eigenthings=topn)
    
    assert layer in comp_layers
    name = "Pairs_{}_{}_{}".format(exp_name, layer, topn)
    H_eigenvals_est, H_eigenvecs_est, _ = eigenthings_est[layer]
    layer_weight_ind = layer + '.weight'
    print(eigenvecs_real.keys())
    H_eigenvecs = eigenvecs_real[layer_weight_ind] # pylint: disable=no-member

    eigenvals_UTAU, eigenvecs_UTAU = HM.utils.eigenthings_tensor_utils(E_UTAUs[layer], symmetric=True)
    eigenvals_xxT, eigenvecs_xxT = HM.utils.eigenthings_tensor_utils(E_xxTs[layer], symmetric=True)
    b_xxT = eigenvecs_xxT.unsqueeze(0)
    b_UTAU = eigenvecs_UTAU.unsqueeze(0)

    M, N = len(eigenvals_xxT), len(eigenvals_UTAU)
    print(M, N)

    vec_est_mat = torch.cat([H_eigenvecs_est[i].view([N, M]).unsqueeze(0) for i in range(topn)]).to(device) # pylint: disable=no-member
    
    est_UTAU_raw_overlap = torch.Tensor.matmul(b_UTAU, vec_est_mat)# pylint: disable=no-member
    est_UTAU_corr_exp = torch.Tensor.sum(est_UTAU_raw_overlap.square_(), dim=-1, keepdim=False).transpose(0, 1)

    est_xxT_raw_overlap = torch.Tensor.matmul(vec_est_mat, b_xxT.transpose(-1, -2))# pylint: disable=no-member
    est_xxT_corr_exp = torch.Tensor.sum(est_xxT_raw_overlap.square_(), dim=1, keepdim=False).transpose(0, 1)
    
    vec_base_mat = torch.cat([H_eigenvecs[i].view([N, M]).unsqueeze(0) for i in range(topn)]).to(device) # pylint: disable=no-member

    base_UTAU_raw_overlap = torch.Tensor.matmul(b_UTAU, vec_base_mat)# pylint: disable=no-member
    base_UTAU_corr_exp = torch.Tensor.sum(base_UTAU_raw_overlap.square_(), dim=-1, keepdim=False).transpose(0, 1)

    base_xxT_raw_overlap = torch.Tensor.matmul(vec_base_mat, b_xxT.transpose(-1, -2))# pylint: disable=no-member
    base_xxT_corr_exp = torch.Tensor.sum(base_xxT_raw_overlap.square_(), dim=1, keepdim=False).transpose(0, 1) # [:len(base_UTAU_corr_exp)]

    plot_single('UTAU_True' + pic_name, base_UTAU_corr_exp[:topn,:topn])
    plot_single('UTAU_Approx' + pic_name, est_UTAU_corr_exp[:topn,:topn])
    plot_single('xxT_True' + pic_name, base_xxT_corr_exp[:topn,:topn])
    plot_single('xxT_Approx' + pic_name, est_xxT_corr_exp[:topn,:topn])

    mats = [
        [base_xxT_corr_exp, est_xxT_corr_exp],
        [base_UTAU_corr_exp, est_UTAU_corr_exp]
    ]
    eigenvals = [eigenvals_xxT[:200], eigenvals_UTAU[:200]]
    plot_all(pic_name, mats, eigenvals)

def eigenvals_sep(dim=20):

    # E_UTAUs = HM.expectation(HM.decomp.UTAU_comp, comp_layers, dataset_crop=crop_ratio, out_device=HM.device) # , auto_grad=autograd, batchsize=16)
    # E_xxTs = HM.expectation(HM.decomp.xxT_comp, comp_layers, dataset_crop=crop_ratio, out_device=HM.device)

    E_UTAUs = UTAU_conv
    E_xxTs = xxT_conv

    pic_name = 'sigvals_t{}_{}'.format(dim, exp_name)
    for layer in comp_layers:
        print(layer)
        plt.figure(figsize=(3, 3))
        gs = gridspec.GridSpec(1, 2)
        E_UTAU = E_UTAUs[layer]
        E_xxT = E_xxTs[layer]
        eigenvals_UTAU, _ = HM.utils.eigenthings_tensor_utils(E_UTAU, symmetric=True)
        eigenvals_xxT, _ = HM.utils.eigenthings_tensor_utils(E_xxT, symmetric=True)
        vals = eigenvals_UTAU[:dim].cpu()
        vals_full = eigenvals_xxT[:dim].cpu()

        plt.subplot(gs[0, 0])
        plt.scatter(np.arange(1, len(vals) + 1), vals, s=5)
        plt.xlim([-1, dim + 1])
        plt.xticks([0, 10, 20])
        plt.ticklabel_format(axis='y', style='sci', scilimits=[-1, 1], useMathText=False)
        plt.xlabel(r"$\mathbb{E}[\bm{M}]$")

        plt.subplot(gs[0, 1])
        plt.scatter(np.arange(1, len(vals_full) + 1), vals_full, s=5)
        plt.xlim([-1, dim + 1])
        plt.xticks([0, 10, 20])
        plt.ticklabel_format(axis='y', style='sci', scilimits=[-1, 1], useMathText=False)
        plt.xlabel(r"$\mathbb{E}[\bm{x}\bm{x}^T]$")

        image_path = vis.get_image_path(pic_name + "_{}".format(layer), store_format='pdf')
        plt.tight_layout(pad=0.5)
        plt.savefig(image_path, bbox_inches='tight')

# for layer in comp_layers:
#     correspondance_expand(layer, 300)
#     correspondance_expand(layer, 300)
# # correspondance_expand('conv1', 50)
correspondance_expand('features.11', 20)
correspondance_expand('features.13', 20)

eigenvals_sep()