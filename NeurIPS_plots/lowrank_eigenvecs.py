import os, sys
import torch
import numpy as np
from algos.closeformhessian import hessianmodule
from evaluations import component_compare, overlap_analysis
from tools import file_fetch
import visualization as vis
import functools

from torch.nn import functional, Conv2d, Linear

import numpy as np
import matplotlib as mpl
mpl.use('Agg')
from matplotlib import cm
from matplotlib import gridspec
import matplotlib.pyplot as plt
from matplotlib import rc

from IPython import embed
rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)
params= {'text.latex.preamble' : [r'\usepackage{amsmath}',r'\usepackage{amssymb}', r'\usepackage{bm}']}

def rgetattr(obj, attr, *args):
    def _getattr(obj, attr):
        return getattr(obj, attr, *args)
    return functools.reduce(_getattr, [obj] + attr.split('.'))

plt.style.use('seaborn-deep')
# plt.rcParams['xtick.labelsize']= 12
# plt.rcParams['ytick.labelsize']= 12
# plt.rcParams['axes.labelsize'] = 14
# plt.rcParams['axes.titlesize'] = 14
plt.rcParams.update(params)

dirc = '../MNIST_ExpBN/experiments/FC2BN_inpnorm_fixlr0.01'
dirc = '../MNIST_ExpBN/experiments/FC2BN_fixlr0.01'
dirc = '../MNIST_Exp1/experiments/FC2_20_small_fixlr0.01'
dirc = '../MNIST_RandomLabel/experiments/FC2_fixlr0.01_RL'
dirc = '../MNIST_0M/experiments/FC2_200_0M_fixlr0.01'
dirc = '../MNIST_ExpBN/experiments/FC2S_20_BN_fixlr0.01'
dirc = '../MNIST_ExpBN/experiments/FC3BN_nl_fixlr0.01'
dirc = '../CIFAR10_Exp1/experiments/LeNet5_fixlr0.01'
dirc = '../CIFAR10_RandomLabel/experiments/LeNet5_fixlr0.01_RL'
dirc = '../CIFAR10_Exp1/experiments/LeNet5_fixlr0.01'
dirc = '../MNIST_Exp1/experiments/WD_FC8_32_fixlr0.01'
dirc = '../MNIST_Exp1/experiments/WD_FC4_64_fixlr0.01'
dirc = '../CIFAR10_Exp1/experiments/LeNet5_sigmoid_fixlr0.01'
dirc = '../CIFAR10_Exp1/experiments/LeNet5_tanh_fixlr0.01'
dirc = '../CIFAR10_Exp1/experiments/LeNet5_fixlr0.1_dMSE'

dirc = '../CIFAR10_Exp1/experiments/LeNet5_LN_nl_fixlr0.01'
dirc = '../CIFAR10_Exp1/experiments/LeNet5_normnew_fixlr0.01'
epoch = -1
run = 2

storage_path = os.path.join(dirc, 'experiment_log/run_{}'.format(run))

load_evals = False
remain_labels = None
crop_ratio = 1

comp_layers = ['fc1', 'fc2', 'fc3', 'fc4', 'fc5']#, 'fc6', 'fc7', 'fc8', 'fc9']#, 'fc3']
fc_layers = ['fc1', 'fc2', 'fc3', 'fc4', 'fc5']#, 'fc6', 'fc7', 'fc8', 'fc9']#, 'fc3']
# fc_layers = ['fc1', 'fc2', 'fc3']
comp_layers = ['conv1', 'conv2', 'fc1', 'fc2', 'fc3'] #, 'fc4']
fc_layers = ['fc1', 'fc2', 'fc3'] #, 'fc4']

snapshot_file = file_fetch.fetch_snapshot(dirc, epoch=epoch, run_name=run)
eval_file = file_fetch.fetch_eval_file(dirc, epoch=epoch, run_name=run, arg='_LW_ET', dim=1000)


assert os.path.isdir(dirc)
sys.path.append(dirc)
from config import Config # pylint: disable=no-name-in-module
conf = Config()
net = conf.net()
LMs = {layer: rgetattr(net, layer) for layer in comp_layers}
exp_name = dirc.split('/')[-3] + '_' + dirc.split('/')[-1] + "R{}_E{}".format(run, epoch)
eigenthings_real = torch.load(eval_file, map_location='cpu')
eigenvals_real = eigenthings_real['eigenvals_layer']
eigenvecs_real = eigenthings_real['eigenvecs_layer']
# embed()
def norm_ratio(dim=50, topn=1):
    
    name = 'Top_Eigenvector_rank_{}_{}'.format(exp_name, dim)

    square_ratio = {}
    for layer in comp_layers:
        print(layer)
        square_ratio[layer] = []
        layer_weight_ind = layer + '.weight'
        Vs = torch.from_numpy(eigenvecs_real[layer_weight_ind]) # pylint: disable=no-member
        for i in range(min(dim, int(len(Vs) / 2))):
            lm = LMs[layer] # type: Conv2d
            V = Vs[i]
            if isinstance(lm, Linear):
                V = V.view(lm.in_features, lm.out_features)
            else:
                w = lm.kernel_size[0] * lm.kernel_size[1] * lm.in_channels
                V = V.view((w, lm.out_channels))
            _, S, _ = V.svd(compute_uv=False)
            t = S[:topn].square().sum().sqrt()
            square_ratio[layer].append(t.item())
        # print(square_ratio[layer])

    fig, axes = plt.subplots(1, len(comp_layers), sharey=True, dpi=600, figsize=(8, 2))
    for i, layer in enumerate(comp_layers):
        axes[i].title.set_text(layer)
        axes[i].scatter(np.arange(1, len(square_ratio[layer]) + 1), square_ratio[layer], s=8)
        axes[i].set_ylim([0, 1])
        axes[i].set_xlim([0, dim])
    
    image_path = vis.get_image_path(name, store_format='pdf')
    plt.tight_layout()
    plt.savefig(image_path, bbox_inches='tight')

def norm_ratio_multitopn(dim=20, topns=[1, 10]):
    
    name = 'Top_Eigenvector_rank_{}_{}_top1_10'.format(exp_name, dim)

    ratios = {i:{} for i in topns}
    for topn in topns:
        square_ratio = ratios[topn]
        for layer in comp_layers:
            print(layer)
            square_ratio[layer] = []
            layer_weight_ind = layer + '.weight'
            Vs = torch.from_numpy(eigenvecs_real[layer_weight_ind]) # pylint: disable=no-member
            for i in range(min(dim, int(len(Vs) / 2))):
                lm = LMs[layer] # type: Conv2d
                V = Vs[i]
                if isinstance(lm, Linear):
                    V = V.view(lm.out_features, lm.in_features)
                else:
                    w = lm.kernel_size[0] * lm.kernel_size[1] * lm.in_channels
                    V = V.view((lm.out_channels, w))
                _, S, _ = V.svd(compute_uv=False)
                t = S[:topn].square().sum().sqrt() / S.square().sum().sqrt()
                square_ratio[layer].append(t.item())
        # print(square_ratio[layer])

    fig, axes = plt.subplots(1, len(comp_layers), sharey=True, dpi=600, figsize=(8, 3))
    for i, layer in enumerate(comp_layers):
        axes[i].title.set_text(layer)
        for j in topns:
            y = ratios[j][layer]
            axes[i].scatter(np.arange(1, len(y) + 1), y, s=5, label=r'$k={}$'.format(j))
        if i == 0:
            axes[i].legend(loc=3)
    image_path = vis.get_image_path(name, store_format='pdf')
    plt.tight_layout()
    plt.savefig(image_path, bbox_inches='tight')

def norm_ratio_custom(dim=50, topn=1):
    
    name = '5x1_Top_Eigenvector_rank_{}_{}'.format(exp_name, dim)

    square_ratio = {}
    for layer in comp_layers:
        print(layer)
        square_ratio[layer] = []
        layer_weight_ind = layer + '.weight'
        Vs = torch.from_numpy(eigenvecs_real[layer_weight_ind]) # pylint: disable=no-member
        for i in range(min(dim, int(len(Vs) / 2))):
            lm = LMs[layer] # type: Conv2d
            V = Vs[i]
            if isinstance(lm, Linear):
                V = V.view(lm.in_features, lm.out_features)
            else:
                w = lm.kernel_size[0] * lm.kernel_size[1] * lm.in_channels
                V = V.view((w, lm.out_channels))
            _, S, _ = V.svd(compute_uv=False)
            t = S[:topn].square().sum().sqrt()
            square_ratio[layer].append(t.item())
        # print(square_ratio[layer])

    fig, axes = plt.subplots(1, 5, sharey=True, dpi=600, figsize=(10, 2.5))
    for i, layer in enumerate(comp_layers):
        # axes[int(i/5)][i%5].title.set_text(layer)
        # axes[int(i/5)][i%5].scatter(np.arange(1, len(square_ratio[layer]) + 1), square_ratio[layer], s=8)
        # axes[int(i/5)][i%5].set_ylim([0, 1])
        # axes[int(i/5)][i%5].set_xlim([0, dim])
        axes[i].title.set_text(layer)
        axes[i].scatter(np.arange(1, len(square_ratio[layer]) + 1), square_ratio[layer], s=8)
        axes[i].set_ylim([0, 1])
        axes[i].set_xlim([0, dim])
    
    image_path = vis.get_image_path(name, store_format='pdf')
    plt.tight_layout()
    plt.savefig(image_path, bbox_inches='tight')

# norm_ratio_multitopn()
norm_ratio_custom()