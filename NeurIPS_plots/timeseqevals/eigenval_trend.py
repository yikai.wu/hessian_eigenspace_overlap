import torch
from copy import deepcopy
import numpy as np
from algos.closeformhessian.hessianmodule import HessianModule

def sigval_trend(HM: HessianModule, epochs, snapshots, func, layers, topn, exp_name):

    assert len(epochs) == len(snapshots)
    inds = list(range(len(epochs)))

    dists = {layer: [] for layer in layers}

    for i in inds:
        print("Computing Epoch {}".format(epochs[i]))
        sd = torch.load(snapshots[i], map_location=HM.device)
        HM.load_sd(sd)

        E_mat = HM.expectation(func, layers, out_device=HM.device, print_log=False)
        
        for layer in layers:
            s = torch.svd(E_mat[layer], compute_uv=False).S[:topn].unsqueeze(-1) # pylint: disable=no-member
            dists[layer].append(s)
    
    for layer in layers:

        fig_name = "SigVal_{}_{}_{}".format(func.__name__[:-5], exp_name, layer)
        trend_base = torch.cat(dists[layer], dim=1).cpu().numpy()
        HM.vis.plots([epochs for i in range(len(trend_base))], trend_base, [str(i + 1) for i in range(len(trend_base))], fig_name, x_label='Epoch', y_label='Singular Values', fig_size=(12, 8))
        
        fig_name = "SigValNormalized_{}_{}_{}".format(func.__name__[:-5], exp_name, layer)
        trend_normalized_top1 = deepcopy(trend_base)
        trend_normalized_top1 /= (trend_normalized_top1[:1])
        HM.vis.plots([epochs for i in range(len(trend_normalized_top1))], trend_normalized_top1, [str(i + 1) for i in range(len(trend_normalized_top1))], fig_name, x_label='Epoch', y_label='Singular Values', fig_size=(12, 8))