import torch
import numpy as np
from algos.closeformhessian.hessianmodule import HessianModule

def top_eigenvector(HM: HessianModule,
                    dim: int,
                    topn: int,
                    comp_layers,
                    exp_name,
                    eigenvals_real: dict,
                    eigenvecs_real: dict,
                    crop_ratio=1,
                    interval_inc_ratio=1.1
                    ):
    overlap_xs, overlap_ys = [], []
    name = 'Top_Eigenvector_rank_{}_{}'.format(exp_name, dim)

    sum_ratio = {}
    gap_ratio = {}
    square_ratio = {}
    for layer in comp_layers:
        print(layer)
        sum_ratio[layer] = []
        gap_ratio[layer] = []
        square_ratio[layer] = []
        layer_weight_ind = layer + '.weight'
        H_eigenvals = eigenvals_real[layer_weight_ind]
        H_eigenvecs = torch.from_numpy(eigenvecs_real[layer_weight_ind]) # pylint: disable=no-member
        for i in range(dim):
            V = HM.utils.reshape_to_layer(H_eigenvecs[i], layer)
            _, S, _ = V.svd(compute_uv=False)
            sum_ratio[layer].append(S[:topn].sum()/S.sum())
            gap_ratio[layer].append(S[0]/S[topn])
            square_ratio[layer].append(S[:topn].square().sum().sqrt()/S.square().sum().sqrt())
            print(sum_ratio[layer][-1], gap_ratio[layer][-1], square_ratio[layer][-1])
        sum_ratio[layer] = np.mean(sum_ratio[layer])
        gap_ratio[layer] = np.mean(gap_ratio[layer])
        square_ratio[layer] = np.mean(square_ratio[layer])
        
    print(sum_ratio)
    print(gap_ratio)
    print(square_ratio)

def top_eigenvector_plot(HM: HessianModule,
                        dim: int,
                        topn: int,
                        comp_layers,
                        exp_name,
                        eigenvals_real: dict,
                        eigenvecs_real: dict,
                        crop_ratio=1,
                        interval_inc_ratio=1.1
                        ):
    

    HM.vis.plot_scatter(topeig_xs, topeig_ys, comp_layers, name=name)


def top_eigenvector_plot_sep(HM: HessianModule,
                           dim: int,
                           topn: int,
                           comp_layers,
                           exp_name,
                           eigenvals_real: dict,
                           eigenvecs_real: dict,
                           crop_ratio=1,
                           interval_inc_ratio=1.1
                           ):
    topeig_xs, topeig_ys = [], []
    name = 'Top_Eigenvector_rank_{}_{}'.format(exp_name, dim)

    square_ratio = {}
    for layer in comp_layers:
        print(layer)
        square_ratio[layer] = []
        layer_weight_ind = layer + '.weight'
        H_eigenvals = eigenvals_real[layer_weight_ind]
        H_eigenvecs = torch.from_numpy(eigenvecs_real[layer_weight_ind]) # pylint: disable=no-member
        for i in range(dim):
            V = HM.utils.reshape_to_layer(H_eigenvecs[i], layer)
            _, S, _ = V.svd(compute_uv=False)
            t = S[:topn].square().sum().sqrt()/S.square().sum().sqrt()
            square_ratio[layer].append(t.item())
        print(square_ratio[layer])

    for layer in comp_layers:
        HM.vis.plot_scatter([np.arange(1,dim+1)], [square_ratio[layer]], [layer], name=name+'_'+layer)

    