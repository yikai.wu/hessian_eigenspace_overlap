import os, sys
import torch
import numpy as np
from algos.closeformhessian import hessianmodule
from evaluations import component_compare, overlap_analysis
from tools import file_fetch
import visualization as vis
import functools

from torch.nn import functional, Conv2d, Linear

import numpy as np
import matplotlib as mpl
mpl.use('Agg')
from matplotlib import cm
from matplotlib import gridspec
import matplotlib.pyplot as plt
from matplotlib import rc

from IPython import embed
rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)
params= {'text.latex.preamble' : [r'\usepackage{amsmath}',r'\usepackage{amssymb}', r'\usepackage{bm}']}

def rgetattr(obj, attr, *args):
    def _getattr(obj, attr):
        return getattr(obj, attr, *args)
    return functools.reduce(_getattr, [obj] + attr.split('.'))

plt.style.use('seaborn-deep')
plt.rcParams['xtick.labelsize']= 12
plt.rcParams['ytick.labelsize']= 12
plt.rcParams['axes.labelsize'] = 14
plt.rcParams['axes.titlesize'] = 14
plt.rcParams.update(params)

dirc = '../MNIST_ExpBN/experiments/FC2BN_inpnorm_fixlr0.01'
dirc = '../MNIST_ExpBN/experiments/FC2BN_fixlr0.01'
dirc = '../MNIST_Exp1/experiments/FC2_20_small_fixlr0.01'
dirc = '../MNIST_RandomLabel/experiments/FC2_fixlr0.01_RL'
dirc = '../MNIST_0M/experiments/FC2_200_0M_fixlr0.01'
dirc = '../MNIST_ExpBN/experiments/FC2S_20_BN_fixlr0.01'
dirc = '../MNIST_ExpBN/experiments/FC3BN_nl_fixlr0.01'
dirc = '../CIFAR10_RandomLabel/experiments/LeNet5_fixlr0.01_RL'
dirc = '../CIFAR10_Exp1/experiments/LeNet5_normnew_fixlr0.01'
dirc = '../MNIST_Exp1/experiments/FC2_fixlr0.01'
dirc = '../CIFAR10_Exp1/experiments/LeNet5_fixlr0.01'
epoch = -1
run = 1

storage_path = os.path.join(dirc, 'experiment_log/run_{}'.format(run))

load_evals = False
remain_labels = None
crop_ratio = 1

comp_layers = ['conv1', 'conv2', 'fc1', 'fc2', 'fc3']
comp_layers = ['fc1', 'fc2']
fc_layers = ['fc1', 'fc2', 'fc3']

snapshot_file = file_fetch.fetch_snapshot(dirc, epoch=epoch, run_name=run)
eval_file = file_fetch.fetch_eval_file(dirc, epoch=epoch, run_name=run, arg='_LW_ET')

assert os.path.isdir(dirc)
sys.path.append(dirc)
from config import Config # pylint: disable=no-name-in-module
conf = Config()
net = conf.net()
LMs = {layer: rgetattr(net, layer) for layer in comp_layers}
exp_name = dirc.split('/')[-3] + '_' + dirc.split('/')[-1] + "R{}_E{}".format(run, epoch)
eigenthings_real = torch.load(eval_file, map_location='cpu')
eigenvals_real = eigenthings_real['eigenvals_layer']
eigenvecs_real = eigenthings_real['eigenvecs_layer']
# embed()
def vec_reshape_vis(topn=4):
    
    name = 'Top_Eigenvector_sigs_{}'.format(exp_name)

    for layer in comp_layers:
        print(layer)
        layer_weight_ind = layer + '.weight'
        Vs = torch.from_numpy(eigenvecs_real[layer_weight_ind]) # pylint: disable=no-member
        lm = LMs[layer] # type: Conv2d

        fig, axes = plt.subplots(1, topn, sharey=True, dpi=600, figsize=(4, 3))
        for i in range(topn):
            
            V = Vs[i]
            if isinstance(lm, Linear):
                V = V.view(lm.out_features, lm.in_features)
            else:
                w = lm.kernel_size[0] * lm.kernel_size[1] * lm.in_channels
                V = V.view((w, lm.out_channels))
            _, S, _ = V.svd(compute_uv=False)

            axes[i].set_xlabel(r'$v_{}$'.format(i + 1))
            axes[i].scatter(np.arange(1, 11), S[:10], s=10)
            axes[i].set_ylim([0, 1.1])
            axes[i].set_xlim([0, 10])

        image_path = vis.get_image_path(name + layer, store_format='pdf')
        plt.tight_layout()
        plt.savefig(image_path, bbox_inches='tight')

vec_reshape_vis()