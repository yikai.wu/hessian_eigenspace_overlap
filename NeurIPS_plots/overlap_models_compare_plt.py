import os, sys
import torch
import numpy as np
import matplotlib as mpl
from matplotlib import cm
from matplotlib import gridspec
import matplotlib.pyplot as plt
import visualization as vis

mpl.use('Agg')
from matplotlib import rc
plt.style.use('seaborn-deep')
rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)
params= {'text.latex.preamble' : [r'\usepackage{amsmath}',r'\usepackage{amssymb}', r'\usepackage{bm}']}
plt.rcParams.update(params)

plt.style.use('seaborn-deep')
plt.rcParams['xtick.labelsize']= 12
plt.rcParams['ytick.labelsize']= 12
plt.rcParams['axes.labelsize'] = 14
plt.rcParams['axes.titlesize'] = 14

# FC1
logs = [
        '../CIFAR10_Exp1/experiments/LeNet5_fc1_80/overlap_layerwise_1_249_249.ovlp',
        '../CIFAR10_Exp1/experiments/LeNet5_fc1_100/overlap_layerwise_1_249_249.ovlp',
        '../CIFAR10_Exp1/experiments/LeNet5_normnew_fixlr0.01/overlap_layerwise_1_249_249.ovlp',
        '../CIFAR10_Exp1/experiments/LeNet5_fc1_150/overlap_layerwise_1_249_249.ovlp',
        ]

cap = 200
layer = 'fc1.weight'

# # Conv2
# logs = [
#         '../CIFAR10_Exp1/experiments/LeNet5_normnew_fixlr0.01/overlap_layerwise_1_249_249.ovlp',
#         '../CIFAR10_Exp1/experiments/LeNet5_conv2_25/overlap_layerwise_1_249_249.ovlp'
#         ]

# cap = 50
# layer = 'conv2.weight'

logs = [
        '../CIFAR100/experiments/Resnet18W48New_nobn_fixlr0.01/overlap_layerwise_1_149_149.ovlp',
        '../CIFAR100/experiments/Resnet18W64New_nobn_fixlr0.01/overlap_layerwise_1_149_149.ovlp',
        '../CIFAR100/experiments/Resnet18W80_nobn_fixlr0.01/overlap_layerwise_1_149_149.ovlp',
        ]

cap = 120
layer = 'layer3.0.conv2.weight'

# logs = [
#         '../CIFAR100/experiments/VGG11W48New_nobn_fixlr0.01/overlap_layerwise_1_120_120.ovlp',
#         '../CIFAR100/experiments/VGG11W64New_nobn_fixlr0.01/overlap_layerwise_1_120_120.ovlp',
#         '../CIFAR100/experiments/VGG11W80New_nobn_fixlr0.01/overlap_layerwise_1_120_120.ovlp',
#         ]

# cap = 120
# layer = 'features.13.weight'

exp_name = 'Overlap_ResNet_varying_NeurIPSnarrow_' + layer[:-7]

plt.figure(figsize=(3, 3))
for fl in logs:
    overlap_means, overlap_stds, overlap_dims, layer_width, layers = torch.load(fl, map_location='cpu')
    i = layers.index(layer)
    print(layer_width[layer])
    p = plt.plot(overlap_dims[i][:cap], overlap_means[i][:cap], label=r"$m={}$".format(layer_width[layer]))
    c = p[0].get_color()
    plt.axvline(layer_width[layer], linestyle=":", color=c)
    plt.fill_between(overlap_dims[i][:cap], overlap_means[i][:cap] + overlap_stds[i][:cap], overlap_means[i][:cap] - overlap_stds[i][:cap], alpha=0.2)
    plt.legend(loc=4)

bottom, top = plt.ylim() 
plt.ylim([0, top])
# plt.xlim([0, 180])
plt.xlabel(r'Dimension $k$', fontsize=12)
plt.ylabel(r'Overlap', fontsize=12)

plt.tight_layout()
image_path = vis.get_image_path(exp_name, 'pdf')
plt.savefig(image_path, bbox_inches='tight')
