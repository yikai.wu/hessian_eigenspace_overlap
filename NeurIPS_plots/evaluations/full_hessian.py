import torch
import os
import numpy as np
from algos.closeformhessian.hessianmodule import HessianModule
from collections import OrderedDict

def full_hessian(HM: HessianModule, exp_name, storage_path):
    layers = ['fc1']
    H = HM.expectation(HM.decomp.H_full_comp, layers, out_device=HM.device, print_log=False)
    H = H['fc1']
    eigenvals, eigenvecs = HM.utils.eigenthings_tensor_utils(H, device=HM.device, symmetric=True)
    print(list(eigenvals.to('cpu').numpy()))
    out_dict = {}
    out_dict['Hessian'] = H
    out_dict['eigenvals'] = eigenvals
    out_dict['eigenvecs'] = eigenvecs
    path = os.path.join(storage_path, exp_name+'_full_hessian.eval')
    torch.save(out_dict, path)
    return H, eigenvals, eigenvecs

def hessian_layerwise(HM: HessianModule, layers, exp_name, storage_path):
    Hs = HM.expectation(HM.decomp.H_comp, layers, out_device=HM.device, print_log=False, y_classification_mode='binary_logistic_pn1')
    UTAU = HM.expectation(HM.decomp.UTAU_comp, layers, out_device=HM.device, print_log=False, y_classification_mode='binary_logistic_pn1')
    H_eigenvals, H_eigenvecs = OrderedDict(), OrderedDict()
    UTAU_eigenvals, UTAU_eigenvecs = OrderedDict(), OrderedDict()
    for layer in layers:
        H_eigenvals[layer], H_eigenvecs[layer] = HM.utils.eigenthings_tensor_utils(Hs[layer], device=HM.device, symmetric=True)
        UTAU_eigenvals[layer], UTAU_eigenvecs[layer] = HM.utils.eigenthings_tensor_utils(UTAU[layer], device=HM.device, symmetric=True)
        print(list(H_eigenvals[layer].to('cpu').numpy()))
        print(list(UTAU_eigenvals[layer].to('cpu').numpy()))
    out_dict = {}
    out_dict['layers'] = layers
    out_dict['Hessian'] = Hs
    out_dict['H_eigenvals'] = H_eigenvals
    out_dict['H_eigenvecs'] = H_eigenvecs
    out_dict['UTAU'] = UTAU
    out_dict['UTAU_eigenvals'] = UTAU_eigenvals
    out_dict['UTAU_eigenvecs'] = UTAU_eigenvecs
    path = os.path.join(storage_path, exp_name+'hessian_layerwise.eval')
    torch.save(out_dict, path)
    return Hs, UTAU

def UTAU_xxT_comp(HM: HessianModule, layers, exp_name, storage_path):
    UTAU = HM.expectation(HM.decomp.UTAU_comp, layers, out_device=HM.device, print_log=False, y_classification_mode='binary_logistic_pn1')
    xxT = HM.expectation(HM.decomp.xxT_comp, layers, out_device=HM.device, print_log=False)
    UTAU_eigenvals, UTAU_eigenvecs = OrderedDict(), OrderedDict()
    xxT_eigenvals, xxT_eigenvecs = OrderedDict(), OrderedDict()
    norms = OrderedDict()
    for layer in layers:
        UTAU_eigenvals[layer], UTAU_eigenvecs[layer] = HM.utils.eigenthings_tensor_utils(UTAU[layer], device=HM.device, symmetric=True)
        xxT_eigenvals[layer], xxT_eigenvecs[layer] = HM.utils.eigenthings_tensor_utils(xxT[layer], device=HM.device, symmetric=True)
        ones_mat = torch.ones((UTAU_eigenvecs[layer].shape[1], xxT_eigenvecs[layer].shape[1])).to(HM.device)
        norms[layer] = UTAU_eigenvecs[layer].square().matmul(ones_mat).matmul(xxT_eigenvecs[layer].t().square())
        norms[layer] = norms[layer].reshape(-1).sqrt_()
        print(list(UTAU_eigenvals[layer].to('cpu').numpy()))
        print(list(xxT_eigenvals[layer].to('cpu').numpy()))
        #eigenvals = UTAU_eigenvals[layer].ger(xxT_eigenvals[layer]).reshape(-1)
        #eigenvecs = HM.utils.kp_2d(UTAU_eigenvecs[layer], xxT_eigenvecs[layer])
        #eigenvecs = eigenvecs.div_(norms[layer])
        #print(list(eigenvals.to('cpu').numpy()))
        #print(list(eigenvecs.to('cpu').numpy()))
    out_dict = {}
    out_dict['layers'] = layers
    out_dict['UTAU'] = UTAU
    out_dict['UTAU_eigenvals'] = UTAU_eigenvals
    out_dict['UTAU_eigenvecs'] = UTAU_eigenvecs
    out_dict['xxT'] = xxT
    out_dict['xxT_eigenvals'] = xxT_eigenvals
    out_dict['xxT_eigenvecs'] = xxT_eigenvecs
    out_dict['norms'] = norms
    path = os.path.join(storage_path, exp_name+'_UTAU_xxT.eval')
    torch.save(out_dict, path)
    return UTAU, xxT

def hessian_layerwise_approx(HM: HessianModule, layers, exp_name, storage_path):
    UTAU = HM.expectation(HM.decomp.UTAU_comp, layers, out_device=HM.device, print_log=False, y_classification_mode='binary_logistic_pn1')
    xxT = HM.expectation(HM.decomp.xxT_comp, layers, out_device=HM.device, print_log=False)
    H_eigenvals, H_eigenvecs = OrderedDict(), OrderedDict()
    UTAU_eigenvals, UTAU_eigenvecs = OrderedDict(), OrderedDict()
    for layer in layers:
        H = HM.utils.kp_2d(UTAU[layer], xxT[layer])
        H_eigenvals[layer], H_eigenvecs[layer] = HM.utils.eigenthings_tensor_utils(H, device=HM.device, symmetric=True)
        UTAU_eigenvals[layer], UTAU_eigenvecs[layer] = HM.utils.eigenthings_tensor_utils(UTAU[layer], device=HM.device, symmetric=True)
        print(list(H_eigenvals[layer].to('cpu').numpy()))
        print(list(UTAU_eigenvals[layer].to('cpu').numpy()))
        #print(list(eigenvecs[layer].to('cpu').numpy()))
    out_dict = {}
    out_dict['layers'] = layers
    out_dict['UTAU'] = UTAU
    out_dict['xxT'] = xxT
    out_dict['H_eigenvals'] = H_eigenvals
    out_dict['H_eigenvecs'] = H_eigenvecs
    out_dict['UTAU_eigenvals'] = UTAU_eigenvals
    out_dict['UTAU_eigenvecs'] = UTAU_eigenvecs
    path = os.path.join(storage_path, exp_name+'hessian_layerwise_approx.eval')
    torch.save(out_dict, path)
    return UTAU, xxT

def UTAU_xxT_comp_softmax(HM: HessianModule, layers, exp_name, storage_path):
    UTAU = HM.expectation(HM.decomp.UTAU_comp, layers, out_device=HM.device, print_log=False, y_classification_mode='softmax')
    xxT = HM.expectation(HM.decomp.xxT_comp, layers, out_device=HM.device, print_log=False)
    UTAU_eigenvals, UTAU_eigenvecs = OrderedDict(), OrderedDict()
    xxT_eigenvals, xxT_eigenvecs = OrderedDict(), OrderedDict()
    norms = OrderedDict()
    for layer in layers:
        UTAU_eigenvals[layer], UTAU_eigenvecs[layer] = HM.utils.eigenthings_tensor_utils(UTAU[layer], device=HM.device, symmetric=True)
        xxT_eigenvals[layer], xxT_eigenvecs[layer] = HM.utils.eigenthings_tensor_utils(xxT[layer], device=HM.device, symmetric=True)
        ones_mat = torch.ones((UTAU_eigenvecs[layer].shape[1], xxT_eigenvecs[layer].shape[1])).to(HM.device)
        norms[layer] = UTAU_eigenvecs[layer].square().matmul(ones_mat).matmul(xxT_eigenvecs[layer].t().square())
        norms[layer] = norms[layer].reshape(-1).sqrt_()
        print(list(UTAU_eigenvals[layer].to('cpu').numpy()))
        print(list(xxT_eigenvals[layer].to('cpu').numpy()))
    out_dict = {}
    out_dict['layers'] = layers
    out_dict['UTAU'] = UTAU
    out_dict['UTAU_eigenvals'] = UTAU_eigenvals
    out_dict['UTAU_eigenvecs'] = UTAU_eigenvecs
    out_dict['xxT'] = xxT
    out_dict['xxT_eigenvals'] = xxT_eigenvals
    out_dict['xxT_eigenvecs'] = xxT_eigenvecs
    out_dict['norms'] = norms
    path = os.path.join(storage_path, exp_name+'_UTAU_xxT.eval')
    torch.save(out_dict, path)
    return UTAU, xxT
