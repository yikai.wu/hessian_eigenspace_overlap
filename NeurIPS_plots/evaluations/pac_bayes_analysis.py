import torch
import os
import numpy as np
from algos.closeformhessian.hessianmodule import HessianModule
from collections import OrderedDict

def sigma_post_plot_hessian(HM: HessianModule, sigma_file, exp_name, storage_path):
    data= torch.load(sigma_file)
    eigenvals = data['eigenvals']
    sigma_post = data['sigma_post']
    layer_info = HM.utils.get_layer_info()
    res = {}
    for layer in layer_info:
        s, e, k = layer[0], layer[1], layer[2]
        print(k, flush=True)
        name = 'sigma_post_hessian_{}_{}'.format(exp_name, k)
        eig = eigenvals[s:e]
        sig = sigma_post[s:e]
        eig, ind = torch.sort(eig, descending=True)
        sig = sig[ind]
        dim_x = torch.range(1, sig.shape[0])
        dim_y = sig
        HM.vis.plot_scatter([dim_x], [dim_y], ['Sigma_post'], x_label='dim', y_label='sigma_post', name=name, dpi=150)
        res[k] = sig
    torch.save(res, storage_path)

def sigma_post_plot_standard(HM: HessianModule, sigma_file, exp_name):
    sigma_post= torch.load(sigma_file)
    layer_info = HM.utils.get_layer_info()
    res = {}
    for layer in layer_info:
        s, e, k = layer[0], layer[1], layer[2]
        print(k, flush=True)
        name = 'sigma_post_standard_{}_{}'.format(exp_name, k)
        sig = sigma_post[s:e]
        dim_x = torch.range(1, sig.shape[0])
        dim_y = sig
        HM.vis.plot_scatter([dim_x], [dim_y], ['Sigma_post'], x_label='dim', y_label='sigma_post', name=name, dpi=150)

def sigma_post_plot_hessian_abs(HM: HessianModule, sigma_file, exp_name, storage_path):
    data= torch.load(sigma_file)
    eigenvals = data['eigenvals']
    sigma_post = data['sigma_post']
    layer_info = HM.utils.get_layer_info()
    res = {}
    for layer in layer_info:
        s, e, k = layer[0], layer[1], layer[2]
        print(k, flush=True)
        name = 'sigma_post_hessian_abs_{}_{}'.format(exp_name, k)
        eig = eigenvals[s:e].abs()
        sig = sigma_post[s:e]
        eig, ind = torch.sort(eig, descending=True)
        sig = sig[ind]
        dim_x = torch.range(1, sig.shape[0])
        dim_y = sig
        HM.vis.plot_scatter([dim_x], [dim_y], ['Sigma_post'], x_label='dim', y_label='sigma_post', name=name, dpi=150)
        res[k] = sig
    torch.save(res, storage_path)