import os, sys
import torch
import numpy as np
from algos.closeformhessian import hessianmodule
from evaluations import component_compare, overlap_analysis
from tools import file_fetch
import visualization as vis
from matplotlib import rc

import numpy as np
import matplotlib as mpl
mpl.use('Agg')
from matplotlib import cm
from matplotlib import gridspec
import matplotlib.pyplot as plt
plt.style.use('seaborn-deep')
rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)
params= {'text.latex.preamble' : [r'\usepackage{amsmath}',r'\usepackage{amssymb}', r'\usepackage{bm}']}
plt.rcParams.update(params)

dirc = '../MNIST_RandomLabel/experiments/FC2_fixlr0.01_RL'
dirc = '../MNIST_ExpBN/experiments/FC2BN_inpnorm_fixlr0.01'
dirc = '../MNIST_Exp1/experiments/FC1_20_small_fixlr0.01'
dirc = '../MNIST_0M/experiments/FC2_200_0M_fixlr0.01'
#dirc = '../MNIST_ExpBN/experiments/FC2BN_fixlr0.01'
dirc = '../MNIST_ExpBN/experiments/FC2S_20_BN_fixlr0.01'
dirc = '../MNIST_ExpBN/experiments/FC2BN_nl_fixlr0.01'
dirc = '../MNIST_ExpBN/experiments/FC3BN_nl_fixlr0.01'
dirc = '../CIFAR10_Exp1/experiments/LeNet5_normnew_fixlr0.01'
dirc = '../CIFAR10_Exp1/experiments/LeNet5_BN_nl_fixlr0.01'
dirc = '../MNIST_Exp1/experiments/FC2_fixlr0.01'
epoch = -1
run = 1

def plot_colorbar():
    fig, ax = plt.subplots(figsize=(0.3, 3))

    cbar = mpl.colorbar.ColorbarBase(
        ax,
        cmap=mpl.cm.get_cmap('Reds'),
        norm=mpl.colors.Normalize(vmin=0, vmax=1),
        orientation='vertical')

    plt.savefig(vis.get_image_path('colorbar', store_format='pdf'), bbox_inches='tight')
    plt.close()

# plot_colorbar()

storage_path = os.path.join(dirc, 'experiment_log/run_{}'.format(run))

load_evals = True
remain_labels = None
autograd = True
crop_ratio = 1
dimension = 1000
# comp_layers = ['fc1', 'fc2']#, 'fc3']
# fc_layers = ['fc1', 'fc2']# 'fc3']
comp_layers = ['fc1', 'fc2', 'fc3']
fc_layers = ['fc1', 'fc2', 'fc3']
# comp_layers = ['fc1', 'fc2', 'fc3', 'fc4']
# fc_layers = ['fc1', 'fc2', 'fc3', 'fc4']

snapshot_file = file_fetch.fetch_snapshot(dirc, epoch=epoch, run_name=run)
if load_evals:
    eval_file = file_fetch.fetch_eval_file(dirc, epoch=epoch, run_name=run, arg='_LW_ET', dim=dimension)

assert os.path.isdir(dirc)
sys.path.append(dirc)
from config import Config # pylint: disable=no-name-in-module
conf = Config()

net = conf.net()
net.load_state_dict(torch.load(snapshot_file, map_location='cuda' if torch.cuda.is_available() else 'cpu'))
dataset = conf.dataset(train=True, transform=conf.test_transform)
HM = hessianmodule.HessianModule(net, dataset, fc_layers, RAM_cap=64, remain_labels=remain_labels)
device = 'cuda' if torch.cuda.is_available() else 'cpu'

exp_name = dirc.split('/')[-3] + '_' + dirc.split('/')[-1] + "R{}_E{}".format(run, epoch)
dataset = conf.dataset(train=True, transform=conf.test_transform)
if remain_labels is not None:
    exp_name += '_y' + ''.join([str(i) for i in remain_labels]) # pylint: disable=not-an-iterable

if load_evals:
    eigenthings_real = torch.load(eval_file, map_location='cpu') 
    eigenvals_real = eigenthings_real['eigenvals_layer']
    eigenvecs_real = eigenthings_real['eigenvecs_layer']

load_prev = True

xxT_snapshot = snapshot_file + "ExxT"
if os.path.isfile(xxT_snapshot) and load_prev:
    E_xxTs = torch.load(xxT_snapshot, map_location='cpu')
    print('Loaded FC xxT from previous computed result')
else:
    E_xxTs = HM.expectation(HM.decomp.xxT_comp, comp_layers)
    torch.save(E_xxTs, xxT_snapshot)
# component_compare.matvis_E_computed(HM, comp_layers, [UTAU, xxT], ['UTAU', 'xxT'], exp_name + 'ConvMX')
UTAU_snapshot = snapshot_file + "EUTAU"
if os.path.isfile(UTAU_snapshot) and load_prev:
    E_UTAUs = torch.load(UTAU_snapshot, map_location='cpu')
    print('Loaded FC UTAU from previous computed result')
else:
    # E_UTAUs = HM.expectation(HM.decomp.UTAU_comp, comp_layers)
    E_UTAUs = HM.expectation(HM.decomp.UTAU_comp, comp_layers, auto_grad=True, batchsize=16)
    torch.save(E_UTAUs, UTAU_snapshot)

def correspondance_sig(layer):
    
    topn=150
    pic_name = 'est_real_corr_t{}_{}_{}'.format(topn, exp_name, layer)
    eigenthings_est = HM.hessian_eigenthings_estimate(comp_layers, num_eigenthings=topn, dataset_crop=crop_ratio)
    # E_xxTs = HM.expectation(HM.decomp.xxT_comp, comp_layers, dataset_crop=crop_ratio)
    # E_UTAUs = HM.expectation(HM.decomp.UTAU_comp, comp_layers, dataset_crop=crop_ratio)
    
    assert layer in comp_layers
    name = "Pairs_{}_{}_{}".format(exp_name, layer, topn)
    H_eigenvals_est, H_eigenvecs_est, _ = eigenthings_est[layer]
    layer_weight_ind = layer + '.weight'
    H_eigenvecs = torch.from_numpy(eigenvecs_real[layer_weight_ind]) # pylint: disable=no-member
    eigenvals_UTAU, eigenvecs_UTAU = HM.utils.eigenthings_tensor_utils(E_UTAUs[layer], symmetric=True) # pylint: disable=no-member
    eigenvals_xxT, eigenvecs_xxT = HM.utils.eigenthings_tensor_utils(E_xxTs[layer], symmetric=True) # pylint: disable=no-member

    est_l = []
    est_r = []
    base_l = []
    base_r = []

    for i in range(topn):
        vec_base_mat = HM.utils.reshape_to_layer(H_eigenvecs[i], layer)
        vec_est_mat = HM.utils.reshape_to_layer(H_eigenvecs_est[i], layer)
        U_base, S_base, V_base = torch.svd(vec_base_mat, compute_uv=True) # pylint: disable=no-member
        U_est, S_est, V_est = torch.svd(vec_est_mat, compute_uv=True) # pylint: disable=no-member
        est_l.append(U_est[:, :1])
        base_l.append(U_base[:, :1])
        est_r.append(V_est[:, :1])
        base_r.append(V_base[:, :1])

    est_L_dot = (HM.utils.mm(eigenvecs_UTAU, torch.cat(est_l, dim=1)) ** 2)[:topn] # pylint: disable=no-member
    base_L_dot = (HM.utils.mm(eigenvecs_UTAU, torch.cat(base_l, dim=1)) ** 2)[:topn] # pylint: disable=no-member
    est_R_dot = (HM.utils.mm(eigenvecs_xxT, torch.cat(est_r, dim=1)) ** 2)[:topn] # pylint: disable=no-member
    base_R_dot = (HM.utils.mm(eigenvecs_xxT, torch.cat(base_r, dim=1)) ** 2)[:topn] # pylint: disable=no-member

    plt.figure(figsize=(6, 6))
    plt.xlabel(r"Eigenvector est", fontsize=13)
    plt.ylabel(r"Eigenvector of E[xxT]", fontsize=13)
    plt.imshow(est_L_dot.cpu(), cmap='Reds')

    image_path = vis.get_image_path(pic_name + 'est_L', store_format='pdf')
    plt.tight_layout()
    plt.savefig(image_path)

    plt.figure(figsize=(6, 6))
    plt.xlabel(r"Eigenvector base", fontsize=13)
    plt.ylabel(r"Eigenvector of E[xxT]", fontsize=13)
    plt.imshow(base_L_dot.cpu(), cmap='Reds')

    image_path = vis.get_image_path(pic_name + 'base_L', store_format='pdf')
    plt.tight_layout()
    plt.savefig(image_path)

def plot_single(name, mat, title=None):
    
    plt.figure(figsize=(6, 3), dpi=600)
    plt.imshow(mat.cpu(), cmap='Reds', interpolation='nearest')
    plt.grid(False)
    plt.box(on=None)
    # for direction in ["top", "bottom", "left", "right"]:
    #     axes[0].spines[direction].set_visible(False)
    # plt.title(title)
    for spine in plt.gca().spines.values():
        spine.set_visible(False)
    image_path = vis.get_image_path(name, store_format='pdf')
    plt.tight_layout()
    plt.savefig(image_path, bbox_inches='tight')

def correspondance_expand(layer, topn):

    device = HM.device
    pic_name = 'est_real_corr_expand_t{}_{}_{}'.format(topn, exp_name, layer)
    # E_UTAUs = HM.expectation(HM.decomp.UTAU_comp, comp_layers, dataset_crop=crop_ratio, out_device=HM.device) # , auto_grad=autograd, batchsize=16)
    # eigenthings_est = HM.hessian_eigenthings_estimate(comp_layers, num_eigenthings=topn, dataset_crop=crop_ratio)
    eigenthings_est = HM.hessian_eigenthings_estimate_precomputed(comp_layers, E_UTAUs, E_xxTs, num_eigenthings=topn)
    # E_xxTs = HM.expectation(HM.decomp.xxT_comp, comp_layers, dataset_crop=crop_ratio, out_device=HM.device)
    
    assert layer in comp_layers
    name = "Pairs_{}_{}_{}".format(exp_name, layer, topn)
    H_eigenvals_est, H_eigenvecs_est, _ = eigenthings_est[layer]
    layer_weight_ind = layer + '.weight'
    H_eigenvecs = torch.from_numpy(eigenvecs_real[layer_weight_ind]).to(device) # pylint: disable=no-member

    eigenvals_UTAU, eigenvecs_UTAU = HM.utils.eigenthings_tensor_utils(E_UTAUs[layer], symmetric=True)
    eigenvals_xxT, eigenvecs_xxT = HM.utils.eigenthings_tensor_utils(E_xxTs[layer], symmetric=True)
    b_xxT = eigenvecs_xxT.unsqueeze(0)
    b_UTAU = eigenvecs_UTAU.unsqueeze(0)

    vec_est_mat = torch.cat([HM.utils.reshape_to_layer(H_eigenvecs_est[i], layer).unsqueeze(0) for i in range(topn)]).to(device) # pylint: disable=no-member
    
    est_UTAU_raw_overlap = torch.Tensor.matmul(b_UTAU, vec_est_mat)# pylint: disable=no-member
    est_UTAU_corr_exp = torch.Tensor.sum(est_UTAU_raw_overlap.square_(), dim=-1, keepdim=False).transpose(0, 1)

    est_xxT_raw_overlap = torch.Tensor.matmul(vec_est_mat, b_xxT.transpose(-1, -2))# pylint: disable=no-member
    est_xxT_corr_exp = torch.Tensor.sum(est_xxT_raw_overlap.square_(), dim=1, keepdim=False).transpose(0, 1)[:len(est_UTAU_corr_exp)]
    
    vec_base_mat = torch.cat([HM.utils.reshape_to_layer(H_eigenvecs[i], layer).unsqueeze(0) for i in range(topn)]).to(device) # pylint: disable=no-member

    base_UTAU_raw_overlap = torch.Tensor.matmul(b_UTAU, vec_base_mat)# pylint: disable=no-member
    base_UTAU_corr_exp = torch.Tensor.sum(base_UTAU_raw_overlap.square_(), dim=-1, keepdim=False).transpose(0, 1)

    base_xxT_raw_overlap = torch.Tensor.matmul(vec_base_mat, b_xxT.transpose(-1, -2))# pylint: disable=no-member
    base_xxT_corr_exp = torch.Tensor.sum(base_xxT_raw_overlap.square_(), dim=1, keepdim=False).transpose(0, 1)[:len(base_UTAU_corr_exp)]

    plot_single('UTAU_True' + pic_name, base_UTAU_corr_exp[:topn])
    plot_single('UTAU_Approx' + pic_name, est_UTAU_corr_exp[:topn])
    plot_single('xxT_True' + pic_name, base_xxT_corr_exp[:topn])
    plot_single('xxT_Approx' + pic_name, est_xxT_corr_exp[:topn])

def eigenvals_sep(dim=20):

    # E_UTAUs = HM.expectation(HM.decomp.UTAU_comp, comp_layers, dataset_crop=crop_ratio, out_device=HM.device) # , auto_grad=autograd, batchsize=16)
    # E_xxTs = HM.expectation(HM.decomp.xxT_comp, comp_layers, dataset_crop=crop_ratio, out_device=HM.device)
    pic_name = 'sigvals_t{}_{}'.format(dim, exp_name)
    for layer in comp_layers:
        print(layer)
        plt.figure(figsize=(3, 3))
        gs = gridspec.GridSpec(1, 2)
        E_UTAU = E_UTAUs[layer]
        E_xxT = E_xxTs[layer]
        eigenvals_UTAU, _ = HM.utils.eigenthings_tensor_utils(E_UTAU, symmetric=True)
        eigenvals_xxT, _ = HM.utils.eigenthings_tensor_utils(E_xxT, symmetric=True)
        vals = eigenvals_UTAU[:dim]
        vals_full = eigenvals_xxT[:dim]

        plt.subplot(gs[0, 0])
        plt.scatter(np.arange(1, len(vals) + 1), vals, s=5)
        plt.xlim([-1, dim + 1])
        plt.xticks([0, 10, 20])
        plt.ticklabel_format(axis='y', style='sci', scilimits=[-1, 1], useMathText=False)
        plt.xlabel(r"$\mathbb{E}[\bm{M}]$")

        plt.subplot(gs[0, 1])
        plt.scatter(np.arange(1, len(vals_full) + 1), vals_full, s=5)
        plt.xlim([-1, dim + 1])
        plt.xticks([0, 10, 20])
        plt.ticklabel_format(axis='y', style='sci', scilimits=[-1, 1], useMathText=False)
        plt.xlabel(r"$\mathbb{E}[\bm{x}\bm{x}^T]$")

        image_path = vis.get_image_path(pic_name + "_{}".format(layer), store_format='pdf')
        plt.tight_layout(pad=0.5)
        plt.savefig(image_path, bbox_inches='tight')

def eigenvals_xxt_all(dim=20):

    # E_UTAUs = HM.expectation(HM.decomp.UTAU_comp, comp_layers, dataset_crop=crop_ratio, out_device=HM.device) # , auto_grad=autograd, batchsize=16)
    # E_xxTs = HM.expectation(HM.decomp.xxT_comp, comp_layers, dataset_crop=crop_ratio, out_device=HM.device)
    pic_name = 'sigvals_xxt_t{}_{}'.format(dim, exp_name)
    plt.figure(figsize=(6, 3))
    gs = gridspec.GridSpec(1, len(comp_layers))
    for i, layer in enumerate(comp_layers):
        print(layer)
        E_xxT = E_xxTs[layer]
        eigenvals_xxT, _ = HM.utils.eigenthings_tensor_utils(E_xxT, symmetric=True)
        vals_full = eigenvals_xxT[:dim]

        plt.subplot(gs[0, i])
        plt.scatter(np.arange(1, len(vals_full) + 1), vals_full, s=5)
        plt.xlim([-1, dim + 1])
        plt.xticks([0, 10, 20])
        plt.ticklabel_format(axis='y', style='sci', scilimits=[-1, 1], useMathText=False)
        plt.xlabel(r"$\mathbb{E}[\bm{x}\bm{x}^T]$")
        plt.title(layer)

    image_path = vis.get_image_path(pic_name, store_format='pdf')
    plt.tight_layout(pad=0.5)
    plt.savefig(image_path, bbox_inches='tight')

# correspondance_expand('fc1', 200)
correspondance_expand('fc2', 20)
# correspondance_expand('fc1', 200)
# correspondance_expand('fc1', 50)
eigenvals_sep()
# correspondance_expand('fc3', 200)
eigenvals_xxt_all()
