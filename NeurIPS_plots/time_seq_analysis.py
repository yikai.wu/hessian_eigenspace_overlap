import os, sys
import torch
import numpy as np
from algos.closeformhessian import hessianmodule
from timeseqevals import eigenval_trend
from timeseqevals import C_trend
from tools import file_fetch

dirc = '../CIFAR10_Exp1/experiments/LeNet5_AdamDefault'
dirc = '../CIFAR10_RandomLabel/experiments/LeNet5_fixlr0.01_RL'
dirc = '../MNIST_Exp1/experiments/FC2_fixlr0.01'
#dirc = '../CIFAR10_Exp1/experiments/LeNet5_fixlr0.01'
dirc = '../MNIST_Exp1/experiments/FC2_narrow30_fixlr0.01'
#dirc = '../MNIST_RandomLabel/experiments/FC2_narrow30_fixlr0.01_RL'
epochs = list(range(0, 501, 10))
run = 1

remain_labels = None
label = 5
crop_ratio = 1
comp_layers = ['fc1', 'fc2', 'fc3']
fc_layers = ['fc1', 'fc2', 'fc3']

def tasks(HM, epochs, snapshots, exp_name):

    #UTAU_comp = HM.decomp.UTAU_comp
    #A_comp, U_comp = HM.decomp.A_comp, HM.decomp.U_comp

    #Ant_comp = HM.tsa.normalize_comp(A_comp, dim=-2)
    #UTAntU_comp = HM.tsa.chain_comp([U_comp, Ant_comp, U_comp], [1, 0, 0])
    
    
    # eigenval_trend.sigval_trend(HM, epochs, snapshots, UTAntU_comp, comp_layers, 20, exp_name)
    #eigenval_trend.sigval_trend(HM, epochs, snapshots, U_comp, comp_layers, 10, exp_name)
    C_trend.C_trend(HM, epochs, snapshots, comp_layers, exp_name)
    #Anf_comp = HM.tsa.normalize_comp(A_comp)
    #UTAnfU_comp = HM.tsa.chain_comp([U_comp, Anf_comp, U_comp], [1, 0, 0])
    #eigenval_trend.sigval_trend(HM, epochs, snapshots, UTAnfU_comp, comp_layers, 20, exp_name)

def main():

    snapshots = []
    for epoch in epochs:
        snapshot_file = file_fetch.fetch_snapshot(dirc, epoch=epoch, run_name=run)
        snapshots.append(snapshot_file)
    print(snapshots)

    assert os.path.isdir(dirc)
    sys.path.append(dirc)
    from config import Config # pylint: disable=no-name-in-module
    conf = Config()

    net = conf.net()
    dataset = conf.dataset(train=True, transform=conf.test_transform)
    HM = hessianmodule.HessianModule(net, dataset, fc_layers, RAM_cap=64, remain_labels=remain_labels)

    exp_name = dirc.split('/')[-3] + '_' + dirc.split('/')[-1]
    dataset = conf.dataset(train=True, transform=conf.test_transform)
    if remain_labels is not None:
        exp_name += '_y' + ''.join([str(i) for i in remain_labels]) # pylint: disable=not-an-iterable
    exp_name += '_{}'.format(epochs[-1])
    
    C_trend.C_trend(HM, epochs, snapshots, comp_layers, exp_name)
    C_trend.C_trend_label(HM, 0, epochs, snapshots, comp_layers, exp_name)
    C_trend.C_trend_label(HM, 1, epochs, snapshots, comp_layers, exp_name)
    C_trend.C_trend_label(HM, 5, epochs, snapshots, comp_layers, exp_name)
    C_trend.C_trend_label(HM, 7, epochs, snapshots, comp_layers, exp_name)

if __name__ == '__main__':
    main()