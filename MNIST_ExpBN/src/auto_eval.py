import torch
import glob
import os, sys
sys.path.append(os.getcwd())
import time
import argparse
from config import Config # pylint: disable=no-name-in-module

parser = argparse.ArgumentParser()
parser.add_argument('-run', type=str, help='The indicator of run (default=1)', default='1')

parser.add_argument('-s', type=int, help='the epoch to start from', default=0)
parser.add_argument('-e', type=int, help='the epoch to end with', default=10000)
parser.add_argument('-n', type=int, help='number of eigenvectors', default=10)
parser.add_argument('-t', type=int, help='number of threads', default=10)

args = parser.parse_args()
conf = Config(args.run)
tasks = []

for i in range(args.s, args.e + 1):
    if conf.eval_policy(i):
        model_path = os.path.join(conf.model_path, "epoch{}.pth".format(i))
        if os.path.isfile(model_path):
            potential_evals = glob.glob(model_path + "*.eval")
            if len(potential_evals) == 0:
                tasks.append(i)

tasks.reverse()
sh_base = ["#!/bin/bash\n",
           "#SBATCH -t 90:00:00\n"
           "#SBATCH -o ./sbatchlog/eval-%A-%a.out\n",
           "#SBATCH -c 4\n"
           "#SBATCH --mem=32G\n",
           "#SBATCH --partition=compsci-gpu\n"
           "#SBATCH --gres=gpu:1\n",
           "#SBATCH --exclude=linux56\n"
           "#SBATCH --array={}%{}\n".format(str(tasks)[1:-1].replace(" ", ""), args.t)
           ]
for x in sh_base:
    print(x[:-1])


command = "python3 eval_epoch.py -n={} -mdir='{}'".format(args.n, conf.model_path)
print(command)

sh_tmp = "./.eval_batch.sh"
if os.path.isfile(sh_tmp):
    os.remove(sh_tmp)
writer = open(sh_tmp, 'w')
for x in sh_base:
    writer.write(x)
writer.write(command)
writer.close()

os.system("sbatch {}".format(sh_tmp))