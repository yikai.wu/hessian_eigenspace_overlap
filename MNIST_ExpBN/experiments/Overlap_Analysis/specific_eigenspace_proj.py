import os
import torch
import numpy as np
import matplotlib as mpl
from matplotlib import cm
from matplotlib import gridspec
import matplotlib.pyplot as plt
import pynvml
mpl.use('Agg')
vis_dir = "Eigespace_Overlap/MNIST/"
# plt.style.use('seaborn')

from subspace_overlap import overlap, file_fetch
from networks import LeNet
from subspace_overlap.overlap import Subspaces

epoch = -1
eval_dim = 250
eval_dim = 1000
pca_dim = 1000
pca_rank = 1000
layer = 'fc2.weight'
dircs = ['../FC1_fixlr0.01']
eval_paths = file_fetch.fetch_dirc(dircs, eval_dim, epoch, arg="_LW_ET")

specific_dim = 10

name_alias = '_X_'.join([x.split('/')[-1] for x in dircs])
subspace = Subspaces(eval_paths)
layer_size = subspace.metas[0]['model_sd'][layer].size()[0]

overlaps = {}
base_subspace = subspace.metas[0]['eigenvecs_layer'][layer][:specific_dim]
for model_ind in range(1, len(subspace.metas)):
    eigenvecs_layerwise = subspace.metas[model_ind]['eigenvecs_layer']
    eigenvecs_layer = eigenvecs_layerwise[layer]
    overlaps[model_ind] = []
    for i, vec in enumerate(eigenvecs_layer):
        overlaps[model_ind].append(base_subspace.mv(vec).norm().item() ** 2)
        if i > specific_dim * 2:
            break
    # print(overlaps)

cmap = cm.Set2 # pylint: disable=no-member
plt.figure(figsize=(10, 10))
gs = gridspec.GridSpec(2, 2)
plt.subplot(gs[0, 0:])
for j in overlaps:
    plt.scatter(np.arange(1, len(overlaps[j]) + 1), overlaps[j], s=5, label='model{}'.format(model_ind))
plt.axvline(specific_dim, label='Base subspace dimension')
plt.axvline(layer_size, label='Layer Size', color='red')

desc = "{}_{}_{}".format(specific_dim, name_alias, layer)
img_name = "specificOverlap{}.jpg".format(desc)
img_path = os.path.join(vis_dir, img_name)
plt.title(desc)
plt.legend()


plt.subplot(gs[1, 0])
for model_ind in range(1, len(subspace.metas)):
    eigenvals_layerwise = subspace.metas[model_ind]['eigenvals_layer'][layer]
    eigenvals_cut = eigenvals_layerwise[:specific_dim * 2]
    plt.scatter(np.arange(1, len(eigenvals_cut) + 1), eigenvals_cut, s=5, label='model{}'.format(model_ind))
plt.axvline(specific_dim, label='Base dimension')
plt.axvline(layer_size, label='Layer Size', color='red')
plt.legend()
plt.title("Eigenvalues")

plt.subplot(gs[1, 1])
for model_ind in range(1, len(subspace.metas)):
    eigenvals_layerwise = subspace.metas[model_ind]['eigenvals_layer'][layer]
    eigenvals_cut = eigenvals_layerwise[10: specific_dim * 2]
    plt.scatter(np.arange(11, len(eigenvals_cut) + 11), eigenvals_cut, s=5, label='model{}'.format(j))
plt.axvline(specific_dim, label='Base dimension')
plt.axvline(layer_size, label='Layer Size', color='red')
plt.legend()
plt.title("Eigenvalues (removing the dominating terms)")

print("" + img_path.split("Visualizations")[1])
plt.tight_layout()
plt.savefig(img_path, dpi=150)