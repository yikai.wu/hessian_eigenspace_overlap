import os, sys
import torch
import numpy as np
import matplotlib as mpl
from matplotlib import cm
from matplotlib import gridspec
import matplotlib.pyplot as plt
import pynvml
mpl.use('Agg')
from matplotlib import rc
plt.style.use('seaborn-deep')
rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)
params= {'text.latex.preamble' : [r'\usepackage{amsmath}',r'\usepackage{amssymb}', r'\usepackage{bm}']}
plt.rcParams.update(params)

plt.style.use('seaborn-deep')
plt.rcParams['xtick.labelsize']= 12
plt.rcParams['ytick.labelsize']= 12
plt.rcParams['axes.labelsize'] = 14
plt.rcParams['axes.titlesize'] = 14

from subspace_overlap import overlap, file_fetch
from networks import LeNet
from subspace_overlap.overlap import Subspaces

def get_image_path(name, store_format='jpg'):
    vis_dir = ""
    img_path = os.path.join(vis_dir, "{}.{}".format(name, store_format))
    print("" + img_path.split("Shared")[1] + '\n')
    return img_path

epoch = -1
eval_dim = 1000
pca_dim = 1000
pca_rank = 1000
dims = np.concatenate([np.arange(1, 250, 1)])
# dims = np.concatenate([np.arange(1, 100, 2), np.arange(101, 250, 10)])
dircs = ['../LeNet5_normnew_fixlr0.001', '../LeNet5_normnew_fixlr0.01', '../LeNet5_normnew_fixlr0.01_momentum']

name_alias = '_X_'.join([x.split('/')[-1] for x in dircs])


assert os.path.isdir(dircs[0])
sys.path.append(dircs[0])
from config import Config # pylint: disable=no-name-in-module
conf = Config()

net = conf.net()
print(net.state_dict().keys())
exp_name = 'DimOverlap_CIFAR10_{}'.format(name_alias)

eval_paths = file_fetch.fetch_dirc(dircs, eval_dim, epoch, arg="_LW_ET")
model_count = len(eval_paths)

print('Total Models: {}'.format(model_count))
subspace = Subspaces(eval_paths)
sim_raw = subspace.model_sim_cross()

assert subspace.layer_info is not None
print(subspace.layer_info)
layers = []# ["full"]
for k in subspace.layer_info:
    print(k)
    if not 'bias' in k:
        layers.append(k)
print(layers)
layer_width = {}
for layer in layers:
    if layer != 'full':
        layer_width[layer] = net.state_dict()[layer].shape[0]
    else:
        layer_width[layer] = subspace.metas[0]['eigenvecs'].size()[1]
print(layer_width)

overlap_means = []
overlap_stds = []
overlap_dims = []

for layer in layers:

    overlaps_raw = []
    dims_layer = dims
    if layer != 'full':
        cnt = subspace.layer_info[layer][1] - subspace.layer_info[layer][0]
        pos = np.sum(dims_layer < cnt)
        dims_layer = dims_layer[:pos]
    print(dims_layer)
    dims_layer = dims_layer
    overlap_dims.append(dims_layer)
    for d in dims_layer:
        print("Evaluating Dim {} {}".format(d, layer))
        overlaps_raw.append(subspace.trace_overlap_cross_layer(dim=d, layer=layer))

    overlap_mean = np.zeros(len(dims_layer))
    overlap_std = np.zeros(len(dims_layer))
    for i in range(len(dims_layer)):
        overlap_mean[i] = np.mean(overlaps_raw[i])
        overlap_std[i] = np.std(overlaps_raw[i])
    overlap_means.append(overlap_mean)
    overlap_stds.append(overlap_std)

cmap = cm.Set2 # pylint: disable=no-member
i = 0

for i, layer in enumerate(layers):
    plt.figure(figsize=(3, 3))
    cap = layer_width[layer] * 3
    # plt.errorbar(overlap_dims[i][:cap], overlap_means[i][:cap], yerr=overlap_stds[i][:cap], marker='.', color=cmap(0.01 + (i / 8)))
    plt.plot(overlap_dims[i][:cap], overlap_means[i][:cap])
    plt.fill_between(overlap_dims[i][:cap], overlap_means[i][:cap] + overlap_stds[i][:cap], overlap_means[i][:cap] - overlap_stds[i][:cap], alpha=0.2)
    # plt.ylim([-0.1, 1.1])
    bottom, top = plt.ylim() 
    plt.ylim([0, top])
    plt.xlabel('Dimension', fontsize=12)
    # plt.ylabel('Subspace Overlap', fontsize=12)
    # plt.title("{} $n={}$".format(layer[:-7], layer_width[layer]))

    plt.tight_layout()
    image_path = get_image_path(exp_name + "_{}".format(layer[:-7]), 'pdf')
    plt.savefig(image_path, bbox_inches='tight')
