# Dissecting Hessian Code Base

### Required Packages:

* PyTorch 1.5.0 + cu101
* TorchVision 0.6.0 + cu101
* Matplotlib 3.2.1

## Code Descriptions:

### Customized Packages

* The package for computing the autocorrelation matrix and the output hessian in closed form is in `./algos/closeformhessian`

* The package for computing the autocorrelation matrix and the output hessian using autograd (for convolutional layers) is in `./algos/cfhag`

* The package for computing the layerwise eigenvalues and eigenvectors is in `./algos/hessian_eigenthings`. This package is modified based on https://github.com/noahgolmant/pytorch-hessian-eigenthings.

### Experient Code

* Experiment models and their training code are available in folders with name of datasets (e.g. `./CIFAR10_Exp1`). For each of these folders, the code to calculate eigenspace overlap is available in the directory `Overlap_Analysis` in the `experiments` folder.

* Code for improved PAC-Bayes bound is available in `./eigen_pac_bayes`

* Code for approximating the eigenvectors of the full Hessian is in `./Nondiag_Eigenspace`

* Code for verifying and reproducing logit gradient clustering are in `./TSNE_Clustering` and `./Clustering_Analysis`