import os, sys
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
from matplotlib import gridspec
mpl.use('Agg')

def get_image_path(name, store_format='pdf'):
    vis_dir = ""
    img_path = os.path.join(vis_dir, "{}.{}".format(name, store_format))
    print("" + img_path.split("Shared")[1] + '\n')
    return img_path

def plot_single(name, mat, title=None):
    plt.figure(figsize=(6, 3), dpi=600)
    plt.imshow(mat.cpu(), cmap='Reds', interpolation='nearest')
    plt.grid(False)
    plt.box(on=None)
    # for direction in ["top", "bottom", "left", "right"]:
    #     axes[0].spines[direction].set_visible(False)
    plt.title(title)
    for spine in plt.gca().spines.values():
        spine.set_visible(False)
        print(spine)
    image_path = get_image_path(name, store_format='pdf')
    plt.tight_layout()
    plt.savefig(image_path, bbox_inches='tight')

def subspace_overlap(ovlp, dim):
    return ovlp[:, :dim].sum(dim=1).cpu().numpy()

def plot_overlap_fig(ovlp, topn, subspace_dims):
    x_base = np.arange(topn)
    for i, dim in enumerate(subspace_dims):
        ovlp_sum = subspace_overlap(ovlp[:topn], dim)
        sc = plt.scatter(x_base, ovlp_sum, label='k={}'.format(dim), zorder=-dim, s=3)
        plt.axvline(dim, lw=1, linestyle=':', color=plt.rcParams['axes.prop_cycle'].by_key()['color'][i])
    plt.legend()

def plot_overlaps(name, ovlps, layers, param_size, topn, subspace_dims):
    plt.figure(figsize=(6, len(layers) * 5))
    gs = gridspec.GridSpec(len(layers), 1)
    for i, layer in enumerate(layers):
        print(layer)
        plt.subplot(gs[i, 0])
        if i == 0:
            plt.title(name + '\n' + layer + ' m={}'.format(param_size[layer]))
        else:
            plt.title(layer + ' m={}'.format(param_size[layer]))
        plt.ylabel('overlap')
        plot_overlap_fig(ovlps[layer], topn, subspace_dims)
    plt.tight_layout()
    plt.savefig(get_image_path(name), dpi=150)
