import torch
import sys, os
import numpy as np
from IPython import embed
from utils import *

from algos.closeformhessian import hessianmodule
from IPython import embed

from matplotlib import rc
rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)

import numpy as np
import matplotlib as mpl
mpl.use('Agg')
from matplotlib import cm
from matplotlib import gridspec
import matplotlib.pyplot as plt
plt.style.use('seaborn-deep')

plt.style.use('seaborn-deep')
plt.rcParams['xtick.labelsize']= 12
plt.rcParams['ytick.labelsize']= 12
plt.rcParams['axes.labelsize'] = 10
plt.rcParams['axes.titlesize'] = 14


def approx_fulleigenvec(vxs, vFTAF, sig_FTAF, split_inds):
    ret = []
    for i, layer in enumerate(layer_seq):
        s, e = split_inds[layer]
        vFTAF_crop = vFTAF[s : e]
        v_kron_x = vFTAF_crop.unsqueeze(-1).matmul(vxs[layer].transpose(-1, -2)).view(-1)
        if with_bias:
            v_kron_x = torch.cat([v_kron_x, vFTAF_crop])
        ret.append(v_kron_x)
        # print(len(v_kron_x))
    vec = torch.cat(ret)
    vec_scale = vec.norm()
    vec.div_(vec.norm())
    return vec.cpu(), sig_FTAF * torch.square(vec_scale)

def orthogonalize(m):
    m.div_(m.norm(dim=1, keepdim=True))
    basis = torch.zeros([len(m), len(m[0])]).to(m.device)
    basis[0] = m[0]

    # Gram Schmit
    for i in range(1, len(basis)):
        v = m[i].clone()
        proj_mat = basis[:i]
        proj_norms = proj_mat.matmul(v.unsqueeze(-1))
        proj_v = proj_norms.t().matmul(proj_mat).squeeze(0)
        v -= proj_v
        basis[i] = v / v.norm()
    # embed()
    return basis

# MNIST - FC2
root_loc = '../MNIST_Exp1/experiments/FC2_fixlr0.01'
log_lw_loc = '../MNIST_Exp1/experiments/FC2_fixlr0.01/experiment_log/run_1/models/final.pth_LW_ET1000.eval'
log_full_loc = '../MNIST_Exp1/experiments/FC2_fixlr0.01/experiment_log/run_1/models/final.pth_LW_ET1000.eval'
model_loc = '../MNIST_Exp1/experiments/FC2_fixlr0.01/experiment_log/run_1/models/final.pth'
layer_seq = ['fc1', 'fc2', 'fc3']
with_bias = True

# # MNIST - FC2-600
# root_loc = '../MNIST_Exp1/experiments/FC2_600copy_fixlr0.01'
# log_lw_loc = '../MNIST_Exp1/experiments/FC2_600copy_fixlr0.01/experiment_log/run_1/models/final.pth_LW_ET200.eval'
# log_full_loc = '../MNIST_Exp1/experiments/FC2_600copy_fixlr0.01/experiment_log/run_1/models/final.pth_LW_ET200.eval'
# model_loc = '../MNIST_Exp1/experiments/FC2_600copy_fixlr0.01/experiment_log/run_1/models/final.pth'
# layer_seq = ['fc1', 'fc2', 'fc3']
# with_bias = True

# # MNIST - FC4nb
# root_loc = '../MNIST_Exp1/experiments/FC4nb_fixlr0.01'
# log_lw_loc = '../MNIST_Exp1/experiments/FC4nb_fixlr0.01/experiment_log/run_1/models/final.pth_LW_ET500.eval'
# log_full_loc = '../MNIST_Exp1/experiments/FC4nb_fixlr0.01/experiment_log/run_1/models/final.pth_LW_ET500.eval'
# model_loc = '../MNIST_Exp1/experiments/FC4nb_fixlr0.01/experiment_log/run_1/models/final.pth'
# layer_seq = ['fc1', 'fc2', 'fc3', 'fc4', 'fc5']
# with_bias = False

# # MNIST - FC4-200
# root_loc = '../MNIST_Exp1/experiments/FC4_fixlr0.01'
# log_lw_loc = '../MNIST_Exp1/experiments/FC4_fixlr0.01/experiment_log/run_1/models/final.pth_LW_ET300.eval'
# log_full_loc = '../MNIST_Exp1/experiments/FC4_fixlr0.01/experiment_log/run_1/models/final.pth_LW_ET300.eval'
# model_loc = '../MNIST_Exp1/experiments/FC4_fixlr0.01/experiment_log/run_1/models/final.pth'
# layer_seq = ['fc1', 'fc2', 'fc3', 'fc4', 'fc5']
# with_bias = True

# # MNIST - FC4nb-600
# root_loc = '../MNIST_Exp1/experiments/FC4_600nb_fixlr0.01'
# log_lw_loc = '../MNIST_Exp1/experiments/FC4_600nb_fixlr0.01/experiment_log/run_1/models/final.pth_LW_ET300.eval'
# log_full_loc = '../MNIST_Exp1/experiments/FC4_600nb_fixlr0.01/experiment_log/run_1/models/final.pth_LW_ET300.eval'
# model_loc = '../MNIST_Exp1/experiments/FC4_600nb_fixlr0.01/experiment_log/run_1/models/final.pth'
# layer_seq = ['fc1', 'fc2', 'fc3', 'fc4', 'fc5']
# with_bias = False

# # MNIST - FC4-600
# root_loc = '../MNIST_Exp1/experiments/FC4_600_fixlr0.01'
# log_lw_loc = '../MNIST_Exp1/experiments/FC4_600_fixlr0.01/experiment_log/run_1/models/final.pth_LW_ET300.eval'
# log_full_loc = '../MNIST_Exp1/experiments/FC4_600_fixlr0.01/experiment_log/run_1/models/final.pth_LW_ET300.eval'
# model_loc = '../MNIST_Exp1/experiments/FC4_600_fixlr0.01/experiment_log/run_1/models/final.pth'
# layer_seq = ['fc1', 'fc2', 'fc3', 'fc4', 'fc5']
# with_bias = True

# # MNIST - FC8-600
# root_loc = '../MNIST_Exp1/experiments/FC8_600_fixlr0.01'
# log_lw_loc = '../MNIST_Exp1/experiments/FC8_600_fixlr0.01/experiment_log/run_1/models/final.pth_LW_ET50.eval'
# log_full_loc = '../MNIST_Exp1/experiments/FC8_600_fixlr0.01/experiment_log/run_1/models/final.pth_LW_ET50.eval'
# model_loc = '../MNIST_Exp1/experiments/FC8_600_fixlr0.01/experiment_log/run_1/models/final.pth'
# layer_seq = ['fc1', 'fc2', 'fc3', 'fc4', 'fc5', 'fc6', 'fc7', 'fc8', 'fc9']
# with_bias = True

# # MNIST - FC8-600
# root_loc = '../MNIST_Exp1/experiments/FC8_600_fixlr0.01'
# log_lw_loc = '../MNIST_Exp1/experiments/FC8_600_fixlr0.01/experiment_log/run_2/models/final.pth_LW_ET300.eval'
# log_full_loc = '../MNIST_Exp1/experiments/FC8_600_fixlr0.01/experiment_log/run_2/models/final.pth_LW_ET300.eval'
# model_loc = '../MNIST_Exp1/experiments/FC8_600_fixlr0.01/experiment_log/run_2/models/final.pth'
# layer_seq = ['fc{}'.format(i) for i in range(1, 10)]
# with_bias = True

# root_loc = '../MNIST_Exp1/experiments/FC4nb_fixlr0.01'
# log_lw_loc = '../MNIST_Exp1/experiments/FC4nb_fixlr0.01/experiment_log/run_1/models/final.pth_LW_ET500.eval'
# log_full_loc = '../MNIST_Exp1/experiments/FC4nb_fixlr0.01/experiment_log/run_1/models/final.pth_LW_ET500.eval'
# model_loc = '../MNIST_Exp1/experiments/FC4nb_fixlr0.01/experiment_log/run_1/models/final.pth'
# layer_seq = ['fc1', 'fc2', 'fc3', 'fc4', 'fc5']

topn = 50
runs = range(1, 6)
overlap_x = np.arange(1, 50)
exp_name = '{}_{}'.format(root_loc.split('/')[-3], root_loc.split('/')[-1])

print(exp_name, flush=True)
dev = 'cuda' if torch.cuda.is_available() else 'cpu'
overlap_ys = []

for run in runs:
    try:
        print(run)
        log_full = torch.load(log_full_loc.replace('run_1', 'run_{}'.format(run)), map_location='cpu')
        log_lw = torch.load(log_lw_loc.replace('run_1', 'run_{}'.format(run)), map_location='cpu')

        vecs_full = torch.from_numpy(log_full['eigenvecs']).to('cpu')
        eigenvals_full = torch.from_numpy(log_full['eigenvals']).to('cpu')

        vecs_lw = log_lw['eigenvecs_layer']
        for k in vecs_lw.keys():
            vecs_lw[k] = torch.from_numpy(vecs_lw[k]).to('cpu')
        partitions = log_lw['layerinfo']

        assert os.path.isdir(root_loc.replace('run_1', 'run_{}'.format(run)))
        sys.path.append(root_loc.replace('run_1', 'run_{}'.format(run)))
        from config import Config #pylint: disable=no-name-in-module
        conf = Config()

        shape_info = {}
        net = conf.net()
        for name, var in net.named_parameters():
            n_entries = len(var.view(-1))
            shape_info[name] = [var.shape[0], n_entries // var.shape[0]]

        net.load_state_dict(torch.load(model_loc.replace('run_1', 'run_{}'.format(run)), map_location='cuda' if torch.cuda.is_available() else 'cpu'))
        dataset = conf.dataset(train=True, transform=conf.test_transform)
        HM = hessianmodule.HessianModule(net, dataset, layer_seq, RAM_cap=64)
        Exs = HM.expectation(HM.decomp.x_comp, layer_seq)
        ExxTs = HM.expectation(HM.decomp.xxT_comp, layer_seq)

        ExxT_eigenvecs = {}
        for layer in layer_seq:
            
            Ex = Exs[layer].clone()
            Ex_normalized = Exs[layer].view(-1).div(Exs[layer].norm())

            ExxT = ExxTs[layer]
            xxT_sigma, xxT_vs = HM.utils.eigenthings_tensor_utils(ExxT, symmetric=True)
            xxT_eigenvec = xxT_vs[0]

            print("Layer {}, Dot squared (E[x] vs E[xxT] eigenvec): {}".format(layer, xxT_eigenvec.dot(Ex_normalized)), flush=True)
            # print(Ex.norm(), xxT_sigma[0].sqrt())
            
            ExxT_eigenvecs[layer] = xxT_vs[0].unsqueeze(-1) * xxT_sigma[0].sqrt()
            print(ExxT_eigenvecs[layer].norm(), Ex.norm())

        # exit()
        EFTAF = HM.expectation(HM.decomp.FTAF_comp, layer_seq)[layer_seq[0]]


        split_inds = {}
        s = 0
        for i, w in enumerate(HM.Ws.__reversed__()):
            e = s + w.shape[0]
            split_inds[layer_seq[i]] = [s, e]
            s = e

        print(split_inds, flush=True)
        approx_vecs = []
        approx_eigenvals = []
        out_sigmas, out_vs = HM.utils.eigenthings_tensor_utils(EFTAF, symmetric=True)
        # print(out_sigmas, out_vs.shape)
        for i, v in enumerate(out_vs[:topn]):
            v_full_approx_Ex, sig_full_approx_Ex = approx_fulleigenvec(Exs, v, out_sigmas[i], split_inds)
            v_full_approx_ExxT, sig_full_approx_ExxT = approx_fulleigenvec(ExxT_eigenvecs, v, out_sigmas[i], split_inds)
            eigenvec_full_real = vecs_full[i]
            eigenval_full_real = eigenvals_full[i]

            print(sig_full_approx_Ex, eigenval_full_real)

            approx_vecs.append(v_full_approx_Ex)
            approx_eigenvals.append(sig_full_approx_Ex.item())
            # print(i, torch.norm(v_full_approx_Ex), torch.norm(v_full_approx_ExxT), torch.norm(eigenvec_full_real))
            assert len(eigenvec_full_real) == len(v_full_approx_Ex)

            overlaps = [i, eigenvec_full_real.dot(v_full_approx_Ex).square(), eigenvec_full_real.dot(v_full_approx_ExxT).square()]
            print("# {} Overlap (Ex): {:.5g} Overlap (ExxT): {:.5g}".format(*overlaps))

        print(root_loc)

        # for i, v in enumerate(approx_vecs):
        #     for j, w in enumerate(approx_vecs[:i]):
        #         print(i, j, v.dot(w))
        vecs_approx = orthogonalize((torch.stack(approx_vecs)))
        overlap_y = HM.measure.trace_overlap_trend(vecs_full, vecs_approx, overlap_x)
        overlap_ys.append(overlap_y)
    except:
        continue

y_means, y_stds = [], []
for i in range(len(overlap_ys[0])):
    ys = [overlap_y[i] for overlap_y in overlap_ys]
    y_means.append(np.mean(ys))
    y_stds.append(np.std(ys))

y_means = np.array(y_means)
y_stds = np.array(y_stds)

if True:
    plt.figure(figsize=(2.5, 2.5))
    plt.xlabel(r"Top $k$ Eigenspace", fontsize=12)

    plt.plot(overlap_x, y_means)
    plt.fill_between(overlap_x, (y_means + y_stds), (y_means - y_stds), alpha=0.2)

    plt.ylim([0, 1])
    image_path = HM.vis.get_image_path(exp_name + '_Average_fulleigenvec_overlap', store_format='pdf')
    plt.tight_layout()
    plt.savefig(image_path, bbox_inches='tight')

# Main text
if False:
    overlap_x = np.arange(1, len(approx_vecs) + 1)
    vecs_approx = orthogonalize((torch.stack(approx_vecs)))
    overlap_y = HM.measure.trace_overlap_trend(vecs_full, vecs_approx, overlap_x)

    plt.figure(figsize=(4.5, 3))
    plt.xlabel(r"Top $k$ Eigenspace", fontsize=12)

    plt.plot(overlap_x, overlap_y)
    plt.ylim([0, 1])
    image_path = HM.vis.get_image_path(exp_name + '_fulleigenvec_overlap_appendix_{}'.format(topn), store_format='pdf')
    plt.tight_layout()
    plt.savefig(image_path, bbox_inches='tight')

    plt.figure(figsize=(4.5, 3))
    plt.xlabel(r"The $i$-th eigenvalue", fontsize=12)
    est, real = approx_eigenvals[:topn], eigenvals_full[:topn]
    plt.scatter(np.arange(1, len(est) + 1), est, label='Approximated', s=8, marker='x', zorder=10)
    plt.scatter(np.arange(1, len(real) + 1), real, label='Exact', s=8, zorder=0)
    plt.legend()
    
    image_path = HM.vis.get_image_path(exp_name + '_fulleigenval_approx_appendix_{}'.format(topn), store_format='pdf')
    plt.tight_layout()
    plt.savefig(image_path)