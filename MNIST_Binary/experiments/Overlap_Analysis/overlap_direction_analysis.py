import os, sys
sys.path.append(os.getcwd())

import torch
import glob
import numpy as np
from tqdm import tqdm
import argparse

import matplotlib as mpl
from matplotlib import cm
import matplotlib.pyplot as plt
mpl.use('Agg')
# plt.style.use('seaborn')

vis_dirc = "train_log/cifar10_landscape_exp/LeNetHEP1/Overlap_Analysis_Data/vis/overlap/"

global_device = 'cpu'
if torch.cuda.is_available():
    global_device = 'cuda'

from networks.LeNet import LeNet5
net = LeNet5()

def get_layer_info():
    s = 0
    e = 0
    sd = net.state_dict()
    ret = []
    for k in (sd):
        e += sd[k].view(-1).size()[0]
        print(s, e, k)
        ret.append([s, e, k])
        s = e
    return ret

def eigenvecs2projmat(eigenvecs, num: int):
    assert len(eigenvecs) >= num
    assert num >= 1
    vecs = [eigenvecs[i] for i in range(num)]
    paramspace_dim = len(vecs[0])
    M = torch.cat(vecs).view(torch.Size([num, paramspace_dim])) # pylint: disable=no-member
    Mt = torch.transpose(M, 0, 1) # pylint: disable=no-member
    return M, Mt

def get_files(base, epochs):
    fl_ls = []
    for i in epochs:
        potential_eval = glob.glob(os.path.join(base, "epoch{}.pth*eval".format(i)))
        if len(potential_eval) != 0:
            fl_ls.append(potential_eval[0])
    return fl_ls

def overlap_calc(eigenvecs1, eigenvecs2, n):
    M, Mt = eigenvecs2projmat(eigenvecs1, n)
    k = 0
    for i in range(n):
        vi = eigenvecs2[i]
        ki = torch.norm(Mt.mv(M.mv(vi))) ** 2
        k += ki
    return (k / n).to('cpu').numpy()

def sub2subproj(eigenvecs_base, eigenvecs, n):
    M, _ = eigenvecs2projmat(eigenvecs_base, n)
    _, Nt = eigenvecs2projmat(eigenvecs, n)
    Np = torch.mm(M, Nt) # pylint: disable=no-member

    return Np.to('cpu').numpy()

def sub2subproj_vec(eigenvecs_base, eigenvecs, n):
    M, _ = eigenvecs2projmat(eigenvecs_base, n)
    _, Nt = eigenvecs2projmat(eigenvecs, n)
    Np = torch.mm(M, Nt) # pylint: disable=no-member
    Np_vec = torch.sum(torch.abs(Np), dim=1) # pylint: disable=no-member

    return Np_vec.to('cpu').numpy()

def overlap_calc_regional(eigenvecs1, eigenvecs2, n, start, end=62006):
    M, Mt = eigenvecs2projmat(eigenvecs1, n)
    M = M[:, start:end]
    k = 0
    for i in range(n):
        vi = eigenvecs2[i][start:end]
        ki = torch.norm(Mt.mv(M.mv(vi))) ** 2
        k += ki
    return (k / n).to('cpu').numpy()

def sim(model_vect1, model_vect2):
    res = torch.dot(model_vect1, model_vect2) / (torch.norm(model_vect1) * torch.norm(model_vect2)) # pylint: disable=no-member
    return res.to('cpu').numpy()

def load_eval(eval_path, device=global_device):
    metas = torch.load(eval_path, map_location=device)
    metas['eigenvecs'] = [torch.from_numpy(v).to(device) for v in metas["eigenvecs"]] # pylint: disable=no-member
    return metas

def eigenvec_PCA(eigenvecs_ls, n):

    eigenvecs = []
    for e in eigenvecs_ls:
        eigenvecs.append(eigenvecs2projmat(e, n)[0])
    print(len(eigenvecs))
    M = torch.cat(eigenvecs, dim=0) # pylint: disable=no-member
    print(M.size())
    U, S, V = torch.pca_lowrank(M, q=n*len(eigenvecs_ls))
    return U.to('cpu').numpy(), S.to('cpu').numpy(), V.to('cpu').numpy()

def overlap_dirc_mat():

    task_name = 'sigma^2_distribution_8models_epoch{}_dim{}'
    task_name = 'sigma^2_distribution_fixlr_4models_epoch{}_dim{}'
    
    dim = 300
    epoch = 1000
    task_name = task_name.format(epoch, dim)

    dirs_base = ["../LeNet_base_repeating/experiment_log/run_lr0.01_1/models/",
                 "../LeNet_base_repeating/experiment_log/run_lr0.01_2/models/",
                 "../LeNet_base_repeating/experiment_log/run_lr0.01_3/models/",
                 "../LeNet_base_repeating/experiment_log/run_lr0.01_4/models/"]

    dirs_exp = ["../LeNet_explr0.1_100_repeating/experiment_log/run_exp_rep1/models/",
                "../LeNet_explr0.1_100_repeating/experiment_log/run_exp_rep2/models/",
                "../LeNet_explr0.1_100_repeating/experiment_log/run_exp_rep3/models/",
                "../LeNet_explr0.1_100_repeating/experiment_log/run_exp_rep4/models/"]
    
    dirs = dirs_exp + dirs_base
    dirs = dirs_exp
    dirs = dirs_base
    
    eval_name = "epoch{}.pth_ET300.eval"
        
    metas = []
    for d in dirs:
        eval_path = os.path.join(d, eval_name.format(epoch))
        print(eval_path)
        assert os.path.isfile(eval_path)
        metas.append(load_eval(eval_path))
    
    basis_meta = metas[0]
    metas_comp = metas[1:]
    overlap_proj = []

    for i, meta in enumerate(metas_comp):
        # Np_vec = sub2subproj_vec(basis_meta['eigenvecs'], meta['eigenvecs'], n)
        Np = sub2subproj(basis_meta['eigenvecs'], meta['eigenvecs'], dim)
        overlap_proj.append(Np)

    # cmap = cm.Set2 # pylint: disable=no-member
    U, S, V = eigenvec_PCA([meta['eigenvecs'] for meta in metas], dim)
    # print(U, S, V)

    plt.figure(figsize=(12, 6))
    plt.title(task_name)
    plt.scatter(np.arange(len(S)), S**2)

    img_name = os.path.join(vis_dirc, "{}.jpg".format(task_name))
    print(img_name)
    print("" + img_name.split("pe_exp")[1])
    plt.savefig(img_name, dpi=200)

if __name__ == "__main__":
    overlap_dirc_mat()