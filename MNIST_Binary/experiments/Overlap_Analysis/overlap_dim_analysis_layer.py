import os
import torch
import numpy as np
import matplotlib as mpl
from matplotlib import cm
from matplotlib import gridspec
import matplotlib.pyplot as plt
import pynvml
mpl.use('Agg')
vis_dir = "Eigespace_Overlap/MNIST/"
plt.style.use('seaborn')

from subspace_overlap import overlap, file_fetch
from networks import LeNet
from subspace_overlap.overlap import Subspaces

epoch = -1
eval_dim = 1000
pca_dim = 500
pca_rank = 500
dims = np.concatenate([np.arange(1, 100, 20)])
dims = np.concatenate([np.arange(1, 100, 2), np.arange(101, 500, 10)])
dircs = ['../FC1_fixlr0.01']

name_alias = '_X_'.join([x.split('/')[-1] for x in dircs])
desc = 'CIFAR10_{}_epoch1000'.format(name_alias)

eval_paths = file_fetch.fetch_dirc(dircs, eval_dim, epoch, arg="_LW_ET")
model_count = len(eval_paths)

print('Total Models: {}'.format(model_count))
subspace = Subspaces(eval_paths)
sim_raw = subspace.model_sim_cross()

assert subspace.layer_info is not None
print(subspace.layer_info)
layers = ["full"]
for k in subspace.layer_info:
    print(k)
    if not 'bias' in k:
        layers.append(k)
print(layers)
layer_width = {}
for layer in layers:
    if layer != 'full':
        layer_width[layer] = subspace.layer_info[layer][1] - subspace.layer_info[layer][0]
    else:
        layer_width[layer] = subspace.metas[0]['eigenvecs'].size()[1]
print(layer_width)

sigsquares = []
for layer in layers:
    pca_done = False
    singular_values = None
    if layer != 'full':
        eigen_count = subspace.layer_info[layer][1] - subspace.layer_info[layer][0]
        print(layer, eigen_count)
    else:
        eigen_count = 1000000000
        print(layer)
    pca_rank_tmp = min(pca_rank, eigen_count)
    pca_dim_tmp = min(pca_dim, eigen_count)
    while not pca_done:
        try:
            singular_values = subspace.pca_eigenspace_layer(dim=pca_dim_tmp, layer=layer, rank_est=pca_rank_tmp)
            # singular_values = subspace.svd_eigenspace(dim=pca_dim)
            pca_done = True
        except:
            torch.cuda.empty_cache()
            pca_rank = int(pca_rank_tmp / 1.3)
            pca_dim = int(pca_dim_tmp / 1.3)
            print("Rank too high, attempt modified rank {}, dim {}".format(pca_rank_tmp, pca_dim_tmp))
            if pca_rank_tmp < 5 or pca_dim_tmp < 5:
                pca_done = True
    if singular_values is not None:
        sigsquare_dist_normalized = np.power(singular_values, 2) / len(eval_paths)
        sigsquares.append(sigsquare_dist_normalized)
    else:
        sigsquares.append(None)

overlap_means = []
overlap_stds = []
overlap_dims = []

for layer in layers:

    overlaps_raw = []
    dims_layer = dims
    if layer != 'full':
        cnt = subspace.layer_info[layer][1] - subspace.layer_info[layer][0]
        pos = np.sum(dims_layer < cnt)
        dims_layer = dims_layer[:pos]
    print(dims_layer)
    dims_layer = dims_layer
    overlap_dims.append(dims_layer)
    for d in dims_layer:
        print("Evaluating Dim {} {}".format(d, layer))
        overlaps_raw.append(subspace.trace_overlap_cross_layer(dim=d, layer=layer))

    overlap_mean = np.zeros(len(dims_layer))
    overlap_std = np.zeros(len(dims_layer))
    for i in range(len(dims_layer)):
        overlap_mean[i] = np.mean(overlaps_raw[i])
        overlap_std[i] = np.std(overlaps_raw[i])
    overlap_means.append(overlap_mean)
    overlap_stds.append(overlap_std)
img_name = "Dim2OverlapLayer_{}.jpg".format(desc)
img_path = os.path.join(vis_dir, img_name)

cmap = cm.Set2 # pylint: disable=no-member
i = 0
gs = gridspec.GridSpec(2, 3, width_ratios=[3, 3, 1.5])
plt.figure(figsize=(20, 10))

plt.subplot(gs[0, 0])
plt.title(desc, fontsize=14)
for i, layer in enumerate(layers):
    plt.errorbar(overlap_dims[i], overlap_means[i], yerr=overlap_stds[i], marker='.', color=cmap(0.01 + (i / 8)), label="{}: {}".format(layer, layer_width[layer]))
    plt.fill_between(overlap_dims[i], overlap_means[i] + overlap_stds[i], overlap_means[i] - overlap_stds[i], color=cmap(0.01 + (i / 8)), alpha=0.2)
plt.legend()
plt.xlabel('Dimension')
plt.ylabel('Trace Overlap')

plt.subplot(gs[0, 1])
plt.title("Model Similarity {:.4g}; #params: {}".format(np.mean(sim_raw), subspace.param_size), fontsize=14)
for i, layer in enumerate(layers):
    plt.errorbar(overlap_dims[i], overlap_means[i], yerr=overlap_stds[i], marker='.', color=cmap(0.01 + (i / 8)), label="{}: {}".format(layer, layer_width[layer]))
    plt.fill_between(overlap_dims[i], overlap_means[i] + overlap_stds[i], overlap_means[i] - overlap_stds[i], color=cmap(0.01 + (i / 8)), alpha=0.2)
plt.xlim([-5, 125])
plt.ylim([-0.1,1.1])
plt.legend()
plt.xlabel('Dimension')
plt.ylabel('Trace Overlap')
plt.hist(sim_raw)

plt.subplot(gs[1, 0])
plt.title("σ^2 Distribution (Full Rank SVD)", fontsize=14)
for i, layer in enumerate(layers):
    if sigsquares[i] is not None:
        plt.scatter(np.arange(len(sigsquares[i])), sigsquares[i], s=7, alpha=0.8, color=cmap(0.01 + (i / 8)), label="{}: {}".format(layer, layer_width[layer]))
plt.legend()
plt.ylim([0,1.1])
plt.ylabel('σ^2 (Normalized)')
plt.xlabel('# of singular value')

plt.subplot(gs[1, 1])
plt.title("σ^2 Distribution (Zoom in)", fontsize=14)
for i, layer in enumerate(layers):
    if sigsquares[i] is not None:
        plt.scatter(np.arange(len(sigsquares[i])), sigsquares[i], s=8, alpha=0.8, color=cmap(0.01 + (i / 8)), label="{}: {}".format(layer, layer_width[layer]))
plt.xlim([0, pca_rank / 5])
plt.legend()
plt.ylim([0,1.1])
plt.ylabel('σ^2 (Normalized)')
plt.xlabel('# of singular value')

plt.subplot(gs[0, 2])
plt.title("Loss (Gap: {:.4g})".format(subspace.metrics['gen_loss'][0]), fontsize=14)
plt.bar(0, subspace.metrics['train_loss'][0], yerr=subspace.metrics['train_loss'][1], label='train')
plt.bar(1, subspace.metrics['test_loss'][0], yerr=subspace.metrics['test_loss'][1], label='test')
plt.legend()

plt.subplot(gs[1, 2])
plt.title("Accuracy (Gap: {:.4g})".format(subspace.metrics['gen_acc'][0]), fontsize=14)
plt.bar(0, subspace.metrics['train_acc'][0], yerr=subspace.metrics['train_acc'][1], label='train')
plt.bar(1, subspace.metrics['test_acc'][0], yerr=subspace.metrics['test_acc'][1], label='test')
plt.ylim([0, 1.2])
plt.legend()

plt.tight_layout()
print("" + img_path.split("Visualizations")[1])
plt.savefig(img_path, dpi=250)
