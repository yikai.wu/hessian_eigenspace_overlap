import os, sys
sys.path.append(os.getcwd())

import torch
import glob
import numpy as np
from tqdm import tqdm
import argparse

import matplotlib as mpl
from matplotlib import cm
import matplotlib.pyplot as plt
mpl.use('Agg')
plt.style.use('seaborn')

plt.rcParams.update({'lines.markeredgewidth': 1})

vis_dirc = "Eigespace_Overlap/"

global_device = 'cpu'
if torch.cuda.is_available():
    global_device = 'cuda'

from networks.LeNet import LeNet5
net = LeNet5()

def get_layer_info():
    s = 0
    e = 0
    sd = net.state_dict()
    ret = []
    for k in (sd):
        e += sd[k].view(-1).size()[0]
        print(s, e, k)
        ret.append([s, e, k])
        s = e
    return ret
get_layer_info()

def eigenvecs2projmat(eigenvecs, num: int):
    assert len(eigenvecs) >= num
    assert num >= 1
    vecs = [eigenvecs[i] for i in range(num)]
    paramspace_dim = len(vecs[0])
    M = torch.cat(vecs).view(torch.Size([num, paramspace_dim])) # pylint: disable=no-member
    Mt = torch.transpose(M, 0, 1) # pylint: disable=no-member
    return M, Mt

def get_files(base, epochs):
    fl_ls = []
    for i in epochs:
        potential_eval = glob.glob(os.path.join(base, "epoch{}.pth*eval".format(i)))
        if len(potential_eval) != 0:
            fl_ls.append(potential_eval[0])
    return fl_ls

def overlap_calc(eigenvecs1, eigenvecs2, n):
    M, Mt = eigenvecs2projmat(eigenvecs1, n)
    k = 0
    for i in range(n):
        vi = eigenvecs2[i]
        ki = torch.norm(Mt.mv(M.mv(vi))) ** 2
        k += ki
    return (k / n).to('cpu').numpy()

def overlap_calc_regional(eigenvecs1, eigenvecs2, n, start, end=62006):
    M, Mt = eigenvecs2projmat(eigenvecs1, n)
    M = M[:, start:end]
    k = 0
    for i in range(n):
        vi = eigenvecs2[i][start:end]
        ki = torch.norm(Mt.mv(M.mv(vi))) ** 2
        k += ki
    return (k / n).to('cpu').numpy()

def sim(model_vect1, model_vect2):
    res = torch.dot(model_vect1, model_vect2) / (torch.norm(model_vect1) * torch.norm(model_vect2)) # pylint: disable=no-member
    return res.to('cpu').numpy()

def load_eval(eval_path, device=global_device):
    metas = torch.load(eval_path, map_location=device)
    # if device == 'cpu':
    #     metas['model_vect'] = torch.from_numpy(metas['model_vect']).to(device) # pylint: disable=no-member
    metas['eigenvecs'] = [torch.from_numpy(v).to(device) for v in metas["eigenvecs"]] # pylint: disable=no-member
    return metas

def overlap_trend():

    task_name = 'exp_full_proj'
    task_desc = 'Fix LR 0.01 LeNet (4 Runs Cross Comparing)'
    task_desc = 'Exp Decay LR LeNet (4 Runs Cross Comparing)'
    dims = [10, 100]
    dims = [10, 20, 50, 100, 200, 300]
    epochs = np.arange(0, 101, 50)
    epochs = np.arange(0, 601, 50)
    dirs_base = ["../LeNet_base_repeating/experiment_log/run_lr0.01_1/models/",
                 "../LeNet_base_repeating/experiment_log/run_lr0.01_2/models/",
                 "../LeNet_base_repeating/experiment_log/run_lr0.01_3/models/",
                 "../LeNet_base_repeating/experiment_log/run_lr0.01_4/models/"]

    dirs_exp = ["../LeNet_explr0.1_100_repeating/experiment_log/run_exp_rep1/models/",
                "../LeNet_explr0.1_100_repeating/experiment_log/run_exp_rep2/models/",
                "../LeNet_explr0.1_100_repeating/experiment_log/run_exp_rep3/models/",
                "../LeNet_explr0.1_100_repeating/experiment_log/run_exp_rep4/models/"]
    
    dirs = dirs_base
    dirs = dirs_exp
    
    eval_name = "epoch{}.pth_ET300.eval"
    
    keys = ['dot', 'overlap']
    raw_res = {k: [] for k in keys}

    for epoch in epochs:
        print(epoch)
        
        metas = []
        for d in dirs:
            eval_path = os.path.join(d, eval_name.format(epoch))
            print(eval_path)
            assert os.path.isfile(eval_path)
            metas.append(load_eval(eval_path))
        
        cross = []
        for i in range(len(dirs)):
            for j in range(i):
                cross.append([i, j])

        dots = []
        overlaps = {n: [] for n in dims}

        for (i, j) in cross:
            meta1, meta2 = metas[i], metas[j]
            dots.append(sim(meta1['model_vect'], meta2['model_vect']))

            for n in dims:
                overlaps[n].append(overlap_calc(meta1['eigenvecs'], meta2['eigenvecs'], n))
        
        raw_res['dot'].append(dots)
        raw_res['overlap'].append(overlaps)
        del metas

    dot_y = []
    dot_std = []
    for dots in raw_res['dot']:
        dot_y.append(np.mean(dots))
        dot_std.append(np.std(dots))

    overlap_y = {n: [] for n in dims}
    overlap_std = {n: [] for n in dims}
    for overlaps in raw_res['overlap']:
        for n in dims:
            overlap_y[n].append(np.mean(overlaps[n]))
            overlap_std[n].append(np.std(overlaps[n]))
    
    dot_y = np.array(dot_y)
    dot_std = np.array(dot_std)
    for n in dims:
        overlap_y[n] = np.array(overlap_y[n])
        overlap_std[n] = np.array(overlap_std[n])

    cmap = cm.Set2 # pylint: disable=no-member

    plt.figure(figsize=(10, 10))
    plt.subplot(211)
    plt.title(task_desc)
    plt.ylabel("Param Dot Product (Normalized)")
    plt.errorbar(epochs, dot_y, yerr=dot_std, fmt='o')

    plt.subplot(212)
    plt.ylabel("Top n Eigenspace Overlap")
    plt.xlabel("Epochs")
    for i, n in enumerate(dims):
        plt.errorbar(epochs, overlap_y[n], yerr=overlap_std[n], marker='.', color=cmap(0.01 + (i / 8)), label="n={}".format(n))
        plt.fill_between(epochs, overlap_y[n] + overlap_std[n], overlap_y[n] - overlap_std[n], color=cmap(0.01 + (i / 8)), alpha=0.2)
    plt.legend(loc=2)
    img_name = os.path.join(vis_dirc, "{}.jpg".format(task_name))
    print(img_name)
    print("" + img_name.split("pe_exp")[1])
    plt.savefig(img_name, dpi=200)

def overlap_trend_regional():

    task_name = 'exp_fc1'
    task_name = 'base_fc1'
    task_desc = 'Exp Decay LR LeNet FC1 (4 Runs Cross Comparing)'
    task_desc = 'Fix LR 0.01 LeNet FC3 (4 Runs Cross Comparing)'
    start_pt, end_pt = 2872, 62006
    start_pt, end_pt = 0, 2872
    start_pt, end_pt = 50992, 61072
    start_pt, end_pt = 2872, 50872
    start_pt, end_pt = 61156, 62006
    
    dims = [10, 100]
    dims = [10, 20, 50, 100, 200, 300]
    epochs = np.arange(0, 101, 50)
    epochs = np.arange(50, 601, 50)
    dirs_base = ["../LeNet_base_repeating/experiment_log/run_lr0.01_1/models/",
                 "../LeNet_base_repeating/experiment_log/run_lr0.01_2/models/",
                 "../LeNet_base_repeating/experiment_log/run_lr0.01_3/models/",
                 "../LeNet_base_repeating/experiment_log/run_lr0.01_4/models/"]

    dirs_exp = ["../LeNet_explr0.1_100_repeating/experiment_log/run_exp_rep1/models/",
                "../LeNet_explr0.1_100_repeating/experiment_log/run_exp_rep2/models/",
                "../LeNet_explr0.1_100_repeating/experiment_log/run_exp_rep3/models/",
                "../LeNet_explr0.1_100_repeating/experiment_log/run_exp_rep4/models/"]
    
    dirs = dirs_exp
    dirs = dirs_base
    
    eval_name = "epoch{}.pth_ET300.eval"
    
    keys = ['dot', 'overlap']
    raw_res = {k: [] for k in keys}

    for epoch in epochs:
        print(epoch)
        
        metas = []
        for d in dirs:
            eval_path = os.path.join(d, eval_name.format(epoch))
            print(eval_path)
            assert os.path.isfile(eval_path)
            metas.append(load_eval(eval_path))
        
        cross = []
        for i in range(len(dirs)):
            for j in range(i):
                cross.append([i, j])

        dots = []
        overlaps = {n: [] for n in dims}

        for (i, j) in cross:
            meta1, meta2 = metas[i], metas[j]
            dots.append(sim(meta1['model_vect'], meta2['model_vect']))

            for n in dims:
                overlaps[n].append(overlap_calc_regional(meta1['eigenvecs'], meta2['eigenvecs'], n, start_pt, end_pt))
        
        raw_res['dot'].append(dots)
        raw_res['overlap'].append(overlaps)
        del metas

    dot_y = []
    dot_std = []
    for dots in raw_res['dot']:
        dot_y.append(np.mean(dots))
        dot_std.append(np.std(dots))

    overlap_y = {n: [] for n in dims}
    overlap_std = {n: [] for n in dims}
    for overlaps in raw_res['overlap']:
        for n in dims:
            overlap_y[n].append(np.mean(overlaps[n]))
            overlap_std[n].append(np.std(overlaps[n]))
    
    dot_y = np.array(dot_y)
    dot_std = np.array(dot_std)
    for n in dims:
        overlap_y[n] = np.array(overlap_y[n])
        overlap_std[n] = np.array(overlap_std[n])

    cmap = cm.Set2 # pylint: disable=no-member

    plt.figure(figsize=(10, 10))
    plt.subplot(211)
    plt.title(task_desc, fontsize=18)
    plt.ylabel("Param Dot Product (Normalized)")
    plt.errorbar(epochs, dot_y, yerr=dot_std, fmt='o')

    plt.subplot(212)
    plt.ylabel("Top n Eigenspace Overlap")
    plt.xlabel("Epochs")
    for i, n in enumerate(dims):
        plt.errorbar(epochs, overlap_y[n], yerr=overlap_std[n], marker='.', color=cmap(0.01 + (i / 8)), label="n={}".format(n))
        plt.fill_between(epochs, overlap_y[n] + overlap_std[n], overlap_y[n] - overlap_std[n], color=cmap(0.01 + (i / 8)), alpha=0.2)
    plt.legend(loc=2)
    img_name = os.path.join(vis_dirc, "{}.jpg".format(task_name))
    print(img_name)
    print("" + img_name.split("pe_exp")[1])
    plt.savefig(img_name, dpi=200)

def overlap_trend_cross():

    task_name = 'base_exp_cross'
    task_desc = 'Fix LR 0.01 LeNet (4 Runs Cross Comparing)'
    task_desc = 'Exp decay x Fix lr (4x4 cross comparing)'
    dims = [10, 100]
    dims = [10, 20, 50, 100, 200, 300]
    epochs = np.arange(0, 101, 50)
    epochs = np.arange(0, 601, 50)
    dirs_base = ["../LeNet_base_repeating/experiment_log/run_lr0.01_1/models/",
                 "../LeNet_base_repeating/experiment_log/run_lr0.01_2/models/",
                 "../LeNet_base_repeating/experiment_log/run_lr0.01_3/models/",
                 "../LeNet_base_repeating/experiment_log/run_lr0.01_4/models/"]

    dirs_exp = ["../LeNet_explr0.1_100_repeating/experiment_log/run_exp_rep1/models/",
                "../LeNet_explr0.1_100_repeating/experiment_log/run_exp_rep2/models/",
                "../LeNet_explr0.1_100_repeating/experiment_log/run_exp_rep3/models/",
                "../LeNet_explr0.1_100_repeating/experiment_log/run_exp_rep4/models/"]
    
    dirs = dirs_base + dirs_exp
    
    eval_name = "epoch{}.pth_ET300.eval"
    
    keys = ['dot', 'overlap']
    raw_res = {k: [] for k in keys}

    for epoch in epochs:
        print(epoch)
        
        metas = []
        for d in dirs:
            eval_path = os.path.join(d, eval_name.format(epoch))
            print(eval_path)
            assert os.path.isfile(eval_path)
            metas.append(load_eval(eval_path))
        
        cross = []
        for i in range(len(dirs_base)):
            for j in range(len(dirs_exp), len(dirs)):
                cross.append([i, j])
        dots = []
        overlaps = {n: [] for n in dims}

        for (i, j) in cross:
            meta1, meta2 = metas[i], metas[j]
            dots.append(sim(meta1['model_vect'], meta2['model_vect']))

            for n in dims:
                overlaps[n].append(overlap_calc(meta1['eigenvecs'], meta2['eigenvecs'], n))
        
        raw_res['dot'].append(dots)
        raw_res['overlap'].append(overlaps)
        del metas

    dot_y = []
    dot_std = []
    for dots in raw_res['dot']:
        dot_y.append(np.mean(dots))
        dot_std.append(np.std(dots))

    overlap_y = {n: [] for n in dims}
    overlap_std = {n: [] for n in dims}
    for overlaps in raw_res['overlap']:
        for n in dims:
            overlap_y[n].append(np.mean(overlaps[n]))
            overlap_std[n].append(np.std(overlaps[n]))
    
    dot_y = np.array(dot_y)
    dot_std = np.array(dot_std)
    for n in dims:
        overlap_y[n] = np.array(overlap_y[n])
        overlap_std[n] = np.array(overlap_std[n])

    cmap = cm.Set2 # pylint: disable=no-member

    plt.figure(figsize=(10, 10))
    plt.subplot(211)
    plt.title(task_desc)
    plt.ylabel("Param Dot Product (Normalized)")
    plt.errorbar(epochs, dot_y, yerr=dot_std, fmt='o')

    plt.subplot(212)
    plt.ylabel("Top n Eigenspace Overlap")
    plt.xlabel("Epochs")
    for i, n in enumerate(dims):
        plt.errorbar(epochs, overlap_y[n], yerr=overlap_std[n], marker='.', color=cmap(0.01 + (i / 8)), label="n={}".format(n))
        plt.fill_between(epochs, overlap_y[n] + overlap_std[n], overlap_y[n] - overlap_std[n], color=cmap(0.01 + (i / 8)), alpha=0.2)
    plt.legend(loc=2)
    img_name = os.path.join(vis_dirc, "{}.jpg".format(task_name))
    print(img_name)
    print("" + img_name.split("pe_exp")[1])
    plt.savefig(img_name, dpi=200)

if __name__ == "__main__":
    # overlap_trend()
    # overlap_trend_cross()
    overlap_trend_regional()

