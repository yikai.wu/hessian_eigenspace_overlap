import torch
import torch.nn as nn
import torch.nn.functional as F

class FC1_600(nn.Module):
    def __init__(self):
        super(FC1_600, self).__init__()
        self.fc1   = nn.Linear(28*28, 600)
        self.fc2   = nn.Linear(600, 2)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = self.fc2(out)
        return out

class FC1_600_l1d(nn.Module):
    def __init__(self):
        super(FC1_600_l1d, self).__init__()
        self.fc1   = nn.Linear(28*28, 600)
        self.fc2   = nn.Linear(600, 1)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = self.fc2(out)
        return out

class FC1_1200(nn.Module):
    def __init__(self):
        super(FC1_1200, self).__init__()
        self.fc1   = nn.Linear(28*28, 1200)
        self.fc2   = nn.Linear(1200, 2)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = self.fc2(out)
        return out

class FC1_1200_l1d(nn.Module):
    def __init__(self):
        super(FC1_1200_l1d, self).__init__()
        self.fc1   = nn.Linear(28*28, 1200)
        self.fc2   = nn.Linear(1200, 1)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = self.fc2(out)
        return out

class FC2_300(nn.Module):
    def __init__(self):
        super(FC2_300, self).__init__()
        self.fc1   = nn.Linear(28*28, 300)
        self.fc2   = nn.Linear(300, 300)
        self.fc3   = nn.Linear(300, 2)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = self.fc3(out)
        return out

class FC2_300_l1d(nn.Module):
    def __init__(self):
        super(FC2_300_l1d, self).__init__()
        self.fc1   = nn.Linear(28*28, 300)
        self.fc2   = nn.Linear(300, 300)
        self.fc3   = nn.Linear(300, 1)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = self.fc3(out)
        return out

class FC2_600(nn.Module):
    def __init__(self):
        super(FC2_600, self).__init__()
        self.fc1   = nn.Linear(28*28, 600)
        self.fc2   = nn.Linear(600, 600)
        self.fc3   = nn.Linear(600, 2)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = self.fc3(out)
        return out

class FC2_600_l1d(nn.Module):
    def __init__(self):
        super(FC2_600_l1d, self).__init__()
        self.fc1   = nn.Linear(28*28, 600)
        self.fc2   = nn.Linear(600, 600)
        self.fc3   = nn.Linear(600, 1)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = self.fc3(out)
        return out

class FC2_1200(nn.Module):
    def __init__(self):
        super(FC2_1200, self).__init__()
        self.fc1   = nn.Linear(28*28, 1200)
        self.fc2   = nn.Linear(1200, 1200)
        self.fc3   = nn.Linear(1200, 2)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = self.fc3(out)
        return out

class FC1_20_small(nn.Module):
    def __init__(self):
        super(FC1_20_small, self).__init__()
        self.fc1 = nn.Linear(14*14, 20)
        self.fc2 = nn.Linear(20, 1)

    def forward(self, x):
        out = F.max_pool2d(x, 2)
        out = out.view(out.size(0), -1)
        out = F.relu(self.fc1(out))
        out = self.fc2(out)
        return out

class FC1_40_small(nn.Module):
    def __init__(self):
        super(FC1_40_small, self).__init__()
        self.fc1 = nn.Linear(14*14, 40)
        self.fc2 = nn.Linear(40, 1)

    def forward(self, x):
        out = F.max_pool2d(x, 2)
        out = out.view(out.size(0), -1)
        out = F.relu(self.fc1(out))
        out = self.fc2(out)
        return out

class FC2_20_small(nn.Module):
    def __init__(self):
        super(FC2_20_small, self).__init__()
        self.fc1 = nn.Linear(14*14, 20)
        self.fc2 = nn.Linear(20, 20)
        self.fc3 = nn.Linear(20, 1)

    def forward(self, x):
        out = F.max_pool2d(x, 2)
        out = out.view(out.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = self.fc3(out)
        return out

class FC2_40_small(nn.Module):
    def __init__(self):
        super(FC2_40_small, self).__init__()
        self.fc1 = nn.Linear(14*14, 40)
        self.fc2 = nn.Linear(40, 40)
        self.fc3 = nn.Linear(40, 1)

    def forward(self, x):
        out = F.max_pool2d(x, 2)
        out = out.view(out.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = self.fc3(out)
        return out