import torch
import torch.nn as nn
import torch.nn.functional as F

class FC3(nn.Module):
    def __init__(self):
        super(FC3, self).__init__()
        self.fc1   = nn.Linear(28*28, 200)
        self.fc2   = nn.Linear(200, 200)
        self.fc3   = nn.Linear(200, 100)
        self.fc4   = nn.Linear(100, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = F.relu(self.fc3(out))
        out = self.fc4(out)
        return out

class FC2(nn.Module):
    def __init__(self):
        super(FC2, self).__init__()
        self.fc1   = nn.Linear(28*28, 200)
        self.fc2   = nn.Linear(200, 200)
        self.fc3   = nn.Linear(200, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = self.fc3(out)
        return out

class FC2_narrow(nn.Module):
    def __init__(self):
        super(FC2_narrow, self).__init__()
        self.fc1   = nn.Linear(28*28, 100)
        self.fc2   = nn.Linear(100, 100)
        self.fc3   = nn.Linear(100, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = self.fc3(out)
        return out

class FC2_narrow_30(nn.Module):
    def __init__(self):
        super(FC2_narrow_30, self).__init__()
        self.fc1   = nn.Linear(28*28, 30)
        self.fc2   = nn.Linear(30, 30)
        self.fc3   = nn.Linear(30, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = self.fc3(out)
        return out

class FC2_narrow_20(nn.Module):
    def __init__(self):
        super(FC2_narrow_20, self).__init__()
        self.fc1   = nn.Linear(28*28, 20)
        self.fc2   = nn.Linear(20, 20)
        self.fc3   = nn.Linear(20, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = self.fc3(out)
        return out


class FC1(nn.Module):
    def __init__(self):
        super(FC1, self).__init__()
        self.fc1   = nn.Linear(28*28, 200)
        self.fc2   = nn.Linear(200, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = self.fc2(out)
        return out

class FC3_small(nn.Module):
    def __init__(self):
        super(FC3_small, self).__init__()
        self.fc1 = nn.Linear(7*7, 40)
        self.fc2 = nn.Linear(40, 20)
        self.fc3 = nn.Linear(20, 10)

    def forward(self, x):
        out = F.max_pool2d(x, 4)
        out = out.view(out.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = self.fc3(out)
        return out