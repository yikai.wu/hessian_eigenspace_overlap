import torch

def get_partition(net):
    res = {}
    start = 0
    for i, (var_name, var_param) in enumerate(net.named_parameters()):
        end = start + var_param.view(-1).size()[0]
        print(var_name, start, end)
        res[var_name] = [start, end]
        start = end
    return res