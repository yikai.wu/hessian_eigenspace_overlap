import os
import torch
import numpy as np
import matplotlib as mpl
from matplotlib import cm
from matplotlib import gridspec
import matplotlib.pyplot as plt
import pynvml
mpl.use('Agg')
vis_dir = "Eigespace_Overlap/CIFAR10/"
plt.style.use('seaborn')

from subspace_overlap import overlap, file_fetch
from networks import LeNet
from subspace_overlap.overlap import Subspaces

epoch = -1
eval_dim = 1000
pca_dim = 1000
pca_rank = 1000
dims = np.concatenate([np.arange(1, 100, 20)])
dims = np.concatenate([np.arange(1, 500, 20), np.arange(500, 1001, 100)])
dircs = ['../LeNet5_AdamDefault']
dircs = ['../LeNet5_fixlr0.01', '../LeNet5_AdamDefault']

name_alias = '_X_'.join([x.split('/')[-1] for x in dircs])
desc = 'CIFAR10_{}_epoch1000'.format(name_alias)

eval_paths = file_fetch.fetch_dirc(dircs, eval_dim, epoch)
model_count = len(eval_paths)

print('Total Models: {}'.format(model_count))
subspace = Subspaces(eval_paths)
sim_raw = subspace.model_sim_cross()

pca_done = False
singular_values = None
while not pca_done:
    try:
        singular_values = subspace.pca_eigenspace(dim=pca_dim, rank_est=pca_rank)
        # singular_values = subspace.svd_eigenspace(dim=pca_dim)
        pca_done = True
    except:
        torch.cuda.empty_cache()
        pca_rank = int(pca_rank / 1.3)
        pca_dim = int(pca_dim / 1.3)
        print("Rank too high, attempt modified rank {}, dim {}".format(pca_rank, pca_dim))
        if pca_rank < 50 or pca_dim < 50:
            exit()
sigsquare_dist_normalized = np.power(singular_values, 2) / len(eval_paths)
tail_target = int(1.5 * (sigsquare_dist_normalized > 0.5).sum())
sigsquare_dist_normalized_short = sigsquare_dist_normalized[:tail_target]


overlaps_raw = []
for d in dims:
    print("Evaluating Dim {}".format(d))
    overlaps_raw.append(subspace.trace_overlap_cross(dim=d))

overlap_mean = np.zeros(len(dims))
overlap_std = np.zeros(len(dims))
for i in range(len(dims)):
    overlap_mean[i] = np.mean(overlaps_raw[i])
    overlap_std[i] = np.std(overlaps_raw[i])

img_name = "Dim2Overlap_{}.jpg".format(desc)
img_path = os.path.join(vis_dir, img_name)

cmap = cm.Set2 # pylint: disable=no-member
i = 0
gs = gridspec.GridSpec(2, 3, width_ratios=[3, 2, 1.5])
plt.figure(figsize=(15, 10))

plt.subplot(gs[0, 0])
plt.title(desc, fontsize=14)
plt.errorbar(dims, overlap_mean, yerr=overlap_std, marker='.', color=cmap(0.01 + (i / 8)))
plt.fill_between(dims, overlap_mean + overlap_std, overlap_mean - overlap_std, color=cmap(0.01 + (i / 8)), alpha=0.2)
plt.xlabel('Dimension')
plt.ylabel('Trace Overlap')

plt.subplot(gs[0, 1])
plt.title("Model Similarity (#params: {})".format(subspace.param_size), fontsize=14)
plt.hist(sim_raw)
plt.xlabel('Dot Product')

plt.subplot(gs[1, 0])
plt.title("σ^2 Distribution (Full Rank SVD)", fontsize=14)
plt.scatter(np.arange(len(sigsquare_dist_normalized)), sigsquare_dist_normalized, s=5, color=cmap(0.2))
plt.fill_between([0, len(sigsquare_dist_normalized)], 1/model_count, color=cmap(0.6), alpha=0.25)
plt.ylim([0,1])
plt.ylabel('σ^2 (Normalized)')
plt.xlabel('# of singular value')

plt.subplot(gs[1, 1])
plt.title("σ^2 Distribution (Zoom in)", fontsize=14)
plt.scatter(np.arange(len(sigsquare_dist_normalized_short)), sigsquare_dist_normalized_short, s=10, color=cmap(0.2))
plt.fill_between([0, len(sigsquare_dist_normalized_short)], 1/model_count, color=cmap(0.6), alpha=0.25)
plt.ylim([0,1])
plt.ylabel('σ^2 (Normalized)')
plt.xlabel('# of singular value')

plt.subplot(gs[0, 2])
plt.title("Loss (Gap: {:.4g})".format(subspace.metrics['gen_loss'][0]), fontsize=14)
plt.bar(0, subspace.metrics['train_loss'][0], yerr=subspace.metrics['train_loss'][1], label='train')
plt.bar(1, subspace.metrics['test_loss'][0], yerr=subspace.metrics['test_loss'][1], label='test')
plt.legend()

plt.subplot(gs[1, 2])
plt.title("Accuracy (Gap: {:.4g})".format(subspace.metrics['gen_acc'][0]), fontsize=14)
plt.bar(0, subspace.metrics['train_acc'][0], yerr=subspace.metrics['train_acc'][1], label='train')
plt.bar(1, subspace.metrics['test_acc'][0], yerr=subspace.metrics['test_acc'][1], label='test')
plt.ylim([0, 1.2])
plt.legend()

plt.tight_layout()
print("" + img_path.split("Visualizations")[1])
plt.savefig(img_path, dpi=200)
