import os, sys
import torch
import numpy as np
import matplotlib as mpl
from matplotlib import cm
from matplotlib import gridspec
import matplotlib.pyplot as plt
import pynvml
mpl.use('Agg')
from matplotlib import rc
plt.style.use('ggplot')
rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)

from subspace_overlap import overlap, file_fetch
from networks import LeNet
from subspace_overlap.overlap import Subspaces

def get_image_path(name, store_format='jpg'):
    vis_dir = ""
    img_path = os.path.join(vis_dir, "{}.{}".format(name, store_format))
    print("" + img_path.split("Shared")[1] + '\n')
    return img_path

epoch = -1
eval_dim = 500
pca_dim = 500
pca_rank = 500
dims = np.concatenate([np.arange(1, 200, 2)])
# dims = np.concatenate([np.arange(1, 100, 2), np.arange(101, 250, 10)])
dirc = '../LeNet5_BN_nl_fixlr0.01'


assert os.path.isdir(dirc)
sys.path.append(dirc)
from config import Config # pylint: disable=no-name-in-module
conf = Config()

net = conf.net()
print(net.state_dict().keys())
exp_name = 'DimOverlap_CIFAR10_{}_epoch1000'.format(dirc[3:])

dircs = [dirc]
eval_paths = file_fetch.fetch_dirc(dircs, eval_dim, epoch, arg="_LW_ET")
model_count = len(eval_paths)

print('Total Models: {}'.format(model_count))
subspace = Subspaces(eval_paths)
sim_raw = subspace.model_sim_cross()

assert subspace.layer_info is not None
print(subspace.layer_info)
layers = []# ["full"]
for k in subspace.layer_info:
    print(k)
    if not 'bias' in k:
        layers.append(k)
print(layers)
layer_width = {}
for layer in layers:
    if layer != 'full':
        layer_width[layer] = net.state_dict()[layer].shape[0]
    else:
        layer_width[layer] = subspace.metas[0]['eigenvecs'].size()[1]
print(layer_width)

sigsquares = []
for layer in layers:
    pca_done = False
    singular_values = None
    if layer != 'full':
        eigen_count = subspace.layer_info[layer][1] - subspace.layer_info[layer][0]
        print(layer, eigen_count)
    else:
        eigen_count = 1000000000
        print(layer)
    pca_rank_tmp = min(pca_rank, eigen_count)
    pca_dim_tmp = min(pca_dim, eigen_count)
    while not pca_done:
        try:
            singular_values = subspace.pca_eigenspace_layer(dim=pca_dim_tmp, layer=layer, rank_est=pca_rank_tmp)
            # singular_values = subspace.svd_eigenspace(dim=pca_dim)
            pca_done = True
        except:
            torch.cuda.empty_cache()
            pca_rank = int(pca_rank_tmp / 1.3)
            pca_dim = int(pca_dim_tmp / 1.3)
            print("Rank too high, attempt modified rank {}, dim {}".format(pca_rank_tmp, pca_dim_tmp))
            if pca_rank_tmp < 5 or pca_dim_tmp < 5:
                pca_done = True
    if singular_values is not None:
        sigsquare_dist_normalized = np.power(singular_values, 2) / len(eval_paths)
        sigsquares.append(sigsquare_dist_normalized)
    else:
        sigsquares.append(None)

overlap_means = []
overlap_stds = []
overlap_dims = []

for layer in layers:

    overlaps_raw = []
    dims_layer = dims
    if layer != 'full':
        cnt = subspace.layer_info[layer][1] - subspace.layer_info[layer][0]
        pos = np.sum(dims_layer < cnt)
        dims_layer = dims_layer[:pos]
    print(dims_layer)
    dims_layer = dims_layer
    overlap_dims.append(dims_layer)
    for d in dims_layer:
        print("Evaluating Dim {} {}".format(d, layer))
        overlaps_raw.append(subspace.trace_overlap_cross_layer(dim=d, layer=layer))

    overlap_mean = np.zeros(len(dims_layer))
    overlap_std = np.zeros(len(dims_layer))
    for i in range(len(dims_layer)):
        overlap_mean[i] = np.mean(overlaps_raw[i])
        overlap_std[i] = np.std(overlaps_raw[i])
    overlap_means.append(overlap_mean)
    overlap_stds.append(overlap_std)

cmap = cm.Set2 # pylint: disable=no-member
i = 0
plt.figure(figsize=(8, 3.5))

for i, layer in enumerate(layers):
    plt.errorbar(overlap_dims[i], overlap_means[i], yerr=overlap_stds[i], marker='.', color=cmap(0.01 + (i / 8)), label="{}: {}".format(layer, layer_width[layer]))
    plt.fill_between(overlap_dims[i], overlap_means[i] + overlap_stds[i], overlap_means[i] - overlap_stds[i], color=cmap(0.01 + (i / 8)), alpha=0.2)
plt.legend(loc='upper right', fontsize=10)
plt.xlabel('Dimension of Top Eigenspace', fontsize=10)
plt.ylabel('Subspace Overlap', fontsize=10)

plt.tight_layout()
image_path = get_image_path(exp_name, 'pdf')
plt.savefig(image_path)
