import os, sys
import torch
import numpy as np
import matplotlib as mpl
from matplotlib import cm
from matplotlib import gridspec
import matplotlib.pyplot as plt
import pynvml
mpl.use('Agg')
from matplotlib import rc
plt.style.use('seaborn-deep')
rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)
params= {'text.latex.preamble' : [r'\usepackage{amsmath}',r'\usepackage{amssymb}', r'\usepackage{bm}']}
plt.rcParams.update(params)

plt.style.use('seaborn-deep')
plt.rcParams['xtick.labelsize']= 12
plt.rcParams['ytick.labelsize']= 12
plt.rcParams['axes.labelsize'] = 14
plt.rcParams['axes.titlesize'] = 14

from subspace_overlap import overlap, file_fetch
from networks import LeNet
from subspace_overlap.overlap import Subspaces

def get_image_path(name, store_format='jpg'):
    vis_dir = ""
    img_path = os.path.join(vis_dir, "{}.{}".format(name, store_format))
    print("" + img_path.split("Shared")[1] + '\n')
    return img_path

epoch = -1
eval_dim = 500
pca_dim = 500
pca_rank = 500
dims = np.concatenate([np.arange(1, 50, 2), np.arange(51, 200, 10)])
# dims = np.concatenate([np.arange(1, 100, 2), np.arange(101, 250, 10)])
dircs = ['../LeNet5_fc1_100']
dircs = ['../LeNet5_fc1_150']
dircs = ['../LeNet5_fc1_80']
dircs = ['../LeNet5_conv2_25']
dircs = ['../LeNet5_normnew_fixlr0.01']
dircs = ['../LeNet5_BN_nl_fixlr0.01']
dircs = ['../LeNet5_normnew_fixlr0.001', '../LeNet5_normnew_fixlr0.01', '../LeNet5_normnew_fixlr0.01_momentum']
dircs = ['../VGG11NN_nobn_fixlr0.01']
dircs = ['../LeNet5_sigmoid_fixlr0.01']
dircs = ['../LeNet5_fixlr0.1_dMSE']
dircs = ['../LeNet5_tanh_fixlr0.01']
dircs = ['../LeNet5_sigmoid_fixlr0.01']
dircs = ['../LeNet5_LN_nl_fixlr0.01']

name_alias = '_X_'.join([x.split('/')[-1] for x in dircs])
exp_name = 'DimOverlap_CIFAR10_{}'.format(name_alias)

if len(dircs) == 1:
    store_file = dircs[0] + '/overlap_layerwise_{}_{}_{}.ovlp'.format(min(dims), max(dims), len(dims))
else:
    store_file = './ovlp_log/overlap_layerwise_{}_{}_{}_{}.ovlp'.format(name_alias, min(dims), max(dims), len(dims))

load_log = True
if os.path.isfile(store_file) and load_log:
    overlap_means, overlap_stds, overlap_dims, layer_width, layers = torch.load(store_file, map_location='cpu')
    print('loaded previously computed result')

else:
    assert os.path.isdir(dircs[0])
    sys.path.append(dircs[0])
    from config import Config # pylint: disable=no-name-in-module
    conf = Config()

    net = conf.net()
    print(net.state_dict().keys())

    #eval_paths = file_fetch.fetch_dirc(dircs, eval_dim, epoch, arg="_LW_part_t18_l")
    eval_paths = file_fetch.fetch_dirc(dircs, eval_dim, epoch, arg="_LW_ET")
    model_count = len(eval_paths)

    print('Total Models: {}'.format(model_count))
    subspace = Subspaces(eval_paths)

    assert subspace.layer_info is not None
    print(subspace.layer_info)
    layers = []# ["full"]
    for k in subspace.layer_info:
        print(k)
        if not 'bias' in k:
            layers.append(k)
    print(layers)
    layer_width = {}
    for layer in layers:
        if layer != 'full':
            layer_width[layer] = net.state_dict()[layer].shape[0]
        else:
            layer_width[layer] = subspace.metas[0]['eigenvecs'].size()[1]
    print(layer_width)

    overlap_means = []
    overlap_stds = []
    overlap_dims = []

    for layer in layers:

        overlaps_raw = []
        dims_layer = dims
        if layer != 'full':
            cnt = subspace.layer_info[layer][1] - subspace.layer_info[layer][0]
            pos = np.sum(dims_layer < cnt)
            dims_layer = dims_layer[:pos]
        print(dims_layer)
        dims_layer = dims_layer
        overlap_dims.append(dims_layer)
        for d in dims_layer:
            print("Evaluating Dim {} {}".format(d, layer))
            overlaps_raw.append(subspace.trace_overlap_cross_layer(dim=d, layer=layer))

        overlap_mean = np.zeros(len(dims_layer))
        overlap_std = np.zeros(len(dims_layer))
        for i in range(len(dims_layer)):
            overlap_mean[i] = np.mean(overlaps_raw[i])
            overlap_std[i] = np.std(overlaps_raw[i])
        overlap_means.append(overlap_mean)
        overlap_stds.append(overlap_std)
    
    torch.save([overlap_means, overlap_stds, overlap_dims, layer_width, layers], store_file)
    print('save computed result to {}'.format(store_file))

cmap = cm.Set2 # pylint: disable=no-member
i = 0

# single plot
xmax = 2000
if False:
    for i, layer in enumerate(layers):
        plt.figure(figsize=(3, 3))
        cap = min(layer_width[layer] * 3, xmax)
        plt.plot(overlap_dims[i][:cap], overlap_means[i][:cap])
        plt.fill_between(overlap_dims[i][:cap], overlap_means[i][:cap] + overlap_stds[i][:cap], overlap_means[i][:cap] - overlap_stds[i][:cap], alpha=0.2)
        bottom, top = plt.ylim() 
        plt.ylim([0, top])
        plt.xlabel('Dimension', fontsize=12)
        plt.xlim([0, min(xmax, max(overlap_dims[i][:cap]))])
        plt.axvline(layer_width[layer], linestyle=":", label=r"Output Dim: {}".format(layer_width[layer]))
        plt.legend(loc=4)
        plt.tight_layout()
        image_path = get_image_path(exp_name + "_{}".format(layer[:-7]), 'pdf')
        plt.savefig(image_path, bbox_inches='tight')

# stitched plot
if True:
    plt.figure(figsize=(20, 3))
    gs = gridspec.GridSpec(1, len(layers))
    for i, layer in enumerate(layers):
        plt.subplot(gs[0, i])

        cap = layer_width[layer] * 3
        plt.plot(overlap_dims[i][:cap], overlap_means[i][:cap])
        plt.fill_between(overlap_dims[i][:cap], overlap_means[i][:cap] + overlap_stds[i][:cap], overlap_means[i][:cap] - overlap_stds[i][:cap], alpha=0.2)
        bottom, top = plt.ylim() 
        plt.ylim([0, top])
        plt.axvline(layer_width[layer], linestyle=":")
    
        plt.xlabel('Dimension', fontsize=12)
        plt.title(r'{} $m={}$'.format(layer[:-7], layer_width[layer]))

        plt.tight_layout(pad=0.2)
    image_path = get_image_path(exp_name + "_appendix_full", 'pdf')
    plt.savefig(image_path, bbox_inches='tight')

if True:
    plt.figure(figsize=(8, 5))
    gs = gridspec.GridSpec(3, 3)
    for i, layer in enumerate(layers):
        plt.subplot(gs[i//3, i%3])

        cap = layer_width[layer] * 3
        plt.plot(overlap_dims[i][:cap], overlap_means[i][:cap], label=r'{} $m={}$'.format(layer[:-7], layer_width[layer]))
        plt.fill_between(overlap_dims[i][:cap], overlap_means[i][:cap] + overlap_stds[i][:cap], overlap_means[i][:cap] - overlap_stds[i][:cap], alpha=0.2)
        bottom, top = plt.ylim() 
        plt.ylim([0, top])
        plt.legend(loc=4)
        plt.axvline(layer_width[layer], linestyle=":")
        if i//3 == 2:
            plt.xlabel('Dimension', fontsize=12)
        if i%3 == 0:
            plt.ylabel('Overlap', fontsize=12)
        # plt.title()

        plt.tight_layout(pad=0.2)
    image_path = get_image_path(exp_name + "_response_full", 'pdf')
    plt.savefig(image_path, bbox_inches='tight')