#!/bin/bash
#SBATCH -t 90:00:00
#SBATCH -o ./sbatchlog/eval-%A-%a.out
#SBATCH -c 4
#SBATCH --mem=32G
#SBATCH --partition=compsci-gpu
#SBATCH --gres=gpu:1
#SBATCH --exclude=linux56
#SBATCH --array=1000,900,800,700,600,500,400,300,200,100,0%2
python3 eval_epoch.py -n=1000 -mdir='/usr/xtmp/CSPlus/VOLDNN/Shared/train_log/eigenspace_overlap_cifar10/CIFAR10_Exp1/Resnet18_fixlr0.01/run_4/models'