import os, sys
import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.use('Agg')

def get_image_path(name, store_format='jpg'):
    vis_dir = ""
    img_path = os.path.join(vis_dir, "{}.{}".format(name, store_format))
    print("" + img_path.split("Shared")[1] + '\n')
    return img_path

def plot_single(name, mat, title=None):
    plt.figure(figsize=(6, 3), dpi=600)
    plt.imshow(mat.cpu(), cmap='Reds', interpolation='nearest')
    plt.grid(False)
    plt.box(on=None)
    # for direction in ["top", "bottom", "left", "right"]:
    #     axes[0].spines[direction].set_visible(False)
    plt.title(title)
    for spine in plt.gca().spines.values():
        spine.set_visible(False)
        print(spine)
    image_path = get_image_path(name, store_format='pdf')
    plt.tight_layout()
    plt.savefig(image_path, bbox_inches='tight')