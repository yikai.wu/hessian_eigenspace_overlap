import os, sys
import torch
import numpy as np
from algos.closeformhessian import hessianmodule
from tools import file_fetch
from tqdm import tqdm

def l2_distmat_parallel(mat, no_sqrt=False):

    """Compute the distance matrix in parallel
    Input:  torch 2D tensor [v1, v2, ..., vn]
    Output: torch 2D tensor [[d(v1, v1), d(v1, v2) ... d(v1, vn)],
                            \...                               ...
                             [d(vn, v1), d(vn, v2) ... d(vn, vn)]]
    """
    N = mat.shape[0]
    dist = torch.zeros([N, N]) # pylint:disable=no-member
    
    for i in range(1, N):
        diff_mat = mat[i: ] - mat[i - 1: i]
        l2 = diff_mat.square_().sum(dim=-1)
        if not no_sqrt:
            l2.sqrt_()
        dist[i - 1][i:] = l2
    dist += dist.transpose(0, 1)
    return dist


def comp_gamma_ccp(HM: hessianmodule.HessianModule, comp_layer, C, batchsize=None):
    
    """Computing the $\sigma_{\delta, \delta'} for half of UTAU$"""
    print("Computing δ(c,c') for {}".format(comp_layer))
    
    Q_comp = HM.decomp.UTALPh_comp
    dl = HM.dl
    if batchsize is not None:
        dl.set_batchsize(batchsize)

    gamma_ccp_ls, gamma_c_ls = [], []
    gamma_ccp_inds, gamma_c_inds = [], []

    for label in tqdm(range(C)):
        HM.set_remain_labels([label])
        count = 0

        UTQ_base = None

        for inputs, labels in iter(dl):
            count += inputs.shape[0]
            assert torch.Tensor.sum(labels == label) == inputs.shape[0], labels
            UTQ = HM.comp_output(Q_comp, [comp_layer], inputs, batch_sum=True, out_device=HM.device)[comp_layer]
            
            if UTQ_base is None:
                UTQ_base = UTQ
            else:
                UTQ_base += UTQ
        
        UTQ_base.div_(count).transpose_(0, 1)

        gamma_ccp_ls.append(UTQ_base)

        gamma_ccp_inds += [label for i in range(UTQ_base.shape[0])]

    gamma_ccp = torch.cat(gamma_ccp_ls) # pylint:disable=no-member
    gamma_ccp_inds = np.array(gamma_ccp_inds)

    return gamma_ccp, gamma_ccp_inds

def comp_delta_ccp(HM: hessianmodule.HessianModule, comp_layer, C, batchsize=None):
    
    """Computing the $\sigma_{\delta, \delta'} for half of xUTAUxT$"""
    print("Computing δ(c,c') for {}".format(comp_layer))
    
    Delta_comp = HM.decomp.xTUTALPh_comp
    dl = HM.dl
    if batchsize is not None:
        dl.set_batchsize(batchsize)

    delta_ccp_ls, delta_c_ls = [], []
    delta_ccp_inds, delta_c_inds = [], []

    for label in tqdm(range(C)):
        HM.set_remain_labels([label])
        count = 0

        Delta_base = None

        for inputs, labels in iter(dl):
            count += inputs.shape[0]
            assert torch.Tensor.sum(labels == label) == inputs.shape[0], labels
            Delta = HM.comp_output(Delta_comp, [comp_layer], inputs, batch_sum=True, out_device=HM.device)[comp_layer]
            
            if Delta_base is None:
                Delta_base = Delta
            else:
                Delta_base += Delta
        
        Delta_base.div_(count).transpose_(0, 1)

        delta_ccp_ls.append(Delta_base)

        delta_ccp_inds += [label for i in range(Delta_base.shape[0])]

    delta_ccp = torch.cat(delta_ccp_ls) # pylint:disable=no-member
    delta_ccp_inds = np.array(delta_ccp_inds)

    return delta_ccp, delta_ccp_inds

if __name__ == "__main__":
    mat = torch.tensor([[1, 1, 1, 1], [2, 2, 2, 2], [3, 3, 3, 3], [4, 4, 4, 4], [5, 5, 5, 5]]).float() # pylint:disable=not-callable
    l2_distmat_parallel(mat)