# import MulticoreTSNE
import os, sys
import torch
import numpy as np
from sklearn.manifold import TSNE as sklearn_tsne

import matplotlib as mpl
mpl.use('Agg')
from matplotlib import cm
from matplotlib import gridspec
from matplotlib import rc
import matplotlib.pyplot as plt
plt.style.use('seaborn-deep')
rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)
params= {'text.latex.preamble' : [r'\usepackage{amsmath}',r'\usepackage{amssymb}', r'\usepackage{bm}']}
plt.rcParams.update(params)

def get_image_path(name, store_format='jpg'):
    vis_dir = ""
    img_path = os.path.join(vis_dir, "{}.{}".format(name, store_format))
    print("" + img_path.split("Shared")[1] + '\n')
    return img_path

def tsne_c_cp(dist, inds, C):
    tsne_embedded = sklearn_tsne(n_components=2,
                     metric='precomputed',
                     perplexity=C).fit_transform(dist)
    centers, others = tsne_embedded[:C], tsne_embedded[C:]
    center_inds, other_inds = inds[:C], inds[C:]
    return centers, center_inds, others, other_inds

def tsne_c_cp_pred(dist, inds, C):
    tsne_embedded = sklearn_tsne(n_components=2,
                     metric='precomputed',
                     perplexity=C).fit_transform(dist)
    centers, others, preds = tsne_embedded[:C], tsne_embedded[C:2*C], tsne_embedded[2*C:]
    center_inds, other_inds, pred_inds = inds[:C], inds[C:2*C], inds[2*C:]
    return centers, center_inds, others, other_inds, preds, pred_inds

def visualize_tsne_withcenter_class_gamma(centers, center_inds, others, other_inds, name, store_format='pdf'):

    cmap = cm.rainbow # pylint:disable=no-member
    image_path = get_image_path(name, store_format)
    plt.figure(figsize=(5, 5))
    plt.scatter(centers[:, 0], centers[:, 1], c=center_inds, s=300, alpha=0.2, cmap='rainbow', label='class cluster centers $\hat{\Gamma}_{i}$')
    plt.scatter(others[:, 0], others[:, 1], c=other_inds, s=10, cmap='rainbow', label="cluster members $\Gamma_{i,j}$")
    for ind in center_inds:
        # plt.text(centers[ind][0], centers[ind][1], s=str(ind), c=cmap(ind / len(center_inds)), horizontalalignment='center', verticalalignment='center')
        plt.text(centers[ind][0], centers[ind][1], s=str(ind), c='black', horizontalalignment='center', verticalalignment='center')
    plt.legend(bbox_to_anchor=(0,1.02,1,0.2), loc="lower left", mode="expand", borderaxespad=0, ncol=2)
    
    # plt.title(name)
    plt.xlabel('t-SNE X')
    plt.ylabel('t-SNE Y')
    
    plt.tight_layout()
    plt.savefig(image_path, bbox_inches='tight')

def eigenspec_vis(D, name, store_format='pdf'):

    cmap = cm.rainbow # pylint:disable=no-member
    image_path = get_image_path(name, store_format)
    plt.figure(figsize=(3, 5))
    plt.scatter(range(20), D[:20])
    
    # plt.title(name)
    plt.ylabel('Eigenvalues')
    
    plt.tight_layout()
    plt.savefig(image_path, bbox_inches='tight')


def visualize_tsne_withcenter_class_delta(centers, center_inds, others, other_inds, name, store_format='pdf'):

    cmap = cm.rainbow # pylint:disable=no-member
    image_path = get_image_path(name, store_format)
    plt.figure(figsize=(5, 5))
    plt.scatter(centers[:, 0], centers[:, 1], c=center_inds, s=300, alpha=0.2, cmap='rainbow', label='class center $\hat{\Delta}_{i}$')
    plt.scatter(others[:, 0], others[:, 1], c=other_inds, s=10, cmap='rainbow', label="cluster members $\Delta_{i,j}$")
    for ind in center_inds:
        # plt.text(centers[ind][0], centers[ind][1], s=str(ind), c=cmap(ind / len(center_inds)), horizontalalignment='center', verticalalignment='center')
        plt.text(centers[ind][0], centers[ind][1], s=str(ind), c='black', horizontalalignment='center', verticalalignment='center')
    plt.legend(bbox_to_anchor=(0,1.02,1,0.2), loc="lower left", mode="expand", borderaxespad=0, ncol=2)
    
    # plt.title(name)
    plt.xlabel('t-SNE X')
    plt.ylabel('t-SNE Y')
    
    plt.tight_layout()
    plt.savefig(image_path, bbox_inches='tight')

def visualize_tsne_withcenter_logits(centers, center_inds, others, other_inds, name, store_format='pdf'):

    cmap = cm.rainbow # pylint:disable=no-member
    image_path = get_image_path(name, store_format)
    plt.figure(figsize=(5, 5))
    p1 = plt.scatter(centers[:, 0], centers[:, 1], c=center_inds, s=200, alpha=0.2, cmap='rainbow', label="logit cluster centers $\delta_c'$")
    p2 = plt.scatter(others[:, 0], others[:, 1], c=other_inds, s=30, cmap='rainbow', label="cluster members $\delta_{c,c'}$")
    for ind in center_inds:
        # plt.text(centers[ind][0], centers[ind][1], s=str(ind), c=cmap(ind / len(center_inds)), horizontalalignment='center', verticalalignment='center')
        plt.text(centers[ind][0], centers[ind][1], s=str(ind), c='black', horizontalalignment='center', verticalalignment='center')
    # plt.legend(handles=[p1, p2], loc='upper left', bbox_to_anchor=(1.01, 1))
    plt.legend(bbox_to_anchor=(0,1.02,1,0.2), loc="lower left", mode="expand", borderaxespad=0, ncol=2)
    
    # plt.title(name)
    plt.xlabel('t-SNE X')
    plt.ylabel('t-SNE Y')
    
    plt.tight_layout()
    plt.savefig(image_path, bbox_inches='tight')

def visualize_tsne_withcenter_logits_pred_gamma(centers, center_inds, preds, pred_inds, others, other_inds, name, store_format='pdf'):

    cmap = cm.rainbow # pylint:disable=no-member
    image_path = get_image_path(name, store_format)
    plt.figure(figsize=(5, 5))
    p1 = plt.scatter(centers[:, 0], centers[:, 1], c=center_inds, s=300, alpha=0.2, cmap='rainbow', label="logit centers $\check{\Gamma}_{j}$")
    p2 = plt.scatter(others[:, 0], others[:, 1], c=other_inds, s=10, alpha=0.8, cmap='rainbow', label="""cluster members $\Gamma_{i,j}$""")
    p3 = plt.scatter(preds[:, 0], preds[:, 1], c=pred_inds, s=100, marker="x", alpha=1, cmap='rainbow', label="Approximated logit centers $\Breve{\Gamma}_{j}$")
    for ind in center_inds:
        # plt.text(centers[ind][0], centers[ind][1], s=str(ind), c=cmap(ind / len(center_inds)), horizontalalignment='center', verticalalignment='center')
        plt.text(centers[ind][0], centers[ind][1] + 2, s="$j={}$".format(ind), c='black', horizontalalignment='center', verticalalignment='center')
    # plt.legend(handles=[p1, p2], loc='upper left', bbox_to_anchor=(1.01, 1))
    plt.legend(bbox_to_anchor=(0,1.02,1,0.2), loc="lower left", mode="expand", borderaxespad=0, ncol=2)
    
    # plt.title(name)
    plt.xlabel('t-SNE X')
    plt.ylabel('t-SNE Y')
    
    # plt.tight_layout()
    plt.savefig(image_path, bbox_inches='tight')

def visualize_tsne_withcenter_logits_pred_delta(centers, center_inds, preds, pred_inds, others, other_inds, name, store_format='pdf'):

    cmap = cm.rainbow # pylint:disable=no-member
    image_path = get_image_path(name, store_format)
    plt.figure(figsize=(5, 5))
    p1 = plt.scatter(centers[:, 0], centers[:, 1], c=center_inds, s=300, alpha=0.2, cmap='rainbow', label="logit centers $\check{\Delta}_{j}$")
    p2 = plt.scatter(others[:, 0], others[:, 1], c=other_inds, s=10, alpha=0.8, cmap='rainbow', label="""cluster members $\Delta_{i,j}$""")
    p3 = plt.scatter(preds[:, 0], preds[:, 1], c=pred_inds, s=100, marker="x", alpha=1, cmap='rainbow', label="Approximated logit centers $\Breve{\Delta}_{j}$")
    for ind in center_inds:
        # plt.text(centers[ind][0], centers[ind][1], s=str(ind), c=cmap(ind / len(center_inds)), horizontalalignment='center', verticalalignment='center')
        plt.text(centers[ind][0], centers[ind][1] + 2, s="$j={}$".format(ind), c='black', horizontalalignment='center', verticalalignment='center')
    # plt.legend(handles=[p1, p2], loc='upper left', bbox_to_anchor=(1.01, 1))
    plt.legend(bbox_to_anchor=(0,1.02,1,0.2), loc="lower left", mode="expand", borderaxespad=0, ncol=2)
    
    # plt.title(name)
    plt.xlabel('t-SNE X')
    plt.ylabel('t-SNE Y')
    
    # plt.tight_layout()
    plt.savefig(image_path, bbox_inches='tight')

if __name__ == "__main__":

    # dist_delta_c_ccp, delta_c_ccp_inds = torch.load('./tmp_delta.pth')
    # centers, center_inds, others, other_inds = tsne_c_cp(dist_delta_c_ccp, delta_c_ccp_inds, C=10)
    # visualize_tsne_withcenter(centers, center_inds, others, other_inds, 'tsne_delta_test')

    dist_gamma_c_ccp, gamma_c_ccp_inds = torch.load('./tmp_gamma.pth')
    centers, center_inds, others, other_inds = tsne_c_cp(dist_gamma_c_ccp, gamma_c_ccp_inds, C=10)
    visualize_tsne_withcenter(centers, center_inds, others, other_inds, 'tsne_gamma_test')
    
