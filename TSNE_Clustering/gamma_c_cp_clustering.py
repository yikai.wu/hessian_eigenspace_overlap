import os, sys
import torch
import numpy as np
from algos.closeformhessian import hessianmodule
from tools import file_fetch
import half_decomposition
from tsne_visualize import *

dirc = '../MNIST_Exp1/experiments/FC2_narrow30_fixlr0.01'
dirc = '../MNIST_RandomLabel/experiments/FC2_fixlr0.01_RL'
dirc = '../MNIST_Exp1/experiments/FC2_20_small_fixlr0.01'
dirc = '../CIFAR10_Exp1/experiments/LeNet5_normnew_fixlr0.01'
dirc = '../MNIST_Exp1/experiments/FC2_fixlr0.01'
dirc = '../CIFAR10_Exp1/experiments/FC3_wide_fixlr0.01'
epoch = 0
run = 1

storage_path = os.path.join(dirc, 'experiment_log/run_{}'.format(run))
remain_labels = None

crop_ratio = 0.1
comp_layers = ['fc1'] # , 'fc2', 'fc3']
comp_layers = ['fc1', 'fc2', 'fc3', 'fc4']
fc_layers = ['fc1', 'fc2', 'fc3', 'fc4']

exp_name = dirc.split('/')[-3] + '_' + dirc.split('/')[-1] + "_E{}".format(epoch)
name = exp_name + '_gamma_ccp_cluster'

assert os.path.isdir(dirc)
sys.path.append(dirc)
snapshot_file = file_fetch.fetch_snapshot(dirc, epoch=epoch, run_name=run)
from config import Config # pylint: disable=no-name-in-module
conf = Config()

net = conf.net()
net.load_state_dict(torch.load(snapshot_file, map_location='cuda' if torch.cuda.is_available() else 'cpu'))
dataset = conf.dataset(train=True, transform=conf.test_transform)
HM = hessianmodule.HessianModule(net, dataset, fc_layers, RAM_cap=5, remain_labels=remain_labels)
C = HM.Ws[0].shape[0]

E_Q = HM.expectation(HM.decomp.ALPh_comp, fc_layers)[fc_layers[0]]

W_chains = [torch.eye(C, device=HM.device)] #pylint:disable=no-member
for i, layer in enumerate(fc_layers[:-1]):
    W_chains.append(torch.Tensor.matmul(W_chains[-1], HM.Ws[i]))

W_chain_dict = {}
for i in range(len(fc_layers)):
    print(fc_layers[i], W_chains[-i-1].shape)
    W_chain_dict[fc_layers[i]] = W_chains[-i-1]

for i, layer in enumerate(comp_layers):

    layer_dist = len(fc_layers) - i - 1
    print(layer, layer_dist, 2**layer_dist)
    UT = W_chain_dict[layer].transpose(0, 1) / (2 ** layer_dist)
    UTQ = torch.mm(UT, E_Q)
    gamma_ccp, gamma_ccp_inds = half_decomposition.comp_gamma_ccp(HM, layer, C, 128)

    gamma_c = gamma_ccp.view(10, 10, gamma_ccp.shape[-1]).mean(dim=1)
    class_inds = np.concatenate([np.arange(10), gamma_ccp_inds])
    class_gammas = torch.cat([gamma_c, gamma_ccp])
    
    class_dist = half_decomposition.l2_distmat_parallel(class_gammas, no_sqrt=True)
    centers, center_inds, others, other_inds = tsne_c_cp(class_dist, class_inds, C=10)
    visualize_tsne_withcenter_class(centers, center_inds, others, other_inds, "CL_{}_{}".format(name, layer))

    gamma_cp = gamma_ccp.view(10, 10, gamma_ccp.shape[-1]).mean(dim=0)
    gamma_cp_pred = UTQ.transpose(0, 1)
    logit_inds = np.concatenate([np.arange(10) for i in range(11)])
    # logit_gammas = torch.cat([gamma_cp, gamma_ccp])
    logit_gammas = torch.cat([gamma_cp_pred, gamma_ccp])
    
    logit_dist = half_decomposition.l2_distmat_parallel(logit_gammas, no_sqrt=True)
    centers, center_inds, others, other_inds = tsne_c_cp(logit_dist, logit_inds, C=10)
    visualize_tsne_withcenter_logits(centers, center_inds, others, other_inds, "LL_{}_{}".format(name, layer))


# exit()
# tsne_visualize()
# print(dist_gamma_c_ccp)