import os, sys
import torch
import numpy as np
from algos.closeformhessian import hessianmodule
from evaluations import component_compare, utau_eig_propagation, samples, misc, gradients_analysis
from tools import file_fetch

dirc = '../CIFAR10_Exp1/experiments/LeNet5_AdamDefault'
dirc = '../CIFAR10_Exp1/experiments/LeNet5_fixlr0.01'
dirc = '../CIFAR10_RandomLabel/experiments/LeNet5_fixlr0.01_RL'
dirc = '../MNIST_RandomLabel/experiments/FC2_fixlr0.01_RL'
#dirc = '../MNIST_Exp1/experiments/FC2_fixlr0.01'
epoch = -1
run = 2

load_evals = False
remain_labels = None
crop_ratio = 0.001
comp_layers = ['fc1', 'fc2', 'fc3']
fc_layers = ['fc1', 'fc2', 'fc3']

def tasks_gradient(HM, eigenvals_real, eigenvecs_real, exp_name):
    gradients_analysis.UxT_overlap(HM, comp_layers, 'UxT_Overlap', crop_ratio)
    return

def tasks(HM, eigenvals_real, eigenvecs_real, exp_name):
    tasks_gradient(HM, eigenvals_real, eigenvecs_real, exp_name)

def main():
    snapshot_file = file_fetch.fetch_snapshot(dirc, epoch=epoch, run_name=run)
    if load_evals:
        eval_file = file_fetch.fetch_eval_file(dirc, epoch=epoch, run_name=run)

    assert os.path.isdir(dirc)
    sys.path.append(dirc)
    from config import Config # pylint: disable=no-name-in-module
    conf = Config()

    net = conf.net()
    net.load_state_dict(torch.load(snapshot_file, map_location='cuda' if torch.cuda.is_available() else 'cpu'))
    dataset = conf.dataset(train=True, transform=conf.test_transform)
    HM = hessianmodule.HessianModule(net, dataset, fc_layers, RAM_cap=64, remain_labels=remain_labels)

    exp_name = dirc.split('/')[-1] + "_Cov{}".format(epoch)
    dataset = conf.dataset(train=True, transform=conf.test_transform)
    if remain_labels is not None:
        exp_name += '_y' + ''.join([str(i) for i in remain_labels]) # pylint: disable=not-an-iterable

    if load_evals:
        eigenthings_real = torch.load(eval_file, map_location='cpu')
        eigenvals_real = eigenthings_real['eigenvals_layer']
        eigenvecs_real = eigenthings_real['eigenvecs_layer']
    else:
        eigenvals_real, eigenvecs_real = None, None

    gradients_analysis.U_dot_expectation(HM, comp_layers, exp_name)

if __name__ == '__main__':
    main()