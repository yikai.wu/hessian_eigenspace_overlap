import torch
import sys, os, copy
import numpy as np
from tools import file_fetch
from torch.utils.data import DataLoader
from torch.nn import functional, Conv2d

from algos.closeformhessian.visualization import vis
from algos.closeformhessian.measures import Measures
from algos.closeformhessian.layerwisefeature import IntermediateFeatureExtractor, rgetattr
from algos.closeformhessian.dp import OnDeviceDataLoader
from algos.closeformhessian.utils import *

from decomp.unfoldx import *

stop_count = 10

dirc = '../CIFAR10_Exp1/experiments/LeNet5_fixlr0.01'
layer_seq = ['conv1', 'conv2', 'fc1', 'fc2', 'fc3']
comp_layers = ['conv1', 'conv2']

epoch = -1
run = 1
storage_path = os.path.join(dirc, 'experiment_log/run_{}'.format(run))
snapshot_file = file_fetch.fetch_snapshot(dirc, epoch=epoch, run_name=run)
exp_name = dirc.split('/')[-3] + '_' + dirc.split('/')[-1] + "_E{}".format(epoch)

lw_hessian_file = snapshot_file + "_LW_ET1000.eval"
hessian_eval = torch.load(lw_hessian_file, map_location='cpu')
eigenvals_layers, eigenvecs_layers = hessian_eval['eigenvals_layer'], hessian_eval['eigenvecs_layer']

assert os.path.isdir(dirc)
sys.path.append(dirc)
from config import Config # pylint: disable=no-name-in-module
conf = Config()

dataset = conf.dataset(train=True, transform=conf.test_transform)
# dataloader = OnDeviceDataLoader(dataset, batchsize=2)
dataloader = DataLoader(dataset, 2048)

net = conf.net()
net.load_state_dict(torch.load(snapshot_file, map_location='cuda' if torch.cuda.is_available() else 'cpu'))
net, device, handle = prepare_net(net) # type: torch.nn.Module, str, str
vis_module = vis(device=device)
measure_module = Measures(device=device)
ife = IntermediateFeatureExtractor(net, layer_seq)

conv_modules = {layer: rgetattr(net, layer) for layer in comp_layers}

# E_xs = comp_E_Bx(dataloader, ife, device, conv_modules, comp_layers)#, max_count=10)
E_xs = comp_E_Bx(dataloader, ife, device, conv_modules, comp_layers, max_count=stop_count)
mats = [E_xs[layer] for layer in comp_layers]
descs = [layer for layer in comp_layers]
# vis_module.plot_matsvd_zero_centered(mats, descs, name='E[x]_' + exp_name, dpi=200)

# E_xxTs = comp_E_BxBxT(dataloader, ife, device, conv_modules, comp_layers)#, max_count=10)
E_xxTs = comp_E_BxBxT(dataloader, ife, device, conv_modules, comp_layers, max_count=stop_count)
mats = [E_xxTs[layer] for layer in comp_layers]
descs = [layer for layer in comp_layers]
# vis_module.plot_matsvd_zero_centered(mats, descs, name='E[xxT]_' + exp_name, dpi=200)

# Singular Vec Compare
for layer in comp_layers:

    eigenvals, eigenvecs = eigenvals_layers[layer + ".weight"], torch.from_numpy(eigenvecs_layers[layer + ".weight"]).to(device) # pylint: disable=no-member
    E_x = E_xs[layer]
    E_xxT = E_xxTs[layer]

    Ux, Sx, Vx = torch.Tensor.svd(E_x)
    Ux.transpose_(0, 1)
    Vx.transpose_(0, 1)
    top_left_sigvec_x = Ux[0]

    UxxT, SxxT, VxxT = torch.Tensor.svd(E_xxT)
    UxxT.transpose_(0, 1)
    VxxT.transpose_(0, 1)
    top_left_sigvec_xxT = UxxT[0]
    top_right_sigvec_xxT = VxxT[0]

    print(layer, 'E[x], E[xxT] left singular vector dot product:', torch.Tensor.dot(top_left_sigvec_x, top_left_sigvec_xxT))
    print(eigenvecs.shape)

    layer_module = conv_modules[layer] # type: Conv2d
    Id_cout = torch.diag(torch.ones(layer_module.out_channels)) #pylint:disable=no-member
    naive_basis = kp_2d(Ux[:1], Id_cout)
    
    overlap_naive = measure_module.trace_overlap_trend(eigenvecs, naive_basis, np.arange(1, layer_module.out_channels + 1))
    print(overlap_naive)
# print(E_x[comp_layers[0]])