import torch
import sys, os, copy
import numpy as np
from torch.utils.data import DataLoader
from torch.nn import functional, Conv2d


def comp_E_Bx(dataloader, ife, device, conv_modules, comp_layers, max_count=1e10):

    ret = {}
    sample_count = 0

    for inputs, labels in iter(dataloader):
        sample_count += len(inputs)
        inputs, labels = inputs.to(device), labels.to(device)
        mids, _ = ife(inputs)
        for layer in comp_layers:

            x = mids[layer][0]
            layer_module = conv_modules[layer] # type: Conv2d
            unfolded_x = functional.unfold(
                x,
                kernel_size=layer_module.kernel_size,
                dilation=layer_module.dilation,
                padding=layer_module.padding,
                stride=layer_module.stride
            ).sum(dim=0)

            if layer not in ret:
                ret[layer] = unfolded_x
            else:
                ret[layer].add_(unfolded_x)
        print(sample_count)

        if sample_count > max_count:
            break
    
    for layer in comp_layers:
        ret[layer].div_(sample_count)
    
    return ret

def comp_E_BxBxT(dataloader, ife, device, conv_modules, comp_layers, max_count=1e10):

    ret = {}
    sample_count = 0

    for inputs, labels in iter(dataloader):
        sample_count += len(inputs)
        inputs, labels = inputs.to(device), labels.to(device)
        mids, _ = ife(inputs)
        for layer in comp_layers:

            x = mids[layer][0]
            layer_module = conv_modules[layer] # type: Conv2d
            unfolded_x = functional.unfold(
                x,
                kernel_size=layer_module.kernel_size,
                dilation=layer_module.dilation,
                padding=layer_module.padding,
                stride=layer_module.stride
            )
            BxBxT = torch.Tensor.matmul(unfolded_x, unfolded_x.transpose(-1, -2)).sum(dim=0)

            if layer not in ret:
                ret[layer] = BxBxT
            else:
                ret[layer].add_(BxBxT)
        print(sample_count)

        if sample_count > max_count:
            break
    
    for layer in comp_layers:
        ret[layer].div_(sample_count)
    
    return ret