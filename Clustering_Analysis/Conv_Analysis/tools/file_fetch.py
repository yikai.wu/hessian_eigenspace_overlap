import os, sys, glob
from os.path import *

def fetch_snapshot(dircs, epoch=-1, run_name='*', ret_first=True):
    if not isinstance(dircs, list):
        dircs = [dircs]
    evals = []
    
    snapshot = 'final.pth' if epoch == -1 else 'epoch{}.pth'.format(epoch)
    for dirc in dircs:
        assert isdir(dirc)
        exp_link = join(dirc, 'experiment_log')
        assert islink(exp_link), "no experiment_log symbolic link found in the given directory {}".format(dirc)
        exp_base = os.readlink(exp_link)
        potentials = glob.glob(exp_base + '/run_{}/**/{}'.format(run_name, snapshot))
        evals.extend(potentials)

    if ret_first:
        evals = evals[0]
    return evals

def fetch_eval_file(dircs, dim=1000, epoch=-1, arg="_LW_ET", run_name='*', ret_first=True):
    if not isinstance(dircs, list):
        dircs = [dircs]
    
    evals = []
    
    snapshot = 'final.pth' if epoch == -1 else 'epoch{}.pth'.format(epoch)
    for dirc in dircs:
        assert isdir(dirc)
        exp_link = join(dirc, 'experiment_log')
        assert islink(exp_link), "no experiment_log symbolic link found in the given directory {}".format(dirc)
        exp_base = os.readlink(exp_link)
        potentials = glob.glob(exp_base + '/run_{}/**/{}{}{}.eval'.format(run_name, snapshot, arg, dim))
        evals.extend(potentials)
    
    if ret_first:
        evals = evals[0]

    return evals
