import torch
import numpy as np
from algos.closeformhessian.hessianmodule import HessianModule

def singular_vec_overlap_analysis(HM: HessianModule,
                                  topn: int,
                                  comp_layers,
                                  exp_name,
                                  eigenvals_real: dict,
                                  eigenvecs_real: dict,
                                  crop_ratio=1):

    eigenthings_est = HM.hessian_eigenthings_estimate(comp_layers, num_eigenthings=topn, dataset_crop=crop_ratio)
    E_xxTs = HM.E_xxT(comp_layers, dataset_crop=crop_ratio)
    E_UTAUs = HM.E_UTAU(comp_layers, dataset_crop=crop_ratio)
    
    for layer in comp_layers:
        name = "Pairs_{}_{}_{}".format(exp_name, layer, topn)
        H_eigenvals_est, H_eigenvecs_est, _ = eigenthings_est[layer]
        layer_weight_ind = layer + '.weight'
        H_eigenvecs = torch.from_numpy(eigenvecs_real[layer_weight_ind]) # pylint: disable=no-member
        eigenvals_UTAU, eigenvecs_UTAU = HM.utils.eigenthings_tensor_utils(E_UTAUs[layer], symmetric=True) # pylint: disable=no-member
        eigenvals_xxT, eigenvecs_xxT = HM.utils.eigenthings_tensor_utils(E_xxTs[layer], symmetric=True) # pylint: disable=no-member

        est_l = []
        est_r = []
        base_l = []
        base_r = []

        for i in range(topn):
            vec_base_mat = HM.utils.reshape_to_layer(H_eigenvecs[i], layer)
            vec_est_mat = HM.utils.reshape_to_layer(H_eigenvecs_est[i], layer)
            U_base, S_base, V_base = torch.svd(vec_base_mat, compute_uv=True) # pylint: disable=no-member
            U_est, S_est, V_est = torch.svd(vec_est_mat, compute_uv=True) # pylint: disable=no-member
            est_l.append(U_est[:, :1])
            base_l.append(U_base[:, :1])
            est_r.append(V_est[:, :1])
            base_r.append(V_base[:, :1])

        est_L_dot = HM.utils.mm(eigenvecs_UTAU, torch.cat(est_l, dim=1)) ** 2 # pylint: disable=no-member
        base_L_dot = HM.utils.mm(eigenvecs_UTAU, torch.cat(base_l, dim=1)) ** 2 # pylint: disable=no-member
        est_R_dot = HM.utils.mm(eigenvecs_xxT, torch.cat(est_r, dim=1)) ** 2 # pylint: disable=no-member
        base_R_dot = HM.utils.mm(eigenvecs_xxT, torch.cat(base_r, dim=1)) ** 2 # pylint: disable=no-member

        mats = [[est_L_dot[:topn], est_R_dot[:topn]], [base_L_dot[:topn], base_R_dot[:topn]]]
        descs = [["est_L", "est_R"], ["base_L", "base_R"]]
        HM.vis.plot_mat(mats, descs, name, w=12, v_min=-1, v_max=1, colormap='bwr')