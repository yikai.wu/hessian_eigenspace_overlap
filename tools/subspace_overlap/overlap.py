import torch
import os, sys
import numpy as np
import datetime
import pynvml

def gpu_memory(info=""):
    try:
        pynvml.nvmlInit()
        handle = pynvml.nvmlDeviceGetHandleByIndex(0)
        mem_info = pynvml.nvmlDeviceGetMemoryInfo(handle)
        print("| GPU RAM {:.4g}% | Total {} | {} |\t {}".format(100 * mem_info.used / mem_info.total, mem_info.total, pynvml.nvmlDeviceGetName(handle), info))
    except:
        return

def overlap_calc(subspace1, subspace2, device):
    M = subspace1.to(device)
    V = subspace2.to(device)
    Mt = torch.transpose(M, 0, 1) # pylint: disable=no-member
    n = M.size()[0]
    k = 0
    for i in range(n):
        vi = V[i]
        li = Mt.mv(M.mv(vi))
        ki = torch.dot(li, li) # pylint: disable=no-member
        k += ki
    del Mt, M, V
    torch.cuda.empty_cache()
    return (k / n).to('cpu').numpy()

def overlap_explicit(eigenvecs, dim:int, cross_max_sample=100, device='cpu', verbose=False):
    """
    Compute the trace overlap between eigenvectors\n
    Inputs: List of dimensions to compute; The starting and ending truncation for the eigenspace\n 
    Return: list of overlaps corresponding to the given dimensions
    """
    st = datetime.datetime.now()
    crosses = []
    for i in range(len(eigenvecs)):
        for j in range(i):
            crosses.append([i, j])
    np.random.shuffle(crosses)
    crosses = crosses[:cross_max_sample]

    res = []
    for k, (i, j) in enumerate(crosses):
        try:
            res.append(overlap_calc(
                eigenvecs[i][:dim],
                eigenvecs[j][:dim],
                device
            ))
        except:
            continue
    res = np.array(res)
    if verbose:
        print("Dim {} took {}".format(dim, datetime.datetime.now() - st))
        gpu_memory("Trace Overlap Cross Finished")
    return res

def load_eval(eval_path, device):
    meta = torch.load(eval_path, map_location='cpu')
    if 'layerinfo' in meta:
        layer_info = meta['layerinfo']
        for layer in layer_info:
            meta['eigenvecs_layer'][layer] = torch.from_numpy(meta['eigenvecs_layer'][layer]).to('cpu')
    return meta

class Subspaces():
    def __init__(self, eval_paths):
        st = datetime.datetime.now()
        gpu_memory("Initialize")
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'
        self.eval_paths = eval_paths
        self.metas = [load_eval(eval_path, self.device) for eval_path in self.eval_paths]
        self.layer_info = None
        if 'layerinfo' in self.metas[0]:
            self.layer_info = self.metas[0]['layerinfo']
        print("Loading finished, took {}".format(datetime.datetime.now() - st))
        gpu_memory("Loading Metas Finished")

    def append(self, eval_paths_new):
        new_metas = [load_eval(eval_path, self.device) for eval_path in eval_paths_new]
        for i in range(len(new_metas)):
            for layer in new_metas[i]['eigenvecs_layer']:
                self.metas[i]['eigenvecs_layer'][layer] = new_metas[i]['eigenvecs_layer'][layer]
        new_layer_info = new_metas[0]['layerinfo']
        self.layer_info.update(new_layer_info)
        print(new_layer_info)
        print(self.layer_info)

    
    def trace_overlap_cross_layer(self, dim:int, layer:str, cross_max_sample=100):
        """
        Compute the trace overlap between eigenvectors\n
        Inputs: List of dimensions to compute; The starting and ending truncation for the eigenspace\n 
        Return: list of overlaps corresponding to the given dimensions
        """
        st = datetime.datetime.now()
        crosses = []
        for i in range(len(self.metas)):
            for j in range(i):
                crosses.append([i, j])
        np.random.shuffle(crosses)
        crosses = crosses[:cross_max_sample]

        res = []
        for k, (i, j) in enumerate(crosses):
            try:
                res.append(overlap_calc(
                    self.metas[i]['eigenvecs_layer'][layer][:dim],
                    self.metas[j]['eigenvecs_layer'][layer][:dim],
                    self.device
                ))
            except:
                continue
        res = np.array(res)
        print("Layer {} Dim {} took {}".format(layer, dim, datetime.datetime.now() - st))
        gpu_memory("Trace Overlap Cross Finished")
        return res
    
    

