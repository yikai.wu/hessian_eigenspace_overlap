import os, sys, glob
from os.path import *

def fetch_dirc(dircs:list, dim, epoch=-1, arg="_ET"):
    evals = []
    
    snapshot = 'final.pth' if epoch == -1 else 'epoch{}.pth'.format(epoch)
    for dirc in dircs:
        assert isdir(dirc)
        exp_link = join(dirc, 'experiment_log')
        assert islink(exp_link), "no experiment_log symbolic link found in the given directory {}".format(dirc)
        exp_base = os.readlink(exp_link)
        potentials = glob.glob(exp_base + '/run_*/**/{}{}{}.eval'.format(snapshot, arg, dim))
        evals.extend(potentials)

    return evals

