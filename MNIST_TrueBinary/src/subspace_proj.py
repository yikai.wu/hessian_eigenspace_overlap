import torch
from collections import OrderedDict
import numpy as np
import os
from config import Config # pylint: disable=no-name-in-module
import utils


def load_eigenthings(eigenthings_path: str, target_device: str):
    assert os.path.isfile(eigenthings_path)    
    eigenvals, eigenvecs = torch.load(eigenthings_path, map_location=target_device)
    print("Load finished, with count {}".format(len(eigenvals)))
    return eigenvals, eigenvecs

def load_eigenthings_from_eval(eigenthings_path: str, target_device: str):
    assert os.path.isfile(eigenthings_path)
    data = torch.load(eigenthings_path)
    eigenvals = torch.from_numpy(data['eigenvals']).to(target_device) # pylint: disable=no-member
    eigenvecs = torch.from_numpy(data['eigenvecs']).to(target_device) # pylint: disable=no-member
    return eigenvals, eigenvecs

def eigenvecs2projmat(eigenvecs, num: int):
    assert len(eigenvecs) >= num
    assert num >= 1
    vecs = [eigenvecs[i] for i in range(num)]
    paramspace_dim = len(vecs[0])
    M = torch.cat(vecs).view(torch.Size([num, paramspace_dim])) # pylint: disable=no-member
    Mt = torch.transpose(M, 0, 1) # pylint: disable=no-member
    return M, Mt

def eigenvecs2projmat_sortabs(eigenvecs, eigenvals, num: int):
    assert len(eigenvecs) >= num
    assert num >= 1

    indices = list(range(len(eigenvecs)))
    indices.sort(key = lambda x: abs(eigenvals[x]), reverse=True)
    vecs = [eigenvecs[i] for i in indices[:num]]
    paramspace_dim = len(vecs[0])
    M = torch.cat(vecs).view(torch.Size([num, paramspace_dim])) # pylint: disable=no-member
    Mt = torch.transpose(M, 0, 1) # pylint: disable=no-member
    return M, Mt

def projvecs2projmat(vecs):
    assert len(vecs) >= 1
    paramspace_dim = len(vecs[0])
    M = torch.cat(vecs).view(torch.Size([len(vecs), paramspace_dim])) # pylint: disable=no-member
    Mt = torch.transpose(M, 0, 1) # pylint: disable=no-member
    return M, Mt

def project(vec: torch.Tensor, M: torch.Tensor, Mt: torch.Tensor):
    coord = Mt.mv(M.mv(vec))
    return coord

if __name__ == "__main__":

    egt_path = "train_log/cifar10_landscape_exp/LeNetProjection/LeNet_base/run_1/models/epoch10.pth.P1_lanczos_topn250.eigenthings"
    conf = Config()
    net = conf.net()
    net, device, _ = utils.prepare_net(net)
    eigenvals, eigenvecs = load_eigenthings(egt_path, device)
    
    M, Mt = eigenvecs2projmat(eigenvecs, 10)
    v = sd2vec(eigenvecs[0]) + sd2vec(eigenvecs[1])
    print(sd2vec(eigenvecs[0]) + sd2vec(eigenvecs[1]))
    v_new = project(v, M, Mt)
    print(v_new)